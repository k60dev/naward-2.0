<?php
class Aboard extends CI_Controller
{
    public $return_data = [];
    public $return_status = "99";
    public $return_msg = "";
    public $access_code;
    public $access_keyword = "aw";
    public $ss_a_id = false;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('aboard_model');
        $this->load->model('admin_model');
        $this->load->library('session');
        $this->load->helper(array('form', 'url','common','upload','thumbnail'));
        $this->ss_a_id = $this->uri->segment(3)?$this->uri->segment(3):get_award_session();
        $this->access_code = get_access($this->access_keyword);
    }

    public function result_refactoring($result)
    {
        $this->return_status = $result['status']?$result['status']:$this->return_status;
        $this->return_data = $result['data']?$result['data']:$this->return_data;
        $this->return_msg = $result['msg']!=null?$result['msg']:$this->return_msg;
        return return_refactoring($this->return_status, $this->return_data, $this->return_msg, $this->access_code);
    }

    public function gallery($ab_id=false)
    {
        $result = $this->aboard_model->get_aboard('gallery', $this->ss_a_id, $ab_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function speech($ab_id=false)
    {
        $result = $this->aboard_model->get_aboard('speech', $this->ss_a_id, $ab_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function create($type)
    {
        if ($this->access_code=="00") {
            $path = "aboard/{$type}";
            $upload_result = upload_local_file($path);
            $img = "";
            if ($upload_result['status']=="00") {
                $storage_result = storage_upload($path, $upload_result);
                $img_result = "{$upload_result['upload_path']}/{$upload_result['file_name']}";
                unlink($img_result);
                rmdir($upload_result['upload_path']);
                $img = $storage_result['path'];
            }
            $result = $this->aboard_model->create_aboard($type, $img);
            $this->return_status = $result['status'];
            $this->return_data = $result['data'];
        }
        $returnArray = pack_return($this->return_status, $this->return_data);
        echo json_encode($returnArray);
    }

    public function modify($type)
    {
        if ($this->access_code=="00") {
            $path = "aboard/{$type}";
            $upload_result = upload_local_file($path);
            $img = "";
            if ($upload_result['status']=="00") {
                $storage_result = storage_upload($path, $upload_result);
                $img_result = "{$upload_result['upload_path']}/{$upload_result['file_name']}";
                unlink($img_result);
                rmdir($upload_result['upload_path']);
                $img = $storage_result['path'];
            }
            $result = $this->aboard_model->modify_aboard($type, $img);
            $this->return_status = $result['status'];
            $this->return_data = $result['data'];
        }
        $returnArray = pack_return($this->return_status, $this->return_data);
        echo json_encode($returnArray);
    }

    public function fetch()
    {
        $access = get_access($this->access_keyword);
        $data = $this->aboard_model->fetch_aboard();
        $returnArray = pack_return($access, $data);
        echo json_encode($returnArray);
        if ($this->access_code=="00") {
            $a_id = $this->ss_a_id;
            $result = $this->aboard_model->fetch_aboard();
            $this->return_status = $result['status'];
            $this->return_data = $result['data'];
        }
        $returnArray = pack_return($this->return_status, $this->return_data);
        echo json_encode($returnArray);
    }

    public function remove($type)
    {
        if ($this->access_code=="00") {
            $a_id = $this->ss_a_id;
            $result = $this->aboard_model->remove_aboard($type);
            $this->return_status = $result['status'];
            $this->return_data = $result['data'];
        }
        $returnArray = pack_return($this->return_status, $this->return_data);
        echo json_encode($returnArray);
    }

    
    public function create_gallery()
    {
        $data = $this->create("gallery");
        echo $data;
    }
    
    public function modify_gallery()
    {
        $data = $this->modify("gallery");
        echo $data;
    }
    
    public function remove_gallery()
    {
        $data = $this->remove("gallery");
        echo $data;
    }
    
    
    public function create_speech()
    {
        $data = $this->create("speech");
        echo $data;
    }
    
    public function modify_speech()
    {
        $data = $this->modify("speech");
        echo $data;
    }
    
    public function remove_speech()
    {
        $data = $this->remove("speech");
        echo $data;
    }

    public function list()
    {
        $this->load->view('vue/index');
    }

    public function gallery_usr($a_id, $ab_id=false)
    {
        $data = $this->aboard_model->get_aboard('gallery', $a_id, $ab_id);
        $returnArray = pack_return("00", $data);
        echo json_encode($returnArray);
    }

    public function speech_usr($a_id, $ab_id=false)
    {
        $data = $this->aboard_model->get_aboard('speech', $a_id, $ab_id);
        $returnArray = pack_return("00", $data);
        echo json_encode($returnArray);
    }
}
