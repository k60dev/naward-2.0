<?php
class Enroll extends CI_Controller
{
    public $u_no = 0;//$this->session->userdata('u_no');
    public function __construct()
    {
        parent::__construct();
        
        $this->load->helper(array('form', 'url','common','upload'));
        $this->load->database();
        $this->load->library('session');
        $this->load->library('Notice');
        $this->load->model('award_model');
        $this->load->model('user_model');
        $this->load->model('enroll_model');
        $this->ss_a_id = get_award_session();
        $this->u_no = $this->session->userdata('u_no');
        /*
        if ($this->u_no=="") {
            echo "접근할 수 없습니다.";
        }*/
    }

    public function main()
    {
        $this->load->view('vue/index');
    }

    public function category()
    {
        $this->load->view('vue/index');
    }

    public function work()
    {
        if ($this->u_no=='') {
            redirect("/login?redirect=/enroll/work");
        } else {
            $this->load->view('vue/index');
        }
    }

    public function cart()
    {
        if ($this->u_no=='') {
            redirect("/login?redirect=/enroll/cart");
        } else {
            $this->load->view('vue/index');
        }
    }

    public function payment()
    {
        $this->load->view('vue/index');
    }

    public function result()
    {
        $this->load->view('payment/process');
    }

    public function process()
    {
        $this->load->view('payment/process');
    }

    public function receipt($p_no=false)
    {
        /* 처리된 결과값을 확인하여 DB에 저장하고 보여주는 부분 */
        if ($p_no) {
            $this->load->view('vue/index');
        } else {
            if (count($_POST)>0) {
                $pay_type = isset($_POST['pay_type']) ? $_POST['pay_type'] : 'credit';
                if ($pay_type=="deposit") {
                    $p_no = $this->input->post('pay_no');
                    $u_no = $this->u_no;
                    $return = $this->enroll_model->complete_payment_bank($u_no, $p_no);
                    if ($return==true) {
                        $group = "pa";
                        $type = "request";
                        $res_mail = Notice::send_notification("mail", $u_no, $group, $type, $p_no);
                        $res_sms = Notice::send_notification("sms", $u_no, $group, $type, $p_no);
                        redirect("/enroll/receipt/{$p_no}");
                    }
                } else {
                    $resultcd   = "";
                    $trno       = "";
                    $authno     = "";
                    $isscd      = "";
                    $rcid       = $_POST["reWHCid"];
                    $rctype     = $_POST["reWHCtype"];
                    $rhash      = $_POST["reWHHash"];
                    $param = [$rcid, null];
                    $this->load->library('KSPayWebHost', $param);
                    if ($this->kspaywebhost->kspay_send_msg("1")) {  //KSNET 결제결과 중 아래에 나타나지 않은 항목이 필요한 경우 Null 대신 필요한 항목명을 설정할 수 있습니다.
                        $authyn   = trim($this->kspaywebhost->kspay_get_value("authyn"));
                        $trno     = trim($this->kspaywebhost->kspay_get_value("trno"));
                        $trddt    = trim($this->kspaywebhost->kspay_get_value("trddt"));
                        $trdtm    = trim($this->kspaywebhost->kspay_get_value("trdtm"));
                        $amt      = trim($this->kspaywebhost->kspay_get_value("amt"));
                        $authno   = trim($this->kspaywebhost->kspay_get_value("authno"));
                        $msg1     = trim($this->kspaywebhost->kspay_get_value("msg1"));
                        $msg2     = trim($this->kspaywebhost->kspay_get_value("msg2"));
                        $ordno    = trim($this->kspaywebhost->kspay_get_value("ordno"));
                        $isscd    = trim($this->kspaywebhost->kspay_get_value("isscd"));
                        $aqucd    = trim($this->kspaywebhost->kspay_get_value("aqucd"));
                        $temp_v   = "";
                        $result   = trim($this->kspaywebhost->kspay_get_value("result"));
                        $halbu    = trim($this->kspaywebhost->kspay_get_value("halbu"));
                        $cbtrno   = trim($this->kspaywebhost->kspay_get_value("cbtrno"));
                        $cbauthno = trim($this->kspaywebhost->kspay_get_value("cbauthno"));

                        //if (!empty($msg1)) $msg1 = iconv("EUC-KR", "UTF-8", $msg1);
                        //if (!empty($msg2)) $msg2 = iconv("EUC-KR", "UTF-8", $msg2);

                        if (!empty($authyn) && 1 == strlen($authyn)) {
                            if ($authyn == "O") {
                                // 정상승인
                                $resultcd = "0";
                            } else {
                                // 승인실패
                                $resultcd = "-2";//trim($authno);
                                $resultmsg = "카드 승인이 실패하였습니다.";
                            }
                        } else {
                            $resultcd = "-2";
                            $resultmsg = "잘못된 결제시도 입니다.";
                        }
                    }
                    $payinfo = [
                        "resultcd" => $resultcd,
                        "trno"=>$trno,
                        "authno"=>$authno,
                        "isscd"=>$isscd
                    ];

                    if ($resultcd=="-2") {
                        // $receipt = $this->enroll_model->fail_payment($this->u_no, $payinfo);
                        echo "<script>alert('".$resultmsg."');window.location.href='/enroll/cart';</script>";
                        //$_SESSION['resultmsg'] = $resultmsg;
                        //$_SESSION['modulemsg1'] = $msg1;
                        //$_SESSION['modulemsg2'] = $msg2;
                        //redirect("/enroll/payment");
                    } else {
                        $receipt = $this->enroll_model->complete_payment($this->u_no, $payinfo);
                        $p_no = $receipt['p_no'];
                        if ($receipt["p_status"]==9||$receipt["p_status"]==1) {
                            $u_no = $receipt['u_no'];
                            $group = "pa";
                            $type = $receipt["p_status"]==9?"finish":"request";
                            $res_mail = Notice::send_notification("mail", $u_no, $group, $type, $p_no);
                            $res_sms = Notice::send_notification("sms", $u_no, $group, $type, $p_no);
                        }
                        redirect("/enroll/receipt/{$p_no}");
                    }
                }
            } else {
                //redirect("/");
            }
        }
    }
}