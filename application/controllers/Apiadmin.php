<?php
class ApiAdmin extends CI_Controller
{
    public $access_total        = "*";
    public $access_dashboard    = "da";
    public $access_basic        = "ba";
    public $access_user         = "us";
    public $access_payment      = "pa";
    public $access_award        = "aw";
    public $access_admin        = "ua";
    public $return_status = "99";
    public $access_code = "00";
    public $access_keyword = "";
    public $return_msg = "";
    public $return_data = [];

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('Notice');
        $this->load->helper(array('form', 'url','common','upload','thumbnail'));
        $this->load->database();
        $this->load->model('terms_model');
        $this->load->model('notification_model');
        $this->load->model('payment_model');
        $this->load->model('result_model');
        $this->load->model('receiv_model');
    }

    public function access_auth()
    {
        return get_auth();
    }

    public function result_refactoring($result)
    {
        $this->return_status = $result['status']?$result['status']:$this->return_status;
        $this->return_data = $result['data']?$result['data']:$this->return_data;
        $this->return_msg = $result['msg']?$result['msg']:$this->return_msg;
        return return_refactoring($this->return_status, $this->return_data, $this->return_msg, $this->access_code);
    }

    public function dashboard_main()
    {
        $a_id = 0;
        $remainDay = 0;
        $cntTotalWork = 0;
        $cntExpectedWinner = '';
        $cntNeedToConfirmedDeposit = 0;
        $totalCreditPrice = 0;
        $cntNeedToTax = 0;
        $this->access_keyword = "da";

        $award_query = "select * from tb_award where a_status = 1 order by a_id desc limit 1";
        $award_result =  $this->db->query($award_query);
        $award_row = $award_result->result_array();
        if ($award_row) {
            $object_date = $award_row[0]['award_end'];
            $a_id = $award_row[0]['a_id'];
            $today_date = date("Y-m-d");
        }

        if ($a_id>0) {
            // $award_total_query = "select count(*) as total_state from tb_work where a_id = {$a_id} and is_del = 'N'";
            $award_total_query = "select count(*) as total_state from tb_work as a left join tb_payment as b on a.p_no = b.p_no where a.a_id = {$a_id} and a.is_del = 'N' and b.p_status = 9";
            $award_total_result =  $this->db->query($award_total_query);
            $award_total_row = $award_total_result->result_array();
            $work_total = $award_total_row[0]['total_state'];
    
            $award_winner_query = "select count(*) as total_winner from tb_prize_receiv where a_id = {$a_id} and r_status = 0";
            $award_totalwinner_result =  $this->db->query($award_winner_query);
            $award_totalwinner_row = $award_totalwinner_result->result_array();
            $winner_total = $award_totalwinner_row[0]['total_winner'];
    
            $remainDay = dateDifference($object_date, $today_date);
            $cntTotalWork = $work_total;
            $cntExpectedWinner = $winner_total;
        }

        $aw = [
            "remainDay"=>$remainDay,
            "cntTotalWork"=>$cntTotalWork,
            "cntExpectedWinner"=>$cntExpectedWinner
            ];


        if ($a_id>0) {
            $pay_confirm_query = "select count(*) as total_confirm from tb_payment where a_id = {$a_id} and  p_status = 0 and p_method = 'deposit'";
            $pay_confirm_result =  $this->db->query($pay_confirm_query);
            $pay_confirm_row = $pay_confirm_result->result_array();
            $confirm_total = $pay_confirm_row[0]['total_confirm'];
            
            // $paid_query = "select sum(p_payprice) as total_price from tb_payment where a_id = {$a_id} and  p_status = 9";
            $paid_query = "select sum(p_totalprice) as total_price from tb_payment where a_id = {$a_id} and  p_status = 9 and p_method = 'credit'";
            $paid_result =  $this->db->query($paid_query);
            $paid_row = $paid_result->result_array();
            $paid_total = $paid_row[0]['total_price'];
            $paid_total = is_null($paid_total)?0:$paid_total;

            $pay_request_query = "select count(*) as total_request from tb_payment where a_id = {$a_id} and  p_taxrequest = 1 and p_status != -1";
            $pay_request_result =  $this->db->query($pay_request_query);
            $pay_request_row = $pay_request_result->result_array();
            $request_total = $pay_request_row[0]['total_request'];

            $cntNeedToConfirmedDeposit = $confirm_total;
            $totalCreditPrice = $paid_total;
            $cntNeedToTax = $request_total;
        }
        $pa = [
            "cntNeedToConfirmedDeposit"=>$cntNeedToConfirmedDeposit,
            "totalCreditPrice"=>$totalCreditPrice,
            "cntNeedToTax"=>$cntNeedToTax
            ];


        $aw_arr = (get_access('aw')!=="99"||get_access('*')!=="99")?["status"=>"00","data"=>$aw]:["status"=>"99","data"=>[]];
        $pa_arr = (get_access('pa')!=="99"||get_access('*')!=="99")?["status"=>"00","data"=>$pa]:["status"=>"99","data"=>[]];
        $returnAccess = $aw_arr["status"]!="99"&&$pa_arr["status"]!="99"?"00":(($aw_arr["status"]=="99"?"9":"0").($pa_arr["status"]=="99"?"9":"0"));
        $returnData = ["aw"=>$aw_arr,"pa"=>$pa_arr];
        $access = get_access($this->access_keyword);
        
        $returnArray = pack_return($returnAccess, $returnData);
        echo json_encode($returnArray);
    }
    
    public function terms_main() # api/admin/terms
    {
        $this->access_keyword = "ba";
        $this->access_code = get_access($this->access_keyword);
        $result = $this->terms_model->get_terms();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function terms_view($id = null) # api/admin/terms/view/1
    {
        $this->access_keyword = "ba";
        $result = $this->terms_model->get_terms($id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function terms_update() # api/admin/terms/update
    {
        $this->access_keyword = "ba";
        $result = $this->terms_model->update_terms();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }




    public function notification_main()
    {
        $this->access_keyword = "ba";
        $result = $this->notification_model->get_notification();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function notification_contents()
    {
        $this->access_keyword = "ba";
        $result = $this->notification_model->get_notification_contents();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function notification_view($id = null)
    {
        $this->access_keyword = "ba";
        $result = $this->notification_model->get_notification();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function notification_update()
    {
        $this->access_keyword = "ba";
        $result = $this->notification_model->update_notification();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function notification_contents_update()
    {
        $this->access_keyword = "ba";
        $result = $this->notification_model->update_contents_notification();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function notice_sms()
    {
        $method = $this->input->get('method');
        $u_no = $this->input->get('u_no');
        $result = Notice::send_notification("sms", $u_no, $method);
        print_r($result);
    }

    public function notice_mail()
    {
        $method = $this->input->get('method');
        $u_no = $this->input->get('u_no');
        $result = Notice::send_notification("mail", $u_no, $method);
        print_r($result);
    }



    public function user_main($rest=false)
    {
        $this->access_keyword = "us";
        if ($rest!==false) {
            $result = $this->user_model->get_user(false, $rest);
        } else {
            $result = $this->user_model->get_user();
        }
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }
    
    public function user_view($u_no)
    {
        $this->access_keyword = "us";
        $result = $this->user_model->get_user_info($u_no);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function user_create()
    {
        $this->access_keyword = "us";
        $result = $this->user_model->insert_user();
        $return = $this->result_refactoring($result);
        if ($return['data']['e_id']>0&&$_FILES) {
            $this->enterprise_registration($return['data']['e_id']);
        }
        echo json_encode($return);
    }

    public function user_modify()
    {
        $this->access_keyword = "us";
        $result = $this->user_model->update_user();
        $return = $this->result_refactoring($result);
        if ($this->input->post('e_id')>0&&$_FILES) {
            $this->enterprise_registration($return['data']['e_id']);
        }
        echo json_encode($return);
    }

    public function reset_password()
    {
        $this->access_keyword = "us";
        $result = $this->user_model->reset_user_password();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }
    
    public function export_userlist()
    {
        $filename = "회원목록";
        $header = ['아이디','담당자명','담당자 유선번호','담당자 무선번호','담당자 이메일','기업명','대표자명','회사대표 이메일','사업장 주소1','사업장 주소2','우편번호','사업자등록번호','업태','업종','가입 일자','마지막 로그인 일자'];
        $data = $this->user_model->export_userlist();
        export_csv($filename, $header, $data);
    }
    

    public function enterprise_registration($e_id)
    {
        $msg = "회사데이터를 확인할 수 없습니다";
        $picture_name = "e_registration";
        $new_picture_name = $e_id."_".time();
        $images = $_FILES;
        $type = "enterprise/registration";
        $file_url = get_url($type);
        $config = [
            "picture_name"=>$picture_name,
            "new_picture_name"=>$new_picture_name,
            "type"=>$type,
            "msg"=>$msg
        ];
        if ($e_id>0) {
            $return = upload_file_in_controller($config);
            $this->user_model->update_registration($e_id, $return['path']);
        } else {
            $return = fail_result($msg);
        }
        //echo json_encode($return);
    }

    public function user_patch_rest()
    {
        $this->access_keyword = "us";
        $result = $this->user_model->patch_user("is_rest", "Y");
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function user_patch_active()
    {
        $this->access_keyword = "us";
        $result = $this->user_model->patch_user("is_rest", "N");
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function user_remove()
    {
        $this->access_keyword = "us";
        $result = $this->user_model->patch_user("is_del", "Y");
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function user_view_enterprise($e_id)
    {
        $this->access_keyword = "us";
        $data = $this->user_model->get_user($e_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function admin_userfind()
    {
        $data = $this->admin_model->find_user_id();
        echo json_encode($data);
    }
    
    public function admin_main()
    {
        $this->access_keyword = "ua";
        $result = $this->admin_model->get_admin();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function admin_view($ua_id)
    {
        $this->access_keyword = "ua";
        $result = $this->admin_model->get_admin($ua_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function admin_create()
    {
        $this->access_keyword = "ua";
        $result = $this->admin_model->create_admin();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function admin_modify()
    {
        $this->access_keyword = "ua";
        $result = $this->admin_model->modify_admin();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function admin_patch()
    {
        $this->access_keyword = "ua";
        $result = $this->admin_model->batch_admin();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function admin_remove()
    {
        $this->access_keyword = "ua";
        $result = $this->admin_model->remove_admin();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function admin_logs()
    {
        $result = $this->admin_model->get_logs();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function payment_main($status=false)
    {
        $this->access_keyword = "pa";
        $result = $this->payment_model->get_payment(false, $status);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function payment_wait()
    {
        $this->access_keyword = "pa";
        $result = $this->payment_model->get_payment(false, 1);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function payment_finish()
    {
        $this->access_keyword = "pa";
        $result = $this->payment_model->get_payment(false, 9);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function payment_cancel()
    {
        $this->access_keyword = "pa";
        $result = $this->payment_model->get_payment(false, -1);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function payment_view($p_no)
    {
        $this->access_keyword = "pa";
        $result = $this->payment_model->get_payment($p_no);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function export_paylist()
    {
        $filename = "결제목록";
        $header = ['출품접수번호','접수일자','아이디','기업명','출품상세','결제유형','결제금액','결제일자'];
        $data = $this->payment_model->export_paylist();
        export_csv($filename, $header, $data);
    }

    public function payment_work($p_no)
    {
        $this->access_keyword = "pa";
        $result = $this->payment_model->get_payment_work($p_no);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }
    
    public function payment_patch($status)
    {
        $this->access_keyword = "pa";
        $result = $this->payment_model->patch_payment("p_status", $status);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function payment_cancels($p_no)
    {
        $this->access_keyword = "pa";
        $result = $this->payment_model->cancel_payment($p_no);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function payment_patchworks($p_no)
    {
        $this->access_keyword = "pa";
        $status = $this->input->post('status');
        $result = $this->payment_model->update_work_status($p_no, $status);
        echo json_encode($result);
    }

    public function payment_work_batch($p_no, $status)
    {
        $this->access_keyword = "ba";
        $result = $this->payment_model->patch_status($p_no, $status);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function payment_request($status=0)
    {
        $this->access_keyword = "pa";
        $result = $this->payment_model->get_request($status);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function payment_process_request($p_no)
    {
        $taxBillNo = $this->payment_model->create_taxBillNo($p_no);
        $taxbill_info = $this->payment_model->get_taxbill_info($p_no);
        if (is_null($taxbill_info)==true) {
            $msg = "결제정보를 확인할 수 없습니다.";
            $result = fail_result($msg);
            $return = $this->result_refactoring($result);
            echo json_encode($return);
        } else {
            $wrtYmd = date("Ymd");
            $pay_total = $taxbill_info['p_totalprice'];
            $pay_supply = ceil($pay_total/1.1);
            $pay_tax = (int)$pay_total-(int)$pay_supply;
            $pay_method = ["deposit"=>"cashAmt","credit"=>"crdAcrcAmt"];
            $rquest_method = $pay_method["{$taxbill_info['p_method']}"];
            $e_number = str_replace('-','',$taxbill_info['e_number']);
            $e_address = $taxbill_info['e_address1']." ".$taxbill_info['e_address2'];
            $billing_data = [
                "taxBills"=>[
                    [
                    "taxBillNo" => "{$taxBillNo}",
                    "taxBillTp" => "2",
                    "taxBillKndCd" => "01",
                    "taxBillCatCd" => "01",
                    "splyrBno" => "2018205787",
                    "splyrBzstNm" => "(사)한국디지털기업협회",
                    "splyrReprNm" => "고경구",
                    "splyrAddr" => "서울 강남구 도산대로 176 503호",
                    "splyrBizcd" => "서비스",
                    "splyrBiztp" => "어워드개최 외",
                    "splyrChrgrEmail" => "master@dea.or.kr",
                    "splyrBizrTp"=> "01",
                    "splyrcptrBno"=> "{$e_number}",
                    "splyrcptrBzstNm"=> "{$taxbill_info['e_company']}",
                    "splyrcptrReprNm"=> "{$taxbill_info['e_name']}",
                    "splyrcptrAddr"=> "{$e_address}",
                    "splyrcptrBizcd"=> "{$taxbill_info['e_business']}",
                    "splyrcptrBiztp"=> "{$taxbill_info['e_sector']}",
                    "splyrcptrChrgrEmail"=> "{$taxbill_info['enrollee_taxmail']}",
                    "splyrcptrBizrTp"=> "01",
                    "wrtYmd"=> "{$wrtYmd}",
                    "splyamt"=> "{$pay_supply}",
                    "taxamt" => "{$pay_tax}",
                    "totAmt" => "{$pay_total}",
                    "{$rquest_method}" => "{$pay_total}",
                    "dmndRcptTp" => "02",
                    "taxBillPitms" => [
                        [
                        "pitmNo" => "1",
                        "splyamt" => "{$pay_supply}",
                        "taxamt" => "{$pay_tax}"
                        ]
                    ]
                    ]
                ]
            ];
            $response = $this->notice->toast_bill($billing_data);
            $result_code = $response->header->resultCode;
            $result_contents = $response->data->taxBillApiIssueResponses;
            $result_contents_code = $result_contents[0]->code;
            $result_contents_issueSuccess = $result_contents[0]->issueSuccess;
            if ($result_code=="0"&&$result_contents_code=="0"&&$result_contents_issueSuccess=="1"&&isset($result_contents_issueSuccess)) {
                $u_no = $taxbill_info['u_no'];
                $group = "pa";
                $type = "tax";
                //$res_sms = Notice::send_notification("sms", $u_no, $group, $type);
                //$res_mail = Notice::send_notification("mail", $u_no, $group, $type);

                $insert_taxbill = [
                    'taxBillNo'=>$taxBillNo,
                    'splyrcptrBno'=>$e_number,
                    'splyrcptrBzstNm'=>"{$taxbill_info['e_company']}",
                    'splyrcptrReprNm'=>"{$taxbill_info['e_name']}",
                    'wrtYmd'=>"{$wrtYmd}",
                    "taxamt"=>$pay_tax,
                    'totAmt'=>$pay_supply,
                    'e_id'=>$taxbill_info['e_id'],
                    'w_date'=>date('Y-m-d H:i:s')
                ];
                $this->payment_model->insert_taxbill($insert_taxbill);
                $this->access_keyword = "pa";
                $result = $this->payment_model->request_tax($p_no);
                $return = $this->result_refactoring($result);
                echo json_encode($return);
            } else {
                $msg = "세금계산서 발행에 실패하였습니다. 자세한 내용은 콘솔을 참조해 주세요.";
                $result = fail_result($msg);
                $return = $this->result_refactoring($result);
                echo json_encode($return);
            }
        }
    }

    public function export_resultlist()
    {
        $filename = "출품목록";
        $header = ['번호','부문','분야','기업명','출품명','URL','제작기간시작','제작기간종료','고객사명','공동개발사명','출품개요 소개말','출품개요 본문','아이디','출품일자','출품접수번호','할당심사위원','상태','행정처리담당자','직책','유선','무선','이메일'];
        $data = $this->result_model->export_resultlist();
        export_csv($filename, $header, $data);
    }

    public function export_judgement()
    {
        $filename = "심사결과";
        $header = ['번호','부문','분야','기업명','출품명','URL','제작기간시작','제작기간종료','고객사명','공동개발사명','출품개요 소개말','출품개요 본문','아이디','출품일자','출품접수번호','할당심사위원','심사총점','심사항목점수','심사일자'];
        $data = $this->result_model->export_judgement();
        export_csv($filename, $header, $data);
    }

    public function export_winnerlist()
    {
        $filename = "수상목록";
        $header = ['번호','부문','분야','기업명','출품명','URL','제작기간시작','제작기간종료','고객사명','공동개발사명','출품개요 소개말','출품개요 본문','아이디','출품일자','출품접수번호','할당심사위원','수상명','상태','행정처리담당자','직책','유선','무선','이메일'];
        $data = $this->receiv_model->export_winnerlist();
        export_csv($filename, $header, $data);
    }

    public function send_member_notice($type)
    {
        $fail_count = 0;
        if ($type=='finish'||$type=='prize'||$type=='request') {
            $group = "aw";
            $u_no = $this->input->post('u_no');
            if ($u_no) {
                $res_sms = Notice::send_notification("sms", $u_no, $group, $type);
                $res_mail = Notice::send_notification("mail", $u_no, $group, $type);
                if ($res_sms!=0||$res_mail!=0) {
                    $fail_count++;
                }
            }
        }
        $status = $fail_count>0?"99":"00";
        $msg = $fail_count>0?"{$fail_count}건이 전송실패 하였습니다.":"전송이 완료되었습니다.";
        $result = ['status'=>$status,'data'=>[],'msg'=>$msg];
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function send_members_notice($type="notice")
    {
        $fail_count = 0;
        $result = $this->user_model->get_user(false, 'N');
        $userlist = $result['data'];
        foreach ($userlist as $user) {
            $u_no = $user['u_no'];
            $group = "aw";
            $res_sms = Notice::send_notification("sms", $u_no, $group, $type);
            $res_mail = Notice::send_notification("mail", $u_no, $group, $type);
            if ($res_sms!=0||$res_mail!=0) {
                $fail_count++;
            }
        }
        $status = $fail_count>0?"99":"00";
        $msg = $fail_count>0?"{$fail_count}건이 전송실패 하였습니다.":"전송이 완료되었습니다.";
        $result = ['status'=>$status,'data'=>[],'msg'=>$msg];
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function send_form_notice($type='email')
    {
        $subject = $this->input->post('subject');
        $contents = $this->input->post('contents');
        $userlist = $this->input->post('userlist');
        $arr_userlist = explode(',', $userlist);
        $fail_count = 0;
        foreach ((array) $arr_userlist as $user) {
            $result = $this->user_model->get_user($user, 'N');
            $userinfo = $result['data'];
            if ($type=='email') {
                $mail_address = $userinfo['u_email'];
                $contents_replace = str_replace(array("\r\n","\r","\n"), '<br/>', $contents);
                $data_contents = array(
                    'email_contents'=> $contents_replace,
                );
                $tpl = $this->load->view('common/tpl_email_base', $data_contents, true);
                $tpl_replace = str_replace(array("\r\n","\r","\n"), '', $tpl);
                $res = Notice::send_toast_form("mail", $mail_address, $subject, $tpl_replace);
            } elseif ($type=='sms') {
                $number = $userinfo['u_phone2'];
                $contents_replace = str_replace(array("\r\n","\r","\n"), '\n', $contents);
                $res = Notice::send_toast_form("sms", $number, $subject, $contents_replace);
            }
            if ($res!=0) {
                $fail_count++;
            }
        }
        $status = $fail_count>0?"99":"00";
        $msg = $fail_count>0?"{$fail_count}건이 전송실패 하였습니다.":"전송이 완료되었습니다.";
        $result = ['status'=>$status,'data'=>[],'msg'=>$msg];
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function send_form_notice_all($type='email')
    {
        $subject = $this->input->post('subject');
        $contents = $this->input->post('contents');
        $result = $this->user_model->get_user(false, 'N');
        $userlist = $result['data'];
        $fail_count = 0;
        $success_count = 0;
        foreach ($userlist as $user) {
            if ($type=='email') {
                $mail_address = $user['u_email'];
                $res = Notice::send_toast_form("mail", $mail_address, $subject, $contents);
            } elseif ($type=='sms') {
                $number = $user['u_phone2'];
                $res = Notice::send_toast_form("sms", $number, $subject, $contents);
            }
            if ($res!=0) {
                $fail_count++;
            } else {
                $success_count++;
            }
        }
        $status = $success_count>0?"00":"99";
        $msg = "{$success_count}건을 전송하였습니다.";
        $result = ['status'=>$status,'data'=>[],'msg'=>$msg];
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function email_auth($u_no)
    {
        $result = $this->user_model->update_email_auth($u_no);
        echo $result;
    }
}
