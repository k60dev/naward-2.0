<?php
class Api extends CI_Controller
{
    public $u_no = 0;//$this->session->userdata('u_no');
    public $return_status = "99";
    public $return_data = [];
    public $return_msg = "";
    public $access_code = "00";
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('mypage_model','enroll_model','service_model','result_model','live_model','payment_model','aboard_model','terms_model','popup_model'));
        $this->load->library(array('session','notice'));
        $this->load->helper(array('form', 'url','common','upload','thumbnail'));
        $this->u_no = $this->session->userdata('u_no');
        $this->ss_a_id = get_award_session();
        if ($this->ss_a_id=="") {
            $a_row = $this->award_model->get_current_award();
            if (is_null($a_row)==false) {
                $this->ss_a_id = $a_row['a_id'];
            }
        }
    }

    public function result_refactoring($result)
    {
        $this->return_status = $result['status']?$result['status']:$this->return_status;
        $this->return_data = $result['data']?$result['data']:$this->return_data;
        $this->return_msg = $result['msg']?$result['msg']:$this->return_msg;
        $result = return_refactoring($this->return_status, $this->return_data, $this->return_msg, $this->access_code);
        echo json_encode($result);
    }

    public function return_thumbnail($resource, $width, $height)
    {
        $fileinfo = pathinfo($resource);
        $dirname = $fileinfo['dirname'];
        $basename = $fileinfo['basename'];
        $filename = $fileinfo['filename'];
        $extension = $fileinfo['extension'];

        $resultname = "{$filename}_{$width}_{$height}.{$extension}";
        $result_src = "{$dirname}/{$resultname}";
        createThumbnail($resource, $result_src, $width, $height);
        return $resultname;
    }
    
    public function make_thumbnail($_filename_, $_storage_, $_user_, $size)//
    {
        $filename = explode(".", $_filename_);
        $filelenth = count($filename);
        $extension = $filename[($filelenth-1)];
        $originfile = str_replace($extension, "", $_filename_);
        
        $storage = $_storage_;
        $under = $_user_;

        $pull_storage = "{$storage}/{$under}";
        $local = "./uploads";
        $local_path = "{$local}/{$pull_storage}";
        $filesize = "_{$size[0]}_{$size[1]}";

        $is_exist_storage = storage_exist($storage, $under);
        if (count($is_exist_storage)>1) {
            $thumb_filename = "{$originfile}{$filesize}.{$extension}";
            $orgin_filename = "{$originfile}.{$extension}";
            $storage_thumb_path = "{$pull_storage}/{$thumb_filename}";
            $local_thumb_path = "{$local}/{$storage_thumb_path}";
    
            if ($is_exist_storage[1]=="") {
                $storage_origin_path = "{$pull_storage}/{$orgin_filename}";
                $local_origin_path = "{$local}/{$storage_origin_path}";
                if (is_dir("{$local}/{$pull_storage}")==false) {
                    mkdir("{$local}/{$pull_storage}", 755);
                }
                $img_result = storage_file_download($storage_origin_path, $local_origin_path);
                $img_300 = $this->return_thumbnail($local_origin_path, $size[0], $size[1]);
                storage_upload_exist($pull_storage, $img_300, $local_path);
                unlink("{$local_path}/{$img_300}");
                unlink($local_origin_path);
                rmdir($local_path);
            }
            
            $imgurl = get_url($storage_thumb_path);
        }
        return $imgurl;
    }

    public function get_current_award()
    {
        $a_row = $this->award_model->get_current_award();
        if (is_null($a_row)==false) {
            $this->ss_a_id = $a_row['a_id'];
        }
        echo json_encode($a_row);
    }

    public function get_award($a_id)
    {
        $a_row = $this->award_model->get_award($a_id);
        echo json_encode($a_row);
    }

    public function get_judge($a_id=false)
    {
        $result = $this->award_model->get_judge($a_id);
        $returnArray = pack_return("00", $result);
        echo json_encode($returnArray);
    }

    public function get_judge_route($route)
    {
        $result = $this->award_model->get_judge_route($route);
        $returnArray = pack_return("00", $result);
        echo json_encode($returnArray);
    }

    public function get_category($a_id=false)
    {
        if (!$a_id) {
            $a_id = $this->ss_a_id;
        }
        // $result = $this->award_model->get_category($a_id?$a_id:"2020");//$this->ss_a_id
        $result = $this->award_model->get_category($a_id);
        $returnArray = pack_return("00", $result);
        echo json_encode($returnArray);
    }

    public function get_category_info($c_id)
    {
        $a_id = $this->input->input_stream('a_id');
        if (!$a_id) {
            $a_id = $this->ss_a_id;
        }
        // $result = $this->award_model->get_category("2020", $c_id);//$this->ss_a_id
        $result = $this->award_model->get_category($a_id, $c_id);
        $returnArray = pack_return("00", $result);
        echo json_encode($returnArray);
    }

    public function get_category_info_only($c_id)
    {
        $result = $this->award_model->get_category_info($c_id);
        $returnArray = pack_return("00", $result);
        echo json_encode($returnArray);
    }

    public function get_subcategory($cp_id)
    {
        $a_id = $this->input->input_stream('a_id');
        if (!$a_id) {
            $a_id = $this->ss_a_id;
        }
        $result = $this->award_model->get_child_category($a_id, $cp_id);
        $returnArray = pack_return("00", $result);
        echo json_encode($returnArray);
    }

    private function file_upload($img_dir)
    {
        $config['upload_path'] = "./uploads/{$img_dir}";
        if (is_dir($config['upload_path'])==false) {
            mkdir($config['upload_path'], "0755", true);
        }
        $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
        //$config['max_size']   = '100';
        //$config['max_width']  = '10240';
        //$config['max_height']  = '7680';
        $field_name = "file";
        $this->load->library('upload', $config);
        $path = $config['upload_path'];
        $status = "99";
        $imgname = "";
        $file_name = "";
        $full_path = "";
        if (! $this->upload->do_upload($field_name)) {
            $msg = $this->upload->display_errors();
        } else {
            $result = $this->upload->data();
            $file_name = $result['file_name'];
            $full_path = $result['full_path'];
            $status = "00";
            $msg = "success";
            // createMaxsizeImg("{$full_path}", "{$full_path}");
        }
        return ["status"=>$status,"msg"=>$msg,"full_path"=>$full_path,"file_name"=>$file_name,"upload_path"=>$path];
    }

    public function mypage_recipient()
    {
        if ($this->u_no=='') {
            echo "접근할 수 없습니다.";
        } else {
            $result = $this->mypage_model->get_recipient($this->u_no);
            $return = $result;
            echo json_encode($return);
        }
    }

    public function mypage_payment()
    {
        if ($this->u_no=='') {
            echo "접근할 수 없습니다.";
        } else {
            $result = $this->mypage_model->get_payment($this->u_no);
            $return = $result;
            echo json_encode($return);
        }
    }

    public function mypage_prize()
    {
        if ($this->u_no=='') {
            echo "접근할 수 없습니다.";
        } else {
            $result = $this->mypage_model->get_prize($this->u_no);
            $return = $result;
            echo json_encode($return);
        }
    }

    public function mypage_profile()
    {
        if ($this->u_no=='') {
            echo "접근할 수 없습니다.";
        } else {
            $result = $this->mypage_model->get_profile($this->u_no);
            $return = $result;
            echo json_encode($return);
        }
    }

    public function request_taxbill($p_no)
    {
        $result = $this->enroll_model->request_taxbill($p_no);
        echo $this->result_refactoring($result);
    }

    public function enroll_cart()
    {
        $pay_data = $this->enroll_model->get_work_before_payment($this->ss_a_id, $this->u_no);
        echo $this->result_refactoring($pay_data);
    }

    public function enroll_cart_count()
    {
        $pay_data = $this->enroll_model->get_work_before_payment_count($this->ss_a_id, $this->u_no);
        echo $this->result_refactoring($pay_data);
    }

    public function enroll_get_total_enterprise()
    {
        $u_info = $this->user_model->get_user_by_field("u_no", $this->u_no);
        $e_id = $u_info['e_id'];
        $enterprise_count = $this->award_model->cnt_enterprise_enroll($this->ss_a_id, $e_id);
        echo $enterprise_count;
    }

    public function enroll_get_price()
    {
        $pay_data = $this->enroll_model->get_payment_before_price($this->ss_a_id, $this->u_no);
        echo $this->result_refactoring($pay_data);
    }

    public function enroll_work_modify()
    {
        $result = $this->enroll_model->enroll_modify();
        echo $this->result_refactoring($result);
    }

    public function enroll_work_delete()
    {
        $result = $this->enroll_model->enroll_delete();
        echo $this->result_refactoring($result);
    }

    public function enroll_work($w_id=null)
    {
        if ($w_id>0) {
            $result = $this->result_model->get_result($this->ss_a_id, $w_id);
        } else {
            $result = ["status"=>"09","data"=>[],"msg"=>"로그인 이후 등록할 수 있습니다."];
            $userid = $this->session->userdata('u_name');
            if ($userid!="") {
                $images = $_FILES;
                $key_image = [];
                $image_list = [];
                $midx = 0;
                $type = "output/{$this->session->userdata('u_id')}";
                $file_url = get_url($type);
                $result = ["status"=>"09","data"=>[],"msg"=>"어워드정보를 찾을 수 없습니다."];
                if ($this->ss_a_id!="") {
                    if (isset($images['image_key'])) {
                        $ori_name = $images['image_key']['name'];
                        $file_info = pathinfo($ori_name);
                        $file_name = preg_replace("/[ #\&\+\-%@=\/\\\:;,\.'\"\^`~\_|\!\?\*$#<>()\[\]\{\}]/i", "", $file_info['filename']);
                        // $img_name = strtotime(date("Ymdhis"))."_{$file_name}.{$file_info['extension']}";
                        $img_name = strtotime(date("Ymdhis"))."_key.{$file_info['extension']}";
                        $key_visual['file']['name'] = $img_name;
                        $key_visual['file']['type'] = $images['image_key']['type'];
                        $key_visual['file']['tmp_name'] = $images['image_key']['tmp_name'];
                        $key_visual['file']['error'] = $images['image_key']['error'];
                        $key_visual['file']['size'] = $images['image_key']['size'];
                        $upload_result = $this->process_enroll_file($key_visual, $type);
                        $storage_result = storage_upload($type, $upload_result);
                        if ($upload_result['status']=="00") {
                            $key_image = [
                                "file_name"=>$upload_result['file_name'],
                                "origin_name"=>$ori_name,
                                "file_url"=>$file_url
                            ];
                            $img_result = "{$upload_result['upload_path']}/{$upload_result['file_name']}";
                            $img_580 = $this->return_thumbnail($img_result, "580", "348");
                            // $img_1470 = $this->return_thumbnail($img_result, "1470", "882");
                            // $img_1000 = $this->return_thumbnail($img_result, "1000", "1000");
                            storage_upload_exist($type, $img_580, $upload_result['upload_path']);
                            // storage_upload_exist($type, $img_1470, $upload_result['upload_path']);
                            // unlink($upload_result['upload_path']."/".$img_580);
                            // unlink($upload_result['upload_path']."/".$img_1470);
                            // unlink($img_result);
                        }
                    }
                    if (isset($images['image_add'])) {
                        $cpt = count($images['image_add']['name']);
                        for ($i=0; $i<$cpt; $i++) {
                            $ori_name = $images['image_add']['name'][$i];
                            $file_info = pathinfo($ori_name);
                            $file_name = preg_replace("/[ #\&\+\-%@=\/\\\:;,\.'\"\^`~\_|\!\?\*$#<>()\[\]\{\}]/i", "", $file_info['filename']);
                            // $img_name = strtotime(date("Ymdhis"))."_{$file_name}_{$i}.{$file_info['extension']}";
                            $img_name = strtotime(date("Ymdhis"))."_add_{$i}.{$file_info['extension']}";
                            $file_obj['file']['name'] = $img_name;
                            $file_obj['file']['type'] = $images['image_add']['type'][$i];
                            $file_obj['file']['tmp_name'] = $images['image_add']['tmp_name'][$i];
                            $file_obj['file']['error'] = $images['image_add']['error'][$i];
                            $file_obj['file']['size'] = $images['image_add']['size'][$i];
                            $upload_result = $this->process_enroll_file($file_obj, $type);
                            $storage_result = storage_upload($type, $upload_result);
                            if ($upload_result['status']=="00") {
                                $image_list[$midx] = [
                                    "file_name"=>$upload_result['file_name'],
                                    "origin_name"=>$ori_name,
                                    "file_url"=>$file_url
                                ];
                                $midx++;
                                $img_result = "{$upload_result['upload_path']}/{$upload_result['file_name']}";
                                // $img_618 = $this->return_thumbnail($img_result, "618", "841");
                                // $img_1470 = $this->return_thumbnail($img_result, "1470", "2000");
                                // $img_1000 = $this->return_thumbnail($img_result, "1000", "1000");
                                // storage_upload_exist($type, $img_618, $upload_result['upload_path']);
                                // storage_upload_exist($type, $img_1470, $upload_result['upload_path']);
                                // unlink($upload_result['upload_path']."/".$img_618);
                                // unlink($upload_result['upload_path']."/".$img_1470);
                                // unlink($img_result);
                            }
                        }
                    }
                    $result = $this->enroll_model->enroll($this->ss_a_id, $image_list, $key_image, $this->u_no);
                }
            }
        }
        echo $this->result_refactoring($result);
    }

    public function process_enroll_file($file_obj, $path)
    {
        $_FILES = $file_obj;
        $upload_result = $this->file_upload($path);
        // $storage_result = storage_upload($path, $upload_result);
        return $upload_result;
    }

    public function enroll_autofill()
    {
        $user_data = $this->user_model->get_user($this->u_no);
        echo $this->result_refactoring($user_data);
    }

    public function enroll_payment()
    {
        $pay_data = $this->enroll_model->set_payment($this->ss_a_id, $this->u_no);
        echo $this->result_refactoring($pay_data);
    }

    public function enroll_after_price($p_no)
    {
        $pay_data = $this->enroll_model->get_payment_price($p_no);
        echo $pay_data;
    }

    public function payment_payment($p_no)
    {
        $result = $this->payment_model->get_payment($p_no);
        echo $this->result_refactoring($result);
    }

    public function get_user_all()
    {
        $result = $this->user_model->get_user_all();
        echo $this->result_refactoring($result);
    }

    public function service_find_user_id()
    {
        $result = $this->service_model->find_userid();
        echo $this->result_refactoring($result);
    }

    public function service_find_user_email()
    {
        $result = $this->service_model->find_useremail();
        echo $this->result_refactoring($result);
    }

    public function service_islogin()
    {
        $is_login = is_login();
        echo $is_login;
    }

    public function service_isadmin()
    {
        $is_admin = is_admin();
        echo $is_admin;
    }

    public function service_ispermission($access_auth)
    {
        $is_permission = get_access($access_auth);
        echo $is_permission == '00' ? true : false;
    }

    public function get_usersession()
    {
        $usersession = $this->session->userdata();
        $status = "00";
        $msg = "";
        $result = ["status"=>$status,"data"=>$usersession,"msg"=>$msg];
        return $this->result_refactoring($result);
    }

    public function service_join()
    {
        $str_date = date("Y-m-d H:i:s");
        $str_date = strtotime($str_date);
        $auth_code = create_mail_autoricate();
        $result = $this->service_model->set_join($auth_code, $str_date);
        $data = $result['data'];
        $status = $result['status'];
        $msg = $result['msg'];
        if ($data['e_id']>0&&$_FILES) {
            $this->enterprise_registration($data['e_id']);
        }
        if ($status=="00") {
            // $u_id = $data['u_id'];
            // $u_email = $data['u_email'];
            // $user_id = base64_encode($u_id);
            // $domain = "http://{$_SERVER['SERVER_NAME']}";
            // $contents = "<a href='{$domain}/mail_auth/?code={$auth_code}&stamp={$str_date}&idx={$user_id}'>링크이동시 인증이 완료됩니다.</a>";
            // $this->notice->toast_mail($u_email, "test", $contents);
            // $group = "ex";
            // $type = "join";
            $group = "us";
            $type = "new";
            $res_mail = Notice::send_notification("mail", $data['u_no'], $group, $type);
            $res_sms = Notice::send_notification("sms", $data['u_no'], $group, $type);
        }
        $result = ["status"=>$status,"data"=>$data,"msg"=>$msg];
        return $this->result_refactoring($result);
    }

    public function enterprise_registration($e_id)
    {
        $msg = "회사데이터를 확인할 수 없습니다";
        $picture_name = "e_registration";
        $new_picture_name = $e_id."_".time();
        $images = $_FILES;
        $type = "enterprise/registration";
        $file_url = get_url($type);
        $config = [
            "picture_name"=>$picture_name,
            "new_picture_name"=>$new_picture_name,
            "type"=>$type,
            "msg"=>$msg
        ];
        if ($e_id>0) {
            $return = upload_file_in_controller($config);
            $this->user_model->update_registration($e_id, $return['path']);
        } else {
            $return = fail_result($msg);
        }
        //echo json_encode($return);
    }

    public function auth_login()
    {
        $userinfo = $this->service_model->get_userinfo();
        $result = $userinfo['data'];
        $status = "29";
        $newdata = [];
        $user_status = "69";//404
        $msg = "회원정보를 찾을 수 없습니다.";
        if (is_null($result)==false) {
            $msg = "이메일 인증을 진행해 주세요.";
            $user_status = "99";//401
            if ($result['is_del']=='Y') {
                $msg = "삭제된 회원입니다.";
                $user_status = "79";//403
            } elseif ($result['is_rest']=='Y') {
                $msg = "휴면회원 입니다.";
                $user_status = "79";//403
            } elseif ($result['is_auth']=='Y') {
                $user_status = "00";
                $msg = "";
                $newdata = ['u_id' => $result['u_id'],'u_no' => $result['u_no'],'u_name' => $result['u_name'],'e_id' => $result['e_id']];
                if (is_null($result['ua_id'])==false) {
                    $newdata['ua_id'] = $result['ua_id'];
                    $newdata['ua_auth'] = $result['ua_auth'];
                    $newdata['is_super'] = $result['is_super'];
                }
                $this->session->set_userdata($newdata);
                $this->service_model->stamp_lastime($result['u_no']);
                session_regenerate_id(true);
                $headers = headers_list();
                krsort($headers);
                foreach ($headers as $header) {
                    if (!preg_match('~^Set-Cookie: PHPSESSID=~', $header)) {
                        continue;
                    }
                    $header = preg_replace('~; secure(; HttpOnly)?$~', '', $header) . '; secure; SameSite=None';
                    header($header, false);
                    break;
                }
            }
        } else {
            $user_status = $userinfo["code"];
            $newdata['is_exist_id'] = $userinfo['is_exist_id'];
            $newdata['is_right_pw'] = $userinfo['is_right_pw'];
        }
        $result = ["status"=>$user_status,"data"=>$newdata,"msg"=>$msg];
        return $this->result_refactoring($result);
    }

    public function auth_find_userid()
    {
        $result = $this->service_model->find_useremil();
        $status = "29";
        $msg = "회원정보를 찾을 수 없습니다.";
        if (is_null($result)==false) {
            $u_id = str_split($result['u_id']);
            $half = floor((count($u_id)-1)/2);
            $rand1 = rand(0, $half);
            $rand2 = rand($half, count($u_id)-1);
            $encryption_uid = "";
            for ($i=0;$i<count($u_id);$i++) {
                $encryption_uid .= $i==$rand1||$i==$rand2?"*":$u_id[$i];
            }
            $status = "00";
            // $msg = "회원님의 아이디는 {$encryption_uid} 입니다.";
            $msg = $encryption_uid;
        }
        $result = ["status"=>$status,"data"=>[],"msg"=>$msg];
        return $this->result_refactoring($result);
    }

    public function auth_find_userpassword()
    {
        $result = $this->service_model->find_userpassword();
        $status = "29";
        $msg = "회원정보를 찾을 수 없습니다.";
        $auth_code = create_mail_autoricate();
        $str_date = date("Y-m-d H:i:s");
        $str_date = strtotime($str_date);
        if (is_null($result)==false) {
            $u_id = $result['u_id'];
            $this->service_model->update_authcode($u_id, $auth_code);
            // $u_email = $result['u_email'];
            // $user_id = base64_encode($u_id);
            // $domain = "http://{$_SERVER['SERVER_NAME']}";
            // $contents = "<a href='{$domain}/change_password/?code={$auth_code}&stamp={$str_date}&idx={$user_id}'>링크를 클릭하면 비밀번호를 변경할 수 있습니다.</a>";
            // $this->notice->toast_mail($u_email, "test", $contents);
            $group = "ex";
            $type = "password";
            $res_mail = Notice::send_notification("mail", $result['u_no'], $group, $type);
            $res_sms = Notice::send_notification("sms", $result['u_no'], $group, $type);
            $status = "00";
            $msg = "메일을 전송하였습니다.";
        }
        $result = ["status"=>$status,"data"=>[],"msg"=>$msg];
        return $this->result_refactoring($result);
    }

    public function auth_update_userpassword()
    {
        $result = $this->service_model->update_password();
        echo $this->result_refactoring($result);
    }

    public function service_route()
    {
        $route = $this->input->get("award");
        $row = $this->service_model->get_award_by_route($route);
        $status = is_null($row)==false?"00":"29";
        $msg = is_null($row)==false?"어워드 정보를 올바르게 조회하였습니다.":"어워드 정보를 찾을 수 없습니다.";
        $result = ["status"=>$status,"data"=>$row,"msg"=>$msg];
        return $this->result_refactoring($result);
    }

    public function service_category($c_id=false)
    {
        $data = $this->award_model->get_category($this->ss_a_id, $c_id);
        echo $this->result_refactoring($data);
    }
    
    public function service_subcategory($cp_id)
    {
        $data = $this->award_model->get_child_category($this->ss_a_id, $cp_id);
        echo $this->result_refactoring($data);
    }

    public function get_enterprize_allnew()
    {
        $rows = $this->user_model->get_enterprise_allnew();
        echo $this->result_refactoring($rows);
    }

    public function get_enterprize($e_id)
    {
        $rows = $this->user_model->get_enterprise_by_id($e_id);
        echo $this->result_refactoring($rows);
    }

    public function user_enterprize($keyword)
    {
        $rows = $this->user_model->get_enterprise_by_keyword($keyword);
        echo $this->result_refactoring($rows);
    }

    public function service_work($a_id, $w_id)
    {
        $rows = $this->result_model->get_result($a_id, $w_id);
        echo $this->result_refactoring($rows);
    }

    public function service_archive($a_id=false)
    {
        $a_id = $a_id==false?$this->ss_a_id:$a_id;
        $rows = $this->award_model->winner_class($a_id);
        echo $this->result_refactoring($rows);
    }

    public function service_winner_received($a_id)
    {
        $rows = $this->award_model->winner_received($a_id);
        echo $this->result_refactoring($rows);
    }

    public function service_winner_main($a_id, $class, $c_type)
    {
        $rows = $this->award_model->winner_received_main($a_id, $class, $c_type);
        echo $this->result_refactoring($rows);
    }

    public function service_winners($a_id, $c_type)
    {
        $rows = $this->award_model->winner_list($a_id, $c_type);
        echo $this->result_refactoring($rows);
    }

    public function service_winners_special($a_id)
    {
        $rows = $this->award_model->winner_special_list($a_id);
        echo $this->result_refactoring($rows);
    }

    public function service_winner($w_id, $a_id=false)
    {
        $rows = $this->award_model->winner_detail($w_id, $a_id);
        echo $this->result_refactoring($rows);
    }

    public function service_judgement_data($a_id, $judge_route, $j_id)
    {
        $rows = $this->service_model->get_judgement_data($a_id, $judge_route, $j_id);
        echo $this->result_refactoring($rows);
    }

    public function service_judgement_view($a_id, $w_id, $j_id)
    {
        $result = $this->result_model->get_result($a_id, $w_id, $j_id);
        echo $this->result_refactoring($result);
    }

    public function get_judgement($route)
    {
        $result = $this->result_model->get_judgement($route);
        $returnArray = pack_return("00", $result);
        echo json_encode($returnArray);
    }

    public function service_set_judgement($a_id, $w_id)
    {
        $result = $this->result_model->set_judgement($a_id, $w_id);
        echo $this->result_refactoring($result);
    }

    public function service_prize($a_id)
    {
        $a_id = $a_id==false?$this->ss_a_id:$a_id;
        $rows = $this->award_model->get_prize($a_id);
        echo $this->result_refactoring($rows);
    }

    public function service_receiv_prize($a_id)
    {
        $a_id = $a_id==false?$this->ss_a_id:$a_id;
        $rows = $this->award_model->get_receiv_prize($a_id);
        echo $this->result_refactoring($rows);
    }

    public function upload_livefile($path)
    {
        $images = $_FILES;
        $image_list = [];
        $midx = 0;
        $file_url = get_url($path);
        // $cpt = count($_FILES['images']);
        $ori_name = $images['image']['name'];
        $file_info = pathinfo($ori_name);
        $file_name = preg_replace("/[ #\&\+\-%@=\/\\\:;,\.'\"\^`~\_|\!\?\*$#<>()\[\]\{\}]/i", "", $file_info['filename']);
        $img_name = strtotime(date("Ymdhis"))."_{$file_name}.{$file_info['extension']}";
        $_FILES['file']['name'] = $img_name;
        $_FILES['file']['type'] = $images['image']['type'];
        $_FILES['file']['tmp_name'] = $images['image']['tmp_name'];
        $_FILES['file']['error'] = $images['image']['error'];
        $_FILES['file']['size'] = $images['image']['size'];
        $upload_result = $this->file_upload($path);
        return $upload_result;
    }


    public function get_live_contents($type, $menu, $l_id=false)
    {
        $rows = $this->live_model->get_live($type, $menu, $l_id);
        echo $this->result_refactoring($rows);
    }
    
    public function upload_live_contents($type, $menu, $l_id=false)
    {
        $result = ["status"=>"09","data"=>[],"msg"=>"등록에 실패하였습니다. 상세 확인 후 다시 시도해 주세요."];
        
        if ($l_id>0) {
            $l_result = $this->live_model->get_live($type, $menu, $l_id);
            $l_row = $l_result['data'];
            if (count($_FILES)>0) {
                $url = $l_row['l_url'];
                storage_url_delete($url, '');
            }
        }

        $path = "live/{$type}/{$menu}";
        $upload_result = $this->upload_livefile($path);
        $storage_result = storage_upload($path, $upload_result);

        if ($upload_result['status']=="00") {
            $img_result = "{$upload_result['upload_path']}/{$upload_result['file_name']}";
            unlink($img_result);
            rmdir($upload_result['upload_path']);
            $info = ['type'=>$type,'menu'=>$menu,'contents'=>'','url'=>$storage_result['path']];
            $result = $l_id>0?$this->live_model->modify_live($l_id, $info):$this->live_model->insert_live($info);
        }
        echo $this->result_refactoring($result);
    }

    public function update_live_contents($type, $menu, $l_id=false)
    {
        $result = ["status"=>"09","data"=>[],"msg"=>"등록에 실패하였습니다. 상세 확인 후 다시 시도해 주세요."];
        $contents = $this->input->post("contents");
        if ($menu == 'text') {
            $content = $contents;
            $url = '';
        } elseif ($menu == 'button') {
            $contents = json_decode($contents, true);
            $content = $contents['text'];
            $url = $contents['url'];
        } elseif ($menu == 'video') {
            $contents = json_decode($contents, true);
            $content = '';
            $url = $contents['url'];
        }
        $info = ['type'=>$type,'menu'=>$menu,'contents'=>$content,'url'=>$url];
        $result = $l_id>0?$this->live_model->modify_live($l_id, $info):$this->live_model->insert_live($info);
        echo $this->result_refactoring($result);
    }

    public function notice_restuser()
    {
        $result = $this->user_model->get_user(false, 'Y');
        $userlist = $result['data'];
        foreach ($userlist as $user) {
            if ($user['is_rest_notice']=='N') {
                $u_no = $user['u_no'];
                $group = "us";
                $type = "rest";
                $res_sms = Notice::send_notification("sms", $u_no, $group, $type);
                $res_mail = Notice::send_notification("mail", $u_no, $group, $type);
                $this->user_model->update_rest_notice($u_no);
            }
        }
    }

    public function notice_paycancel()
    {
        $result = $this->payment_model->get_cancel_payment();
        $paylist = $result;
        foreach ($paylist as $pay) {
            $u_no = $pay['u_no'];
            $p_no = $pay['p_no'];
            $group = "pa";
            $type = "over";
            $res_sms = Notice::send_notification("sms", $u_no, $group, $type);
            $res_mail = Notice::send_notification("mail", $u_no, $group, $type);
            $this->payment_model->update_status_payment($p_no);
        }
    }


    public function service_gallery($a_id, $ab_id=false)
    {
        $result = $this->aboard_model->get_aboard('gallery', $a_id, $ab_id);
        echo $return = $this->result_refactoring($result);
    }

    public function service_speech($a_id, $ab_id=false)
    {
        $result = $this->aboard_model->get_aboard('speech', $a_id, $ab_id);
        echo $return = $this->result_refactoring($result);
    }

    public function service_terms($id)
    {
        $result = $this->terms_model->get_terms($id);
        echo $return = $this->result_refactoring($result);
    }

    public function service_popup($a_id)
    {
        $result = $this->popup_model->get_popup($a_id);
        echo $return = $this->result_refactoring($result);
    }

    public function test_image()
    {
        $data = array(
            "post"  => $_POST,
            "files" => $_FILES
        );
        echo json_encode($data);
    }

    public function storage_download()
    {
        $dir = $this->input->get("dir");
        $path = $this->input->get("path");
        $filename = $this->input->get("filename");
        //$res = storage_file_download($dir, $filename);
        $res = storage_path_download($path, $filename);
        $file_content = file_get_contents($res);
        $this->load->helper('download');
        unlink("./".$res);
        force_download($filename, $file_content);
    }

    public function delete_user()
    {
        $result = $this->user_model->delete_user();
        echo $return = $this->result_refactoring($result);
    }

    /* 아카이브 최근년도 초기값 조회 */
    public function get_archive_year()
    {
        $a_row = $this->award_model->get_archive_year();
        echo json_encode($a_row);
    }

    /* 아카이브 수상년도 리스트 조회 */
    public function get_archive_year_array()
    {
        $a_row = $this->award_model->get_archive_year_array();
        $yearArr = array();
        $i = 0;
        foreach ($a_row as $row) {
            $yearArr[$i] = $row->a_year;
            $i++;
        }
        echo json_encode($yearArr);
    }

    /* 아카이브 수상작 카테고리 목록 조회 */
    public function service_winners_category($year, $type)
    {
        $a_row = $this->award_model->service_winners_category($year, $type);
        $yearArr = array();
        $i = 0;
        foreach ($a_row as $row) {
            $yearArr[$i] = $row;
            $i++;
        }
        echo json_encode($yearArr);
    }

    /* 수상작 카테고리 상세 조회 */
    public function service_winners_choice($a_id, $c_type, $c_id)
    {
        $rows = $this->award_model->winner_list_choice($a_id, $c_type, $c_id);
        echo $this->result_refactoring($rows);
    }
}