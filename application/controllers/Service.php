<?php
class Service extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('award_model');
        $this->load->model('admin_model');
        $this->load->model('user_model');
        $this->load->model('service_model');
        $this->load->model('terms_model');
        $this->load->model('enroll_model');
        $this->load->library('session');
        $this->load->library('Notice');
        $this->load->helper('common');
        $this->router = $this->uri->segment(2)?$this->uri->segment(2):"";
        $this->load->database();
        $this->ss_a_id = $this->uri->segment(2)?$this->uri->segment(2):get_award_session();
        $this->u_no = $this->session->userdata('u_no');
        if ($this->router!="") {
            $row = $this->service_model->get_award_by_route($this->router);
            if (is_null($row)==false) {
                $this->ss_a_id = $row['a_id'];
            }
            // else {
            //     $row = $this->award_model->get_current_award();
            //     if (is_null($row)==false) {
            //         $this->ss_a_id = $row['a_id'];
            //     }
            // }
        }
    }
    public function index()
    {
        redirect('/main');
    }

    public function award_redirect()
    {
        $award_router = $this->uri->segment(1);
        $result = $this->service_model->get_award_router($award_router);
        if (is_null($result)==true) {
            // $result = $this->service_model->get_before_award();
            show_404();
        }
        redirect("/{$result['award_route']}/{$result['a_id']}");
    }

    public function info_award()
    {
        $a_id = $this->router!="" ? $this->router : ($this->ss_a_id!="" ? $this->ss_a_id : "");
        if ($a_id) {
            // $result = $this->award_model->get_award($a_id);
            // $return = ["data"=>$result["data"]];
            //print_r($return['data']);
            $this->load->view('vue/index');
        } else {
            redirect('/main');
        }
    }

    public function preview($w_id)
    {
        $this->load->view('vue/index');
    }

    public function judges()
    {
        $result = $this->service_model->get_judge($this->ss_a_id);
        $return = $result['data'];
        echo json_encode($return);
        $this->load->view('vue/index');
    }

    public function category()
    {
        // $result = $this->award_model->get_category($this->ss_a_id);
        // $returnArray = pack_return("00", $result);
        // echo json_encode($returnArray);
        $this->load->view('vue/index');
    }

    public function terms($id)
    {
        $this->load->view('vue/index');
    }
    
    public function main()
    {
        $this->load->view('vue/index');
    }

    public function about()
    {
        $this->load->view('vue/index');
    }

    public function info_prize()
    {
        $data = $this->award_model->get_prize($this->ss_a_id);
        $returnArray = pack_return("00", $data);
        echo json_encode($returnArray);
    }
    
    public function receiv_prize()
    {
        $data = $this->award_model->get_receiv_prize($this->ss_a_id);
        $returnArray = pack_return("00", $data);
        echo json_encode($returnArray);
    }

    public function receiv_prize_detail($zr_id=false)
    {
        $data = $this->award_model->get_receiv_prize($this->ss_a_id, $zr_id);
        $returnArray = pack_return("00", $data);
        echo json_encode($returnArray);
    }

    public function get_olduser_info()
    {
        if ($this->input->server('REQUEST_METHOD')=="POST") {
            $data = $this->user_model->get_olduser_auth();
            // print_r($data);
            redirect("/join?user_id={$data['UserId']}") ;
        } else {
            $this->load->view('vue/index');
        }
    }

    public function join()
    {
        if ($this->u_no>0) {
            redirect("/main");
        } else {
            $this->load->view('vue/index');
        }
    }

    public function find_userinfo()
    {
        if ($this->u_no>0) {
            redirect("/main");
        } else {
            $this->load->view('vue/index');
        }
    }

    public function change_password()
    {
        $this->load->view('vue/index');
    }

    public function faq()
    {
        $this->load->view('vue/index');
    }
    public function testimg()
    {
        $this->load->view('vue/index');
    }

    public function olduser()
    {
        $data = $this->service_model->get_olduserinfo();
        // print_r($data);
    }

    // public function judgement($judge_route)
    // {
    //     // $data = $this->service_model->get_judgement_data($this->ss_a_id, $judge_route);
    //     // echo json_encode($data);
    //     $is_check_access = $this->award_model->get_judge_by_field('j_access', $judge_route);
    //     if (is_null($is_check_access)==false) {
    //         if ($is_check_access['a_id'] == $this->ss_a_id) {
    //             $this->load->view('vue/index');
    //         } else {
    //             show_404();
    //         }
    //     } else {
    //         show_404();
    //     }
    // }

    public function judgement($judge_route)
    {
        // $data = $this->service_model->get_judgement_data($this->ss_a_id, $judge_route);
        // echo json_encode($data);
        $is_check_access = $this->award_model->get_judge_party_by_field('jp_access', $judge_route);
        if (is_null($is_check_access)==false) {
            if ($is_check_access['a_id'] == $this->ss_a_id) {
                $this->load->view('vue/index');
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function give_prize()
    {
        $data = $this->service_model->give_prize();
        // print_r($data);
    }

    public function archive()
    {
        $this->load->view('vue/index');
    }

    public function winners()
    {
        $award_router = $this->uri->segment(2);
        if ($award_router) {
            $result = $this->service_model->get_award_exist($award_router);
            if (is_null($result)==true) {
                show_404();
            } else {
                $this->load->view('vue/index');
            }
        } else {
            $a_row = $this->award_model->get_current_award();
            redirect('/winners/main/'.$a_row['a_id']);
        }
    }

    public function winner($w_id)
    {
        $this->load->view('vue/index');
    }

    public function mail_auth()
    {
        $result = $this->service_model->authorize_mail();
        if ($result['status']=="00") {
            $u_no = $result['data']['u_no'];
            $group = "us";
            $type = "new";
            // $res_sms = Notice::send_notification("sms", $u_no, $group, $type);
            // $res_mail = Notice::send_notification("mail", $u_no, $group, $type);
        }
        redirect('/login');
    }

    public function check_admin()
    {
        if (is_admin()==false) {
            echo "<script>alert('관리자 유저가 아닙니다!');</script>";
            return false;
        }
        return true;
    }

    public function scheduler(Type $var = null)
    {
        $this->user_model->update_rest_notice();
        $this->user_model->update_delete_by_schedule();
        $this->enroll_model->delete_bank_payment();
    }

    public function browser()
    {
        $this->load->view('common/browser');
    }
}
