<?php
class Mypage extends CI_Controller
{
    public $u_no = 0;//$this->session->userdata('u_no');
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mypage_model');
        $this->load->library('session');
        $this->u_no = $this->session->userdata('u_no');
    }

    public function index()
    {
        if ($this->u_no=='') {
            // echo "접근할 수 없습니다.";
            redirect("/login?redirect=/mypage");
        } else {
            if ($this->input->get('api')) {
                $limit_count = 5;
                $recipient_summary = $this->mypage_model->get_recipient($this->u_no, $limit_count);
                $payment_summary = $this->mypage_model->get_payment($this->u_no, $limit_count);
                $prize_summary = $this->mypage_model->get_prize($this->u_no, $limit_count);
                $return = ["data"=>
                        [
                            "recipients"=>$recipient_summary,
                            "payments"=>$payment_summary,
                            "prizes"=>$prize_summary
                        ]
                    ];
                echo json_encode($return);
            } else {
                $this->load->view('vue/index');
            }
        }
    }
    
    public function recipient_history()
    {
        if ($this->u_no=='') {
            // echo "접근할 수 없습니다.";
            redirect("/login?redirect=/mypage");
        } else {
            $this->load->view('vue/index');
        }
    }

    public function payment_history()
    {
        if ($this->u_no=='') {
            // echo "접근할 수 없습니다.";
            redirect("/login?redirect=/mypage");
        } else {
            $this->load->view('vue/index');
        }
    }
    
    public function prize_history()
    {
        if ($this->u_no=='') {
            // echo "접근할 수 없습니다.";
            redirect("/login?redirect=/mypage");
        } else {
            $this->load->view('vue/index');
        }
    }

    public function profile()
    {
        if ($this->u_no=='') {
            // echo "접근할 수 없습니다.";
            redirect("/login?redirect=/mypage");
        } else {
            $this->load->view('vue/index');
        }
    }


    public function modify()
    {
        $result = $this->mypage_model->set_profile($this->u_no);
        redirect("mypage/profile");
    }
}
