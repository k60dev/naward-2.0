<?php
class ApiAward extends CI_Controller
{
    public $return_data = [];
    public $return_status = "99";
    public $return_msg = "";
    public $access_code;
    public $access_keyword = "aw";
    public $ss_a_id = false;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('award_model');
        $this->load->model('admin_model');
        $this->load->model('popup_model');
        $this->load->model('receiv_model');
        $this->load->model('aboard_model');
        $this->load->model('result_model');
        $this->load->library('session');
        $this->load->helper(array('form', 'url','common','upload','thumbnail'));
        $this->ss_a_id = $this->uri->segment(3)?$this->uri->segment(3):get_award_session();
        $this->access_code = get_access($this->access_keyword);
    }
    public function result_refactoring($result)
    {
        $this->return_status = $result['status']?$result['status']:$this->return_status;
        $this->return_data = $result['data']?$result['data']:$this->return_data;
        $this->return_msg = $result['msg']?$result['msg']:$this->return_msg;
        return return_refactoring($this->return_status, $this->return_data, $this->return_msg, $this->access_code);
    }

    public function index()
    {
        $result = $this->award_model->get_award();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function list()
    {
        $result = $this->award_model->get_award();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function view()
    {
        $result = $this->award_model->get_award($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function create()
    {
        $result = $this->award_model->create_award();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function onair()
    {
        $result = $this->award_model->onair_award();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function modify()
    {
        $type = "award/logo";
        $upload_result = upload_local_file($type);
        $logo_path = "";
        if ($upload_result['status']=="00") {
            $storage_result = storage_upload($type, $upload_result);
            $img_result = "{$upload_result['upload_path']}/{$upload_result['file_name']}";
            unlink($img_result);
            rmdir($upload_result['upload_path']);
            $logo_path = $storage_result['path'];
        }
        $result = $this->award_model->modify_award($logo_path);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function alter()
    {
        $result = $this->award_model->alter_award($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function descript()
    {
        $result = $this->award_model->modify_descript_award();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }
    public function remove()
    {
        $result = $this->award_model->remove_award();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function dashboard()
    {
        $result = $this->award_model->get_award_dashboard($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }



    /* judge */
    public function judge()
    {
        $result = $this->award_model->get_judge($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function view_judge($j_id)
    {
        $result = $this->award_model->get_judge($this->ss_a_id, $j_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function create_judge()
    {
        $result = $this->award_model->create_judge();
        $return = $this->result_refactoring($result);
        if ($return['data']['j_id']>0&&$_FILES) {
            $this->upload_judge_picture($return['data']['j_id']);
        }
        echo json_encode($return);
    }

    public function modify_judge()
    {
        $result = $this->award_model->modify_judge();
        $return = $this->result_refactoring($result);
        if ($this->input->post('j_id')>0&&$_FILES) {
            $this->upload_judge_picture($return['data']['j_id']);
        }
        echo json_encode($return);
    }

    public function modify_judge_order()
    {
        $result = $this->award_model->modify_judge_order();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function judge_party()
    {
        $result = $this->award_model->get_judge_party($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function view_judge_party($jp_id)
    {
        $result = $this->award_model->get_judge_party($this->ss_a_id, $jp_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function judge_party_all()
    {
        $result = $this->award_model->get_judge_party_all($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function create_judge_party()
    {
        $result = $this->award_model->create_judge_party();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function modify_judge_party()
    {
        $result = $this->award_model->modify_judge_party();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function create_access()
    {
        $result = $this->award_model->create_access();
        echo $result;
    }

    public function remove_judge()
    {
        $result = $this->award_model->remove_judge($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function remove_judge_party()
    {
        $result = $this->award_model->remove_judge_party();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function upload_judge_picture($j_id)
    {
        $picture_name = "j_picture";
        $new_picture_name = $j_id."_".time();
        $images = $_FILES;
        $type = "award/judge/{$this->ss_a_id}";
        $file_url = get_url($type);
        $config = [
            "picture_name"=>$picture_name,
            "new_picture_name"=>$new_picture_name,
            "type"=>$type,
            "msg"=>"심사위원 정보가 없습니다.",
            "a_id"=>$this->ss_a_id
        ];
        if ($j_id>0) {
            $return = upload_file_in_controller($config);
            $this->award_model->update_jpicture($j_id, $return['path']);
        } else {
            $return = fail_result("심사위원 정보를 확인할 수 없습니다.");
        }
        echo json_encode($return);
    }

    /* category */
    public function category()
    {
        $result = $this->award_model->get_category($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function view_category($c_id)
    {
        $result = $this->award_model->get_category($this->ss_a_id, $c_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function subcategory()
    {
        $result = $this->award_model->get_child_category($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function view_subcategory($cp_id)
    {
        $result = $this->award_model->get_child_category($this->ss_a_id, $cp_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function create_category($cp_id=0)
    {
        $result = $this->award_model->create_category($cp_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }
    
    public function modify_category()
    {
        $result = $this->award_model->modify_category();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function modify_category_order()
    {
        $result = $this->award_model->modify_category_order();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function remove_category()
    {
        $result = $this->award_model->remove_category();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    /* prize */
    public function prize()
    {
        $result = $this->award_model->get_prize($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function view_prize($z_id)
    {
        $result = $this->award_model->get_prize($this->ss_a_id, $z_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }
    
    public function create_prize()
    {
        $result = $this->award_model->create_prize();
        $return = $this->result_refactoring($result);
        if ($return['data']['z_id']>0&&$_FILES) {
            $this->upload_prize_picture($return['data']['z_id']);
        }
        echo json_encode($return);
    }
    
    public function modify_prize()
    {
        $result = $this->award_model->modify_prize();
        $return = $this->result_refactoring($result);
        if ($this->input->post('z_id')>0&&$_FILES) {
            $this->upload_prize_picture($return['data']['z_id']);
        }
        echo json_encode($return);
    }

    public function upload_prize_picture($z_id)
    {
        $picture_name = "z_picture";
        $new_picture_name = $z_id."_".time();
        $images = $_FILES;
        $type = "award/prize/{$this->ss_a_id}";
        $file_url = get_url($type);
        $config = [
            "picture_name"=>$picture_name,
            "new_picture_name"=>$new_picture_name,
            "type"=>$type,
            "msg"=>"수상클래스 정보가 없습니다.",
            "a_id"=>$this->ss_a_id
        ];
        if ($z_id>0) {
            $return = upload_file_in_controller($config);
            $this->award_model->update_prize($z_id, $return['path']);
        } else {
            $return = fail_result("수상클래스 정보를 확인할 수 없습니다.");
        }
        echo json_encode($return);
    }
    
    public function remove_prize()
    {
        $result = $this->award_model->remove_prize();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }


    public function popup()
    {
        $result = $this->popup_model->get_popup($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function view_popup($pop_id)
    {
        $result = $this->popup_model->get_popup($this->ss_a_id, $pop_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function create_popup()
    {
        $result = $this->popup_model->create_popup();
        $return = $this->result_refactoring($result);
        if ($return['data']['pop_id']>0&&$_FILES) {
            $this->upload_popup_picture($return['data']['pop_id']);
        }
        echo json_encode($return);
    }

    public function modify_popup()
    {
        $result = $this->popup_model->modify_popup();
        $return = $this->result_refactoring($result);
        if ($this->input->post('pop_id')>0&&$_FILES) {
            $this->upload_popup_picture($return['data']['pop_id']);
        }
        echo json_encode($return);
    }

    public function remove_popup($pop_id)
    {
        $result = $this->popup_model->remove_popup($pop_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function upload_popup_picture($pop_id)
    {
        $picture_name = "pop_picture";
        $new_picture_name = $pop_id."_".time();
        $images = $_FILES;
        $type = "award/popup/{$this->ss_a_id}";
        $file_url = get_url($type);
        $config = [
            "picture_name"=>$picture_name,
            "new_picture_name"=>$new_picture_name,
            "type"=>$type,
            "msg"=>"팝업 정보가 없습니다.",
            "a_id"=>$this->ss_a_id
        ];
        if ($pop_id>0) {
            $return = upload_file_in_controller($config);
            $this->popup_model->update_popup_picture($pop_id, $return['path']);
        } else {
            $return = fail_result("팝업 정보를 확인할 수 없습니다.");
        }
    }

    public function results($status=false)
    {
        $result = $this->result_model->get_result($this->ss_a_id, false, false, $status);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function cart()
    {
        $result = $this->result_model->get_cart($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function results_category($w_id)
    {
        $result = $this->result_model->get_category_in_result($this->ss_a_id, $w_id);
        echo json_encode($result);
    }

    public function results_view($w_id)
    {
        $result = $this->result_model->get_result($this->ss_a_id, $w_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function results_patch($status)
    {
        $result = $this->result_model->batch_result($status);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function results_judges($w_id)
    {
        $result = $this->result_model->get_judge_in_result($this->ss_a_id, $w_id);
        echo json_encode($result);
    }

    public function results_judges_party($w_id)
    {
        $result = $this->result_model->get_judge_party_in_result($this->ss_a_id, $w_id);
        echo json_encode($result);
    }

    public function results_judge_party_exist()
    {
        $result = $this->result_model->get_judge_party_exist_in_result($this->ss_a_id);
        echo json_encode($result);
    }

    public function results_out_judges($w_id)
    {
        $result = $this->result_model->get_judge_out_result($this->ss_a_id, $w_id);
        echo json_encode($result);
    }

    public function results_judge($w_id)
    {
        $result = $this->result_model->set_judge($w_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function results_judge_onlist()
    {
        $result = $this->result_model->set_judge_onlist();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function results_judge_party_onlist()
    {
        $result = $this->result_model->set_judge_party_onlist();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function results_judge_party_reset()
    {
        $result = $this->result_model->set_judge_party_reset();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function results_remove()
    {
        $result = $this->result_model->remove_result();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function results_receiv()
    {
        $result = $this->result_model->win_result();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function result_judgement()
    {
        $result = $this->result_model->get_result_judgement($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function winners($status=false)
    {
        $result = $this->receiv_model->get_receiv($this->ss_a_id, false, $status);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function winner($zr_id)
    {
        $result = $this->receiv_model->get_receiv($this->ss_a_id, $zr_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function winner_output($w_id)
    {
        $result = $this->receiv_model->get_work_output($w_id);
        echo json_encode($result);
    }

    public function winner_prize($w_id)
    {
        $result = $this->receiv_model->get_receiv_prize($this->ss_a_id, $w_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function update_winner_comment($zr_id)
    {
        $result = $this->receiv_model->update_comment($zr_id);
        echo json_encode($result);
    }

    public function patch_winner($status)
    {
        $result = $this->receiv_model->batch_receiv($status);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function request_winner()
    {
        $result = $this->receiv_model->request_receiv();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function gallery($ab_id=false)
    {
        $result = $this->aboard_model->get_aboard('gallery', $this->ss_a_id, $ab_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function speech($ab_id=false)
    {
        $result = $this->aboard_model->get_aboard('speech', $this->ss_a_id, $ab_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function create_aboard($type)
    {
        $result = $this->aboard_model->create_aboard($type);
        $return = $this->result_refactoring($result);
        if ($return['data']['ab_id']>0&&$_FILES) {
            $this->upload_aboard_picture($return['data']['ab_id'], $type);
        }
        echo json_encode($return);
        /*
        if ($this->access_code=="00") {
            $path = "aboard/{$type}";
            $upload_result = upload_local_file($path);
            $img = "";
            if ($upload_result['status']=="00") {
                $storage_result = storage_upload($path, $upload_result);
                $img_result = "{$upload_result['upload_path']}/{$upload_result['file_name']}";
                unlink($img_result);
                rmdir($upload_result['upload_path']);
                $img = $storage_result['path'];
            }
            $this->return_status = $result['status'];
            $this->return_data = $result['data'];
        }
        $returnArray = pack_return($this->return_status, $this->return_data);
        echo json_encode($returnArray);*/
    }

    public function modify_aboard($type)
    {
        $result = $this->aboard_model->modify_aboard($type);
        $return = $this->result_refactoring($result);
        if ($return['data']['ab_id']>0&&$_FILES) {
            $this->upload_aboard_picture($return['data']['ab_id'], $type);
        }
        echo json_encode($return);
        /*
        if ($this->access_code=="00") {
            $path = "aboard/{$type}";
            $upload_result = upload_local_file($path);
            $img = "";
            if ($upload_result['status']=="00") {
                $storage_result = storage_upload($path, $upload_result);
                $img_result = "{$upload_result['upload_path']}/{$upload_result['file_name']}";
                unlink($img_result);
                rmdir($upload_result['upload_path']);
                $img = $storage_result['path'];
            }
            $this->return_status = $result['status'];
            $this->return_data = $result['data'];
        }
        $returnArray = pack_return($this->return_status, $this->return_data);
        echo json_encode($returnArray);
        */
    }

    
    public function upload_aboard_picture($ab_id, $ab_type)
    {
        $picture_name = "ab_thumb";
        $new_picture_name = $ab_id."_".time();
        $images = $_FILES;
        $type = "aboard/{$this->ss_a_id}/{$ab_type}";
        $file_url = get_url($type);
        $config = [
            "picture_name"=>$picture_name,
            "new_picture_name"=>$new_picture_name,
            "type"=>$type,
            "msg"=>"게시글 정보가 없습니다.",
            "a_id"=>$this->ss_a_id
        ];
        if ($ab_id>0) {
            $return = upload_file_in_controller($config);
            $this->aboard_model->upload_aboard_picture($ab_id, $return['path']);
        } else {
            $return = fail_result("게시글 정보를 확인할 수 없습니다.");
        }
    }

    public function fetch_aboard()
    {
        $access = get_access($this->access_keyword);
        $data = $this->aboard_model->fetch_aboard();
        $returnArray = pack_return($access, $data);
        echo json_encode($returnArray);
        if ($this->access_code=="00") {
            $a_id = $this->ss_a_id;
            $result = $this->aboard_model->fetch_aboard();
            $this->return_status = $result['status'];
            $this->return_data = $result['data'];
        }
        $returnArray = pack_return($this->return_status, $this->return_data);
        echo json_encode($returnArray);
    }

    public function remove_aboard($type)
    {
        if ($this->access_code=="00") {
            $a_id = $this->ss_a_id;
            $result = $this->aboard_model->remove_aboard($type);
            $this->return_status = $result['status'];
            $this->return_data = $result['data'];
        }
        $returnArray = pack_return($this->return_status, $this->return_data);
        echo json_encode($returnArray);
    }

    
    public function create_gallery()
    {
        $data = $this->create_aboard("gallery");
        echo $data;
    }
    
    public function modify_gallery()
    {
        $data = $this->modify_aboard("gallery");
        echo $data;
    }
    
    public function remove_gallery()
    {
        $data = $this->remove_aboard("gallery");
        echo $data;
    }
    
    
    public function create_speech()
    {
        $data = $this->create_aboard("speech");
        echo $data;
    }
    
    public function modify_speech()
    {
        $data = $this->modify_aboard("speech");
        echo $data;
    }
    
    public function remove_speech()
    {
        $data = $this->remove_aboard("speech");
        echo $data;
    }
}
