<?php
class Auth extends CI_Controller
{
    public $ss_a_id = false;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('award_model');
        $this->load->model('admin_model');
        $this->load->model('user_model');
        $this->load->model('service_model');
        $this->load->model('terms_model');
        $this->load->library('session');
        $this->load->library('user_agent');
        $this->load->helper('common');
        $this->router = $this->uri->segment(1)?$this->uri->segment(1):"";
        $this->load->database();
        $this->ss_a_id = get_award_session();
        $this->u_no = $this->session->userdata('u_no');
        if ($this->router!="") {
            $query = "select * from tb_award where award_route = '{$this->router}' order by a_id desc limit 1";
            $result =  $this->db->query($query);
            $row = $result->row_array();
            $this->ss_a_id = $row['a_id']?$row['a_id']:get_award_session();
        }
    }

    public function login()
    {
        if ($this->input->server('REQUEST_METHOD')=="POST") {
            $result = $this->service_model->get_userinfo();
            if (is_null($result)==false) {
                $newdata = [
                    'u_id' => $result['u_id'],
                    'u_no' => $result['u_no'],
                    'u_name' => $result['u_name'],
                    'e_id' => $result['e_id']
                ];
                $this -> session -> set_userdata($newdata);
                $result = $this->service_model->stamp_lastime($result['u_no']);
                redirect($this->agent->referrer());
            } else {
                echo "회원정보를 찾을 수 없습니다.";
                $this->load->view('vue/index');
            }
        } else {
            if ($this->u_no>0) {
                redirect($this->agent->referrer());
            } else {
                $this->load->view('vue/index');
            }
        }
    }

    public function logout()
    {
        $newdata = [
            'u_id' => '',
            'u_no' => '',
            'u_name' => '',
            'e_id' => ''
        ];
        $this->session->unset_userdata($newdata);
        $this->session->sess_destroy();
        $sessions = $this->session->all_userdata();
        redirect($this->agent->referrer());
    }


    public function _auto_delete_user()
    {
        $this->service_model->auto_delete_user();
    }
}
