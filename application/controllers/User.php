<?php
class User extends CI_Controller
{
    public $return_data = [];
    public $return_status = "99";
    public $return_msg = "";
    public $access_keyword = "us";
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->library('session');
        $this->load->helper('common');
        //$this->output->enable_profiler(TRUE);
        $this->ss_a_id = $this->uri->segment(3)?$this->uri->segment(3):get_award_session();
        $this->access_code = get_access($this->access_keyword);
    }

    public function result_refactoring($result)
    {
        $this->return_status = $result['status']?$result['status']:$this->return_status;
        $this->return_data = $result['data']?$result['data']:$this->return_data;
        $this->return_msg = $result['msg']?$result['msg']:$this->return_msg;
        return return_refactoring($this->return_status, $this->return_data, $this->return_msg, $this->access_code);
    }

    public function admin(Type $var = null)
    {
        $this->load->view('vue/index');
    }

    public function main($rest=false)
    {
        // if ($rest!==false) {
        //     $result = $this->user_model->get_user(false, $rest);
        // } else {
        //     $result = $this->user_model->get_user();
        // }
        // $return = $this->result_refactoring($result);
        //echo json_encode($return);

        if (!$this->session->userdata('u_no')) {
            redirect('/login?redirect=/admin/dashboard');
        } else {
            // if (isset($_SESSION['ua_id'])==false) {
            if (!$this->session->userdata('ua_id')) {
                $newdata = [
                    'u_id' => '',
                    'u_no' => '',
                    'u_name' => '',
                    'e_id' => ''
                ];
                $this->session->unset_userdata($newdata);
                $this->session->sess_destroy();
                $sessions = $this->session->all_userdata();
                redirect('/main');
            } else {
                $this->load->view('vue/index');
            }
        }
    }
    
    public function view($u_no=0)
    {
        $result = $this->user_model->get_user($u_no);
        $return = $this->result_refactoring($result);
        //echo json_encode($return);

        $this->load->view('vue/index');
    }

    public function create()
    {
        $result = $this->user_model->insert_user();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function modify()
    {
        $result = $this->user_model->update_user();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function patch_rest()
    {
        $result = $this->user_model->patch_user("is_rest", "Y");
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function patch_active()
    {
        $result = $this->user_model->patch_user("is_rest", "N");
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function remove()
    {
        $result = $this->user_model->patch_user("is_del", "Y");
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function view_enterprise($e_id)
    {
        $data = $this->user_model->get_user($e_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }
}
