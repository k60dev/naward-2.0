<?php
class Receiv extends CI_Controller {

	public $return_data = [];
	public $return_status = "99";
	public $return_msg = "";
	public $access_code;
	public $access_keyword = "aw";	
	public $ss_a_id = FALSE;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('receiv_model');
		$this->load->model('admin_model');
		$this->load->library('session');
		$this->load->helper('common');
		$this->ss_a_id = $this->uri->segment(3)?$this->uri->segment(3):get_award_session();
		$this->access_code = get_access($this->access_keyword);
	}

	public function result_refactoring($result){
		$this->return_status = $result['status']?$result['status']:$this->return_status;
		$this->return_data = $result['data']?$result['data']:$this->return_data;
		$this->return_msg = $result['msg']?$result['msg']:$this->return_msg;
		return return_refactoring($this->return_status,$this->return_data,$this->return_msg,$this->access_code);
	}

	public function index(){
		$result = $this->receiv_model->get_receiv($this->ss_a_id);
		$return = $this->result_refactoring($result);
		echo json_encode($return);
	}

	public function view($zr_id){
		$result = $this->receiv_model->get_receiv($this->ss_a_id,$zr_id);
		$return = $this->result_refactoring($result);
		echo json_encode($return);
	}

	public function patch(){
		$result = $this->receiv_model->batch_receiv();
		$return = $this->result_refactoring($result);
		echo json_encode($return);
	}

	public function request(){		
		$result = $this->receiv_model->request_receiv();
		$return = $this->result_refactoring($result);
		echo json_encode($return);
	}

}
?>