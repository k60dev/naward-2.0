<?php
class Admin extends CI_Controller
{
    public $return_data = [];
    public $return_status = "99";
    public $return_msg = "";
    public $access_code;
    public $access_keyword = "ua";
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('admin_model');
        $this->load->library('session');
        $this->load->helper('common');
        $this->access_code = get_access($this->access_keyword);
    }

    public function result_refactoring($result)
    {
        $this->return_status = $result['status']?$result['status']:$this->return_status;
        $this->return_data = $result['data']?$result['data']:$this->return_data;
        $this->return_msg = $result['msg']?$result['msg']:$this->return_msg;
        return return_refactoring($this->return_status, $this->return_data, $this->return_msg, $this->access_code);
    }

    public function main()
    {

        if (!$this->session->userdata('u_no')) {
            redirect('/login?redirect=/admin/dashboard');
        } else {
            // if (isset($_SESSION['ua_id'])==false) {
            if (!$this->session->userdata('ua_id')) {
                $newdata = [
                    'u_id' => '',
                    'u_no' => '',
                    'u_name' => '',
                    'e_id' => ''
                ];
                $this->session->unset_userdata($newdata);
                $this->session->sess_destroy();
                $sessions = $this->session->all_userdata();
                redirect('/main');
            } else {;
                $this->load->view('vue/index');
            }
        }

    }

    public function view($ua_id)
    {
        $this->load->view('vue/index');
    }

    public function create()
    {
        $result = $this->admin_model->create_admin();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function modify()
    {
        $result = $this->admin_model->modify_admin();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function patch()
    {
        $result = $this->admin_model->patch_admin("is_super", "Y");
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function logs()
    {
        $data = $this->admin_model->get_logs();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function logging()
    {
        $data = $this->admin_model->logging_admin();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }
}
