<?php
class Award extends CI_Controller
{
    public $return_data = [];
    public $return_status = "99";
    public $return_msg = "";
    public $access_code;
    public $access_keyword = "aw";
    public $ss_a_id = false;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('award_model');
        $this->load->model('admin_model');
        $this->load->library('session');
        $this->load->helper(array('form', 'url','common','upload','thumbnail'));
        $this->ss_a_id = $this->uri->segment(3)?$this->uri->segment(3):get_award_session();
        $this->access_code = get_access($this->access_keyword);
    }

    public function result_refactoring($result)
    {
        $this->return_status = $result['status']?$result['status']:$this->return_status;
        $this->return_data = $result['data']?$result['data']:$this->return_data;
        $this->return_msg = $result['msg']?$result['msg']:$this->return_msg;
        return return_refactoring($this->return_status, $this->return_data, $this->return_msg, $this->access_code);
    }

    public function index()
    {
        $data = $this->award_model->get_award();
        $return = $this->result_refactoring($data);
        echo json_encode($return);
    }

    public function list()
    {
        // $data = $this->award_model->get_award();
        // $return = $this->result_refactoring($result);
        // echo json_encode($return);

        if (!$this->session->userdata('u_no')) {
            redirect('/login?redirect=/admin/dashboard');
        } else {
            // if (isset($_SESSION['ua_id'])==false) {
            if (!$this->session->userdata('ua_id')) {
                $newdata = [
                    'u_id' => '',
                    'u_no' => '',
                    'u_name' => '',
                    'e_id' => ''
                ];
                $this->session->unset_userdata($newdata);
                $this->session->sess_destroy();
                $sessions = $this->session->all_userdata();
                redirect('/main');
            } else {
                $this->load->view('vue/index');
            }
        }
    }

    public function view()
    {
        /*$result = $this->award_model->get_award($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);*/
        $this->load->view('vue/index');
    }

    public function create()
    {
        $result = $this->award_model->create_award();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function modify()
    {
        $type = "award/logo";
        $upload_result = upload_local_file($type);
        $logo_path = "";
        if ($upload_result['status']=="00") {
            $storage_result = storage_upload($type, $upload_result);
            $img_result = "{$upload_result['upload_path']}/{$upload_result['file_name']}";
            unlink($img_result);
            rmdir($upload_result['upload_path']);
            $logo_path = $storage_result['path'];
        }
        $result = $this->award_model->modify_award($logo_path);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function remove()
    {
        $result = $this->award_model->remove_award();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }



    /* judge */
    public function judge()
    {
        $result = $this->award_model->get_judge($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function view_judge($j_id)
    {
        $result = $this->award_model->get_judge($this->ss_a_id, $j_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function create_judge()
    {
        $img = "";
        $path = "award/judge";
        $upload_result = upload_local_file($path);
        if ($upload_result['status']=="00") {
            $storage_result = storage_upload($path, $upload_result);
            $img_result = "{$upload_result['upload_path']}/{$upload_result['file_name']}";
            unlink($img_result);
            rmdir($upload_result['upload_path']);
            $img = $storage_result['path'];
        }
        $result = $this->award_model->create_judge($img);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function modify_judge()
    {
        $img = "";
        $path = "award/judge";
        $upload_result = upload_local_file($path);
        if ($upload_result['status']=="00") {
            $storage_result = storage_upload($path, $upload_result);
            $img_result = "{$upload_result['upload_path']}/{$upload_result['file_name']}";
            unlink($img_result);
            rmdir($upload_result['upload_path']);
            $img = $storage_result['path'];
        }
        $result = $this->award_model->modify_judge($img);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    /* category */
    public function category()
    {
        $result = $this->award_model->get_category($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function view_category($c_id)
    {
        $result = $this->award_model->get_category($this->ss_a_id, $c_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }
    
    public function create_category($cp_id=0)
    {
        $result = $this->award_model->create_category($cp_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }
    
    public function modify_category()
    {
        $result = $this->award_model->modify_category();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    
    public function remove_category()
    {
        $result = $this->award_model->remove_category();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    

    /* prize */
    public function prize()
    {
        $result = $this->award_model->get_prize($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function view_prize($c_id)
    {
        $result = $this->award_model->get_prize($this->ss_a_id, $c_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }
    
    public function create_prize()
    {
        $result = $this->award_model->create_prize();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }
    
    public function modify_prize()
    {
        $result = $this->award_model->modify_prize();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }
    
    public function remove_prize()
    {
        $result = $this->award_model->remove_prize();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }


    /* 유저 함수 */
    
    public function receiv_prize($a_id)
    {
        $data = $this->award_model->get_receiv_prize($a_id);
        $returnArray = pack_return("00", $data);
        echo json_encode($returnArray);
    }

    public function receiv_prize_detail($a_id, $zr_id=false)
    {
        $data = $this->award_model->get_receiv_prize($a_id, $zr_id);
        $returnArray = pack_return("00", $data);
        echo json_encode($returnArray);
    }
    
    public function create_receiv_prize()
    {
        $access = get_access($this->access_keyword);
        $data = $this->award_model->create_receiv_prize();
        $returnArray = pack_return($access, $data);
        echo json_encode($returnArray);
    }


    public function prize_usr($a_id)
    {
        $data = $this->award_model->get_prize($a_id);
        $returnArray = pack_return("00", $data);
        echo json_encode($returnArray);
    }


    public function category_usr($a_id)
    {
        $result = $this->award_model->get_category($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function judge_usr($a_id)
    {
        $data = $this->award_model->get_judge($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function view_user($a_id)
    {
        $result = $this->award_model->get_award($a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }
}
