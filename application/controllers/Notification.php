<?php
class Notification extends CI_Controller
{
    public $return_data = [];
    public $return_status = "99";
    public $access_code;
    public $access_keyword = "ba";
    public $return_msg = "";
        
    public function __construct()
    {
        parent::__construct();
        $this->load->model('notification_model');
        $this->load->library('session');
        $this->load->library('Notice');
        $this->load->helper('common');
        $this->load->database();
        $this->access_code = get_access($this->access_keyword);
        $this->load->model('user_model');
        $this->load->model('admin_model');
        $this->load->model('payment_model');
    }

    public function result_refactoring($result)
    {
        $this->return_status = $result['status']?$result['status']:$this->return_status;
        $this->return_data = $result['data']?$result['data']:$this->return_data;
        $this->return_msg = $result['msg']?$result['msg']:$this->return_msg;
        return return_refactoring($this->return_status, $this->return_data, $this->return_msg, $this->access_code);
    }


    public function main()
    {
        if (!$this->session->userdata('u_no')) {
            redirect('/login?redirect=/admin/dashboard');
        } else {
            // if (isset($_SESSION['ua_id'])==false) {
            if (!$this->session->userdata('ua_id')) {
                $newdata = [
                    'u_id' => '',
                    'u_no' => '',
                    'u_name' => '',
                    'e_id' => ''
                ];
                $this->session->unset_userdata($newdata);
                $this->session->sess_destroy();
                $sessions = $this->session->all_userdata();
                redirect('/main');
            } else {
                $this->load->view('vue/index');
            }
        }
    }

    public function view($id = null)
    {
        $result = $this->notification_model->get_notification();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function update()
    {
        $result = $this->notification_model->update_notification();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function contents_update()
    {
        $result = $this->notification_model->update_contents_notification();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function replace_template($replace_a, $replace_b, $contents)
    {
        return str_replace($replace_a, $replace_b, $contents);
    }

    public function replace_result($u_no, $contents)
    {
        $user_result = $this->user_model->get_user($u_no);
        $user_row = $user_result['data'];
        $enterprise_row = $this->user_model->get_enterprise($user_row['e_id']);
        $a_array = ["{{아이디}}","{{담당자명}}","{{담당자유선번호}}","{{담당자무선번호}}","{{담당자이메일}}","{{회사명}}","{{대표자명}}","{{회사대표이메일}}","{{사업자번호}}"];
        $b_array = [$user_row['u_id'],$user_row['u_name'],$user_row['u_phone1'],$user_row['u_phone2'],$user_row['u_email'],$enterprise_row['e_company'],$enterprise_row['e_name'],$enterprise_row['e_taxemail'],$enterprise_row['e_number']];
        foreach ($a_array as $a_key => $a_value) {
            $b_value = $b_array[$a_key];
            $contents = $this->replace_template($a_value, $b_value, $contents);
        }
        return $contents;
    }

    public function send_notification($method, $u_no, $type)
    {
        $msg = "회원정보가 없습니다.";
        $method_config_array = [
                "sms"=>[
                    "title"=>"문자",
                    "subject"=>"n_sms_subject",
                                        "contents"=>"n_sms_contents",
                                        "address_field"=>"u_phone1"
                                ],
                "mail"=>[
                    "title"=>"메일",
                    "subject"=>"n_mail_subject",
                                        "contents"=>"n_mail_contents",
                                        "address_field"=>"u_email"
                ]
        ];
        $method_config = $method_config_array["{$method}"];
        if ($u_no>0) {
            $user_result = $this->user_model->get_user($u_no);
            $msg = "회원정보를 찾을 수 없습니다.";
            if ($user_result['status']=="00") {
                $user_row = $user_result['data'];
                $user_name = $user_row['u_name'];
                $query = "select * from tb_notification_contents where n_type = '{$type}'";
                $result =  $this->db->query($query);
                $template = $result->row_array();
                $msg = "템플릿 정보가 없습니다.";
                if (is_null($template)==false) {
                    $receiv = $user_row["{$method_config['address_field']}"];
                    $subject = $this->replace_result($u_no, $template[$method_config['subject']]);
                    $contents = $this->replace_result($u_no, $template[$method_config['contents']]);
                    $msg = "{$method_config['title']}전송이 실패하였습니다.";
                    $send_status = $receiv==""?false:true;
                                                        
                    $return = $send_status==true?$this->send_toast($method, $receiv, $subject, $contents):"-1";
                    if ($return===0) {
                        $msg = "{$user_name}({$receiv})에게 {$method_config['title']}를 성공적으로 전송하였습니다.";
                        $this->admin_model->logging_admin("{$user_name}({$receiv})에게 {$method_config['title']}전송", $u_no);
                    }
                }
            }
        }
        echo $msg;
    }
        
    public function send_toast($method, $address, $subject, $contents)
    {
        if ($method=="sms") {
            return $this->notice->toast_sms($address, $subject, $contents);
        } elseif ($method=="mail") {
            return $this->notice->toast_mail($address, $subject, $contents);
        }
    }

    
        
    public function send_bill($p_no)
    {
        $taxBillNo = $this->payment_model->create_taxBillNo($p_no);
        $taxbill_info = $this->payment_model->get_taxbill_info($p_no);
        $wrtYmd = date("Ymd");
        $pay_total = $taxbill_info['p_totalprice'];
        $pay_supply = ceil($pay_total/1.1);
        $pay_tax = (int)$pay_total-(int)$pay_supply;
        $pay_method = ["deposit"=>"cashAmt","credit"=>"crdAcrcAmt"];
        $rquest_method = $pay_method["{$taxbill_info['p_method']}"];
        $e_number = str_replace('-','',$taxbill_info['e_number']);
        $e_address = $taxbill_info['e_address1']." ".$taxbill_info['e_address2'];
        $billing_data = [
            "taxBills"=>[
                [
                "taxBillNo" => "{$taxBillNo}",
                "taxBillTp" => "2",
                "taxBillKndCd" => "01",
                "taxBillCatCd" => "01",
                "splyrBno" => "2018205787",
                "splyrBzstNm" => "(사)한국디지털기업협회",
                "splyrReprNm" => "고경구",
                "splyrAddr" => "서울 강남구 도산대로 176 503호",
                "splyrBizcd" => "서비스",
                "splyrBiztp" => "어워드개최 외",
                "splyrChrgrEmail" => "master@dea.or.kr",
                "splyrBizrTp"=> "01",
                "splyrcptrBno"=> "{$e_number}",
                "splyrcptrBzstNm"=> "{$taxbill_info['e_company']}",
                "splyrcptrReprNm"=> "{$taxbill_info['e_name']}",
                "splyrcptrAddr"=> "{$e_address}",
                "splyrcptrBizcd"=> "{$taxbill_info['e_business']}",
                "splyrcptrBiztp"=> "{$taxbill_info['e_sector']}",
                "splyrcptrChrgrEmail"=> "{$taxbill_info['enrollee_taxmail']}",
                "splyrcptrBizrTp"=> "01",
                "wrtYmd"=> "{$wrtYmd}",
                "splyamt"=> "{$pay_supply}",
                "taxamt" => "{$pay_tax}",
                "totAmt" => "{$pay_total}",
                "{$rquest_method}" => "{$pay_total}",
                "dmndRcptTp" => "02",
                "taxBillPitms" => [
                    [
                    "pitmNo" => "1",
                    "splyamt" => "{$pay_supply}",
                    "taxamt" => "{$pay_tax}"
                    ]
                ]
                ]
            ]
        ];
        $response = $this->notice->toast_bill($billing_data);
        $result_code = $response->header->resultCode;
        if ($result_code=="0") {
            $this->payment_model->insert_taxbill();
        }
    }
}
