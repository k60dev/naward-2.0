<?php
class Popup extends CI_Controller
{
    public $return_data = [];
    public $return_status = "99";
    public $return_msg = "";
    public $access_code;
    public $ss_a_id = false;
    public $access_keyword = "aw";
    public function __construct()
    {
        parent::__construct();
        $this->load->model('popup_model');
        $this->load->model('admin_model');
        $this->load->library('session');
        $this->load->helper(array('form', 'url','common','upload','thumbnail'));
        $this->ss_a_id = $this->uri->segment(3)?$this->uri->segment(3):get_award_session();
        $this->access_code = get_access($this->access_keyword);
    }

    public function result_refactoring($result)
    {
        $this->return_status = $result['status']?$result['status']:$this->return_status;
        $this->return_data = $result['data']?$result['data']:$this->return_data;
        $this->return_msg = $result['msg']!=null?$result['msg']:$this->return_msg;
        return return_refactoring($this->return_status, $this->return_data, $this->return_msg, $this->access_code);
    }

    public function index()
    {
        $result = $this->popup_model->get_popup($this->ss_a_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function view($pop_id)
    {
        $result = $this->popup_model->get_popup($this->ss_a_id, $pop_id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function create()
    {
        $path = "award/popup";
        $upload_result = upload_local_file($path);
        $img = "";
        if ($upload_result['status']=="00") {
            $storage_result = storage_upload($path, $upload_result);
            $img_result = "{$upload_result['upload_path']}/{$upload_result['file_name']}";
            unlink($img_result);
            rmdir($upload_result['upload_path']);
            $img = $storage_result['path'];
        }
        $result = $this->popup_model->create_popup($img);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function modify()
    {
        $path = "award/popup";
        $upload_result = upload_local_file($path);
        $img = "";
        if ($upload_result['status']=="00") {
            $storage_result = storage_upload($path, $upload_result);
            $img_result = "{$upload_result['upload_path']}/{$upload_result['file_name']}";
            unlink($img_result);
            rmdir($upload_result['upload_path']);
            $img = $storage_result['path'];
        }
        $result = $this->popup_model->modify_popup($img);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function remove()
    {
        $result = $this->popup_model->remove_popup();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }
}
