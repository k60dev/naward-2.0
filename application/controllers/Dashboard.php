<?php
class Dashboard extends CI_Controller
{
    public $access_keyword = 'aw,pa,*';
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('common');
        $this->load->database();
    }
    public function dashboard()
    {
        // if (isset($_SESSION['ua_id'])==false) {
        //     echo "<script>alert('관리자만 접근할 수 있습니다.');location.href='/';</script>";
        //     exit;
        // }

        if (!$this->session->userdata('u_no')) {
            redirect('/login?redirect=/admin/dashboard');
        } else {
            // if (isset($_SESSION['ua_id'])==false) {
            if (!$this->session->userdata('ua_id')) {
                $newdata = [
                    'u_id' => '',
                    'u_no' => '',
                    'u_name' => '',
                    'e_id' => ''
                ];
                $this->session->unset_userdata($newdata);
                $this->session->sess_destroy();
                $sessions = $this->session->all_userdata();
                redirect('/main');
            } else {
                $this->load->view('vue/index');
            }
        }
    }
}
