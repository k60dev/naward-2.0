<?php
class Terms extends CI_Controller
{
    public $return_data = [];
    public $return_status = "99";
    public $access_code;
    public $access_keyword = "ua";
    public $return_msg = "";
    public function __construct()
    {
        parent::__construct();
        $this->load->model('terms_model');
        $this->load->library('session');
        $this->load->helper('common');
        $this->access_code = get_access($this->access_keyword);
        // $this->arr_auth = explode(',', $this->session->userdata('ua_auth'));
        // $this->permission = in_array($this->access_keyword, $this->arr_auth);
    }

    public function result_refactoring($result)
    {
        $this->return_status = $result['status']?$result['status']:$this->return_status;
        $this->return_data = $result['data']?$result['data']:$this->return_data;
        $this->return_msg = $result['msg']?$result['msg']:$this->return_msg;
        return return_refactoring($this->return_status, $this->return_data, $this->return_msg, $this->access_code);
    }

    public function main()
    {
        if (!$this->session->userdata('u_no')) {
            redirect('/login?redirect=/admin/dashboard');
        } else {
            // if (isset($_SESSION['ua_id'])==false) {
            if (!$this->session->userdata('ua_id')) {
                $newdata = [
                    'u_id' => '',
                    'u_no' => '',
                    'u_name' => '',
                    'e_id' => ''
                ];
                $this->session->unset_userdata($newdata);
                $this->session->sess_destroy();
                $sessions = $this->session->all_userdata();
                redirect('/main');
            } else {
                $this->load->view('vue/index');
            }
        }
    }

    public function view($id = null)
    {
        $result = $this->terms_model->get_terms($id);
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }

    public function update()
    {
        $result = $this->terms_model->update_terms();
        $return = $this->result_refactoring($result);
        echo json_encode($return);
    }
}
