<?php
class Upload extends CI_Controller
{
    public $ss_a_id = false;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url','common','upload','thumbnail'));
        $this->load->database();
        $this->load->library('session');
        $this->load->model('award_model');
        $this->load->model('user_model');
        $this->ss_a_id = get_award_session();
    }
    
    public function index()
    {
        $type = $this->input->post('type');
        $index = $this->input->post('index');
        $a_id = $this->ss_a_id;
        $returnCode = "";

        if ($type&&$index) {
            switch ($type) {
                case "judge":
                    $row = $this->award_model->get_judge($a_id, $index);
                break;
                case "enterprise":
                    $row = $this->user_model->get_enterprise($index);
                break;
                case "prize":
                    $row = $this->award_model->get_prize($a_id, $index);
                break;
            }
            
            if ($row!==[]) {
                $config_table_array = ["judge"=>"tb_judge","enterprise"=>"tb_enterprise","prize"=>"tb_prize"];
                $config_setField_array = ["judge"=>"j_id","enterprise"=>"e_id","prize"=>"z_id"];
                $config_updateField_array = ["judge"=>"j_picture","enterprise"=>"e_registration","prize"=>"z_picture"];
                $img_dir = $this->input->post('type')?$this->input->post('type'):"";

                $upload_result = $this->file_upload($img_dir);
                if ($upload_result['status']=="00") {
                    $img_path = "{$upload_result['upload_path']}/{$upload_result['file_name']}";
                    $query = "update {$config_table_array[$type]} set {$config_updateField_array[$type]} = '{$img_path}' where {$config_setField_array[$type]} = {$index}";
                    $this->db->query($query);
                }
                $status = $upload_result['status'];
                $msg = $upload_result['msg'];
            } else {
                $returnCode = "400";
                $msg = "데이터를 확인해 주세요.";
            }
        } else {
            $returnCode = "400";
            $msg = "데이터를 확인해 주세요.";
        }
        $returnArray = ["status"=>$returnCode,"data"=>["msg"=>$msg]];
        echo json_encode($returnArray);
    }

    public function file_upload($img_dir)
    {
        $config['upload_path'] = "./uploads/{$img_dir}";
        $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
        $config['max_size']	= '100';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';
        $field_name = "images";

        $this->load->library('upload', $config);
        $path = $config['upload_path'];
        $status = "99";
        $imgname = "";
        $full_path = "";
        if (! $this->upload->do_upload($field_name)) {
            $msg = $this->upload->display_errors();
        } else {
            $result = $this->upload->data();
            $file_name = $result['file_name'];
            $full_path = $result['full_path'];
            $status = "00";
            $msg = "success";
        }

        return ["status"=>$status,"msg"=>$msg,"full_path"=>$full_path,"file_name"=>$file_name,"upload_path"=>$path];
    }


    public function make_thumbnail()
    {
        /* test */
        $src = "./uploads/output/long.png";
        $img_500 = $this->return_thumbnail($src, "300", "300");
        $img_1000 = $this->return_thumbnail($src, "1000", "1000");
        $img_1500 = $this->return_thumbnail($src, "1500", "1500");
    }

    public function return_thumbnail($resource, $width, $height)
    {
        $fileinfo = pathinfo($resource);
        $dirname = $fileinfo['dirname'];
        $basename = $fileinfo['basename'];
        $filename = $fileinfo['filename'];
        $extension = $fileinfo['extension'];

        $resultname = "{$filename}_{$width}_{$height}.{$extension}";
        $result_src = "{$dirname}/{$resultname}";
        createThumbnail($resource, $result_src, $width, $height);
    }

    public function huge_upload()
    {
        echo "@";
        //$storage_result = storage_upload($type, $upload_result);
        $dir = "./uploads/SumitUploads/";
        
        $this->readDir($dir);
    }

    private function readDir($dir)
    {
        if (is_dir($dir)) {
            if ($my_dir = opendir($dir)) {
                while ($file=readdir($my_dir)) {
                    if ($file!="."&&$file!="..") {
                        if (is_dir($dir.$file)) {
                            /*
                            $storage_name = str_replace("./uploads/SumitUploads/", "", $dir);
                            $storage = "legacy/".$file;

                            $is_exist_storage = storage_exist($storage);
                            if ($is_exist_storage[0]!="") {
                                //storage_create($storage);
                            }
                            */
                            $this->readdir($dir.$file);
                        } else {
                            $storage_name = str_replace("./uploads/SumitUploads/", "", $dir);
                            $storage = "legacy/".$storage_name."/";
                            $result = storage_exist("legacy", $storage_name);
                            $filename = $storage_name."/".$file;
                            if (in_array($filename, $result)==false) {
                                echo $filename;
                                storage_upload_exist($storage, $file, $dir);
                            }
                        }
                    }
                }
            }
        }
    }
}
