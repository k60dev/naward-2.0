<?php
class Aboard_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->load->model('award_model');
        $this->load->helper(array('upload','thumbnail'));
    }

    public function get_aboard($ab_type, $a_id, $ab_id=false)
    {
        $where["ab_type"] = $ab_type;
        $where["a_id"] = $a_id;
        $where["is_del"] = 'N';
        if ($ab_id === false) {
            //$where["p_status"] = 9;
            $query = $this->db->select("ab_id,ab_type,ab_title,ab_dsc,ab_contents,ab_thumb,ab_status,wdate,udate")->order_by('ab_id', 'desc')->get_where('tb_aboard', $where);
            $data = $query->result_array();
        } else {
            $where["ab_id"] = $ab_id;
            $query = $this->db->get_where('tb_aboard', $where);
            $data = $query->row_array();
        }
        $cnt=is_null($data) ? 0 : count($data);
        $status = $cnt>0?"00":"29";
        return class_return_refactoring($status, $data);
    }

    public function create_aboard($type)
    {
        $a_id = $this->input->post('a_id');
        $is_exist = $this->award_model->get_award_by_field('a_id', $a_id);
        $status = "09";
        $cntAffected = 0;
        $data = [];
        if (is_null($is_exist)==false) {
            $ab_type = $type;
            $ab_title = $this->input->post('ab_title');
            $ab_contents = $this->input->post('ab_contents');
            $ab_resource = $this->input->post('ab_resource');
            $ab_status = $this->input->post('ab_status');
            $wdate = date("Y-m-d h:i:s");
            $ab_data = [
                'a_id'=>$a_id,
                'ab_type'=>$ab_type,
                'ab_title'=>$ab_title,
                'ab_contents'=>$ab_contents,
                'ab_resource'=>$ab_resource,
                'ab_status'=>$ab_status,
                'wdate'=>$wdate
                ];
            $query = $this->db->insert('tb_aboard', $ab_data);
            $ab_id = $this->db->insert_id();
            $result = $this->get_aboard($type, $a_id, $ab_id);
            $data = $result['data'];
            $status = $ab_id>0?"00":"09";
            if ($status=="00") {
                $boardType = $type=="gallery"?"갤러리":"강연";
                $this->admin_model->logging_admin("{$boardType} 생성 (id: {$ab_id})");
            }
        }
        return class_return_refactoring($status, $data);
    }


    public function modify_aboard($type)
    {
        $table = "tb_aboard";
        $ab_id = $this->input->post('ab_id');
        $is_exist = $this->award_model->get_table_by_field($table, 'ab_id', $ab_id, true);
        $status = "19";
        $cntAffected = 0;
        $data = [];
        if (is_null($is_exist)==false) {
            $ab_title = $this->input->post('ab_title');
            $ab_contents = $this->input->post('ab_contents');
            $ab_resource = $this->input->post('ab_resource');
            $ab_status = $this->input->post('ab_status');
            $wdate = date("Y-m-d h:i:s");
            $ab_data = [
                'ab_title'=>$ab_title,
                'ab_contents'=>$ab_contents,
                'ab_resource'=>$ab_resource,
                'ab_status'=>$ab_status
                ];
            $where = "ab_id = '{$ab_id}'";
            $query = $this->db->update($table, $ab_data, $where);
            $cntAffected = $this->db->affected_rows();
            $result = $this->get_aboard($type, $is_exist['a_id'], $ab_id);
            $data = $result['data'];
            // $status = $cntAffected>0?"00":"19";
            $status = "00";
            if ($status=="00") {
                $boardType = $type=="gallery"?"갤러리":"강연";
                $this->admin_model->logging_admin("{$boardType} 수정 (id: {$ab_id})");
            }
        }
        return class_return_refactoring($status, $data);
    }

    public function upload_aboard_picture($ab_id, $ab_thumb)
    {
        $ab_data = ['ab_thumb'=>$ab_thumb];
        $where = "ab_id = '{$ab_id}'";
        $query = $this->db->update("tb_aboard", $ab_data, $where);
    }

    public function fetch_aboard($type, $field, $value)
    {
        $ab_id = $this->input->input_stream('ab_id');
        $table = "tb_aboard";
        $is_exist = $this->award_model->get_table_by_field($table, 'ab_id', $ab_id, true);
        $status = "19";
        $cntAffected = 0;
        $data = [];
        if (is_null($is_exist)==false) {
            $ab_data = [
                $field=>$value
                ];
            $where = "ab_id = '{$ab_id}'";
            $query = $this->db->update('tb_aboard', $ab_data, $where);
            $cntAffected = $this->db->affected_rows();
            $result = $this->get_aboard($type, $is_exist['a_id'], $ab_id);
            $data = ["cnt"=>$cntAffected];
            $status = $cntAffected>0?"00":"19";
            if ($status=="00") {
                $boardType = $type=="gallery"?"갤러리":"강연";
                $this->admin_model->logging_admin("{$boardType} 상태변경 ({$field} -> {$value}({$ab_id}))");
            }
        }
        return class_return_refactoring($status, $data);
    }

    public function remove_aboard($type)
    {
        $result = $this->fetch_aboard($type, "is_del", "Y");
        return $result;
    }
}
