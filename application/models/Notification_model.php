<?php
class Notification_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get_table_by_field($table, $field, $value, $isdel=false)
    {
        $where[$field] = $value;
        if ($isdel==true) {
            $where['is_del']='N';
        }
        $query = $this->db->get_where($table, $where);
        return $query->row_array();
    }
    
    public function get_table_by_fields($table, $where, $isdel=false)
    {
        if ($isdel==true) {
            $where['is_del']='N';
        }
        $query = $this->db->get_where($table, $where);
        return $query->row_array();
    }

    public function get_notification($id = false)
    {
        if ($id === false) {
            $query = $this->db->get('tb_notification');
            $data = $query->result_array();
            $status = "00";
        } else {
            $where["n_id"] = $id;
            $query = $this->db->get_where('tb_notification', $where);
            $data = $query->row_array();
            $cnt=is_null($data) ? 0 : count($data);
            $status = $cnt>0?"00":"29";
        }

        return class_return_refactoring($status, $data);
    }

    public function get_notification_contents($id = false)
    {
        if ($id === false) {
            $query = $this->db->get('tb_notification_contents');
            $data = $query->result_array();
            $status = "00";
        } else {
            $where["nc_id"] = $id;
            $query = $this->db->get_where('tb_notification_contents', $where);
            $data = $query->row_array();
            $cnt=is_null($data) ? 0 : count($data);
            $status = $cnt>0?"00":"29";
        }

        return class_return_refactoring($status, $data);
    }
    
    public function update_notification()
    {
        $n_group = $this->input->post("n_group");
        $n_type = $this->input->post("n_type");
        $n_mail = $this->input->post("n_mail");
        $n_sms = $this->input->post("n_sms");
        
        $table = "tb_notification";
        $data = $this->get_table_by_field($table, 'n_type', $n_type);
        if (is_null($data)==false) {
            $n_data = [
                'n_mail'=>$n_mail,
                'n_sms'=>$n_sms
                ];
            $where = "n_type = '{$n_type}'";
            $query = $this->db->update('tb_notification', $n_data, $where);
        } else {
            $n_data = [
                'n_group'=>$n_group,
                'n_type'=>$n_type,
                'n_mail'=>$n_mail,
                'n_sms'=>$n_sms
                ];

            $query = $this->db->insert('tb_notification', $n_data);
        }
        $data = $this->get_table_by_field($table, 'n_type', $n_type);
        $cntAffected = $this->db->affected_rows();
        $status = $cntAffected>0?"00":"19";
        if ($status=="00") {
            $this->admin_model->logging_admin("알림설정 변경 (type: {$n_type})");
        }

        return class_return_refactoring($status, $data);
    }
    
    public function update_contents_notification()
    {
        $table = "tb_notification_contents";
        $n_group = $this->input->post("n_group");
        $n_type = $this->input->post("n_type");
        $c_where['n_group'] = $n_group;
        $c_where['n_type'] = $n_type;
        $n_row = $this->get_table_by_fields($table, $c_where);
        $status = "19";
        $data = [];
        $n_subject = $this->input->post("n_subject");
        $n_contents = $this->input->post("n_contents");
        $method = $this->input->post("method");

        $nc_data = [
            "n_{$method}_subject"=>$n_subject,
            "n_{$method}_contents"=>$n_contents
            ];

        $c_table = "tb_notification_contents";
        $nc_row = $this->get_table_by_fields($c_table, $c_where);
        if (is_null($nc_row)==false) {
            $where = "n_group = '{$n_group}' and n_type = '{$n_type}'";
            $query = $this->db->update($c_table, $nc_data, $where);
            $cntAffected = $this->db->affected_rows();
            $status = $cntAffected>0?"00":"19";
        } else {
            $nc_data['n_group'] = $n_group;
            $nc_data['n_type'] = $n_type;
            $query = $this->db->insert($c_table, $nc_data);
            $cntAffected = $this->db->affected_rows();
            $status = $cntAffected>0?"00":"09";
            $nc_id = $this->db->insert_id();
        }
        if ($status=="00") {
            $this->admin_model->logging_admin("알림설정 템플릿 변경 (type: {$n_type})");
        }
        $data = $this->get_table_by_field($c_table, 'n_id', $n_row['n_id']);
        return class_return_refactoring($status, $data);
    }
}
