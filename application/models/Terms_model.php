<?php
class Terms_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->load->model('admin_model');
    }

    public function get_table_by_field($table, $field, $value, $isdel=false)
    {
        $where[$field] = $value;
        if ($isdel==true) {
            $where['is_del']='N';
        }
        $query = $this->db->get_where($table, $where);
        return $query->row_array();
    }

    public function get_terms($id = false)
    {
        if ($id === false) {
            $query = $this->db->get('tb_terms');
            $data = $query->result_array();
            $status = "00";
        } else {
            $query = $this->db->get_where('tb_terms', array('id' => $id));
            $data = $query->row_array();
            $cnt= is_null($data) ? 0 : count($data);
            $status = $cnt>0?"00":"29";
        }
        return class_return_refactoring($status, $data);
    }

    public function update_terms()
    {
        $table = "tb_terms";
        $id = $this->input->post('id');
        $is_exist = $this->get_table_by_field($table, 'id', $id);
        $data = [];
        $status = "19";
        if (is_null($is_exist)==false) {
            $description = $this->input->post('description');
            $contents = $this->input->post('contents');
            $t_data = array('contents' => $contents, 'description' => $description);
            $where = "id = {$id}";
            $query = $this->db->update('tb_terms', $t_data, $where);
            $result = $this->get_terms($id);
            $data = $result['data'];
            $cntAffected = $this->db->affected_rows();
            $status = $cntAffected>0?"00":"19";
            if ($status=="00") {
                $this->admin_model->logging_admin("약관수정 (id: {$id})");
            }
        }
        return class_return_refactoring($status, $data);
    }
}
