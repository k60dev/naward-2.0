<?php
class Popup_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->load->model('award_model');
    }

    public function get_popup($a_id, $pop_id=false)
    {
        $where["a_id"] = $a_id;
        if ($pop_id === false) {
            // $where["pop_status"] = "1";
            $query = $this->db->order_by('pop_id', 'desc')->get_where('tb_popup', $where);
            $data = $query->result_array();
            $status = "00";
        } else {
            $where["pop_id"] = $pop_id;
            $query = $this->db->get_where('tb_popup', $where);
            //return $query->row_array();
            
            $data = $query->row_array();
            $cnt=is_null($data) ? 0 : count($data);
            $status = $cnt>0?"00":"29";
        }
        return class_return_refactoring($status, $data);
    }
    
    public function create_popup()
    {
        $a_id = $this->input->post('a_id');
        $is_exist = $this->award_model->get_award_by_field('a_id', $a_id);
        $status = "09";
        $cntAffected = 0;
        $data = [];
        if (is_null($is_exist)==false) {
            $pop_title = $this->input->post('pop_title');
            $pop_headline = $this->input->post('pop_headline');
            $pop_start = $this->input->post('pop_start');
            $pop_end = $this->input->post('pop_end');
            $pop_width = $this->input->post('pop_width');
            $pop_height = $this->input->post('pop_height');
            $pop_x = $this->input->post('pop_x');
            $pop_y = $this->input->post('pop_y');
            $pop_data = [
                'a_id'=>$a_id,
                'pop_title'=>$pop_title,
                'pop_start'=>$pop_start,
                'pop_end'=>$pop_end,
                'pop_width'=>$pop_width,
                'pop_height'=>$pop_height,
                'pop_x'=>$pop_x,
                'pop_y'=>$pop_y,
                'pop_headline'=>$pop_headline
                ];
            $query = $this->db->insert('tb_popup', $pop_data);
            $pop_id = $this->db->insert_id();
            $result = $this->get_popup($a_id, $pop_id);
            $data = $result['data'];
            $status = $pop_id>0?"00":"09";
            if ($status=="00") {
                $this->admin_model->logging_admin("팝업 생성 (id: {$pop_id})");
            }
        }
        return class_return_refactoring($status, $data);
    }

    public function modify_popup()
    {
        $table = "tb_popup";
        $pop_id = $this->input->post('pop_id');
        $is_exist = $this->award_model->get_table_by_field($table, 'pop_id', $pop_id);
        $status = "19";
        $cntAffected = 0;
        $data = [];
        if (is_null($is_exist)==false) {
            $a_id = $is_exist['a_id'];//$this->input->post('a_id');
            $pop_title = $this->input->post('pop_title');
            $pop_start = $this->input->post('pop_start');
            $pop_end = $this->input->post('pop_end');
            $pop_width = $this->input->post('pop_width');
            $pop_height = $this->input->post('pop_height');
            $pop_x = $this->input->post('pop_x');
            $pop_y = $this->input->post('pop_y');
            $pop_global = $this->input->post('pop_global');
            $pop_status = $this->input->post('pop_status');
            $pop_headline = $this->input->post('pop_headline');
            $pop_data = [
                'pop_title'=>$pop_title,
                'pop_start'=>$pop_start,
                'pop_end'=>$pop_end,
                'pop_width'=>$pop_width,
                'pop_height'=>$pop_height,
                'pop_x'=>$pop_x,
                'pop_y'=>$pop_y,
                'pop_global'=>$pop_global,
                'pop_status'=>$pop_status,
                'pop_headline'=>$pop_headline
                ];
            $where = "pop_id = '{$pop_id}'";
            $query = $this->db->update('tb_popup', $pop_data, $where);
            $result = $this->get_popup($a_id, $pop_id);
            $data = $result['data'];
            $cntAffected = $this->db->affected_rows();
            $status = $cntAffected>0?"00":"19";
            if ($status=="00") {
                $this->admin_model->logging_admin("팝업 수정 (id: {$pop_id})");
            }
        }
        return class_return_refactoring($status, $data);
    }


    public function remove_popup($pop_id)
    {
        $table = "tb_popup";
        $is_exist = $this->award_model->get_table_by_field($table, 'pop_id', $pop_id);
        $status = "39";
        $cntAffected = 0;
        $data = [];
        if (is_null($is_exist)==false) {
            $this->db->where('pop_id', $pop_id);
            $this->db->delete('tb_popup');
            $cntAffected = $this->db->affected_rows();
            $status = $cntAffected>0?"00":"39";
            if ($status=="00") {
                $result = $this->get_popup($is_exist['a_id']);
                $data = $result->data;
                $this->admin_model->logging_admin("팝업 삭제 (id: {$pop_id})");
            }
        }

        return class_return_refactoring($status, $data);
    }

    public function update_popup_picture($pop_id, $img_path)
    {
        $img_data = ["pop_picture"=>$img_path];
        $this->db->set($img_data);
        $this->db->where('pop_id', $pop_id);
        $query = $this->db->update('tb_popup');
    }
}
