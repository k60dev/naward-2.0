<?php
class Payment_model extends CI_Model
{
    public $enroll_free_count = 7;
    public function __construct()
    {
        $this->load->database();
        $this->load->model('admin_model');
        $this->load->helper('common');
        $this->load->library('Notice');
        $this->load->model('award_model');
        $this->load->model('enroll_model');
    }

    public function get_payment($p_no = false, $rest_value=false)
    {
        $where["p_status !="] = "0";
        if ($p_no === false) {
            if ($rest_value!=false) {
                if ($rest_value == "3") {
                    $where["p_taxrequest"] = "1";
                } else {
                    $where["p_status"] = $rest_value;
                }
            }
            $this->db->select('tb_payment.*,tb_user.u_id as u_id,tb_enterprise.e_company', false);
            $this->db->from('tb_payment');
            $this->db->join('tb_user', 'tb_payment.u_no = tb_user.u_no', 'left');
            $this->db->join('tb_enterprise', 'tb_payment.e_id = tb_enterprise.e_id', 'left');
            $this->db->where($where);
            $query = $this->db->get();
            $data = $query->result_array();
            foreach ($data as $key1 => $row) {
                $count_enterprise = $this->award_model->cnt_enterprise_enroll($row['a_id'], $row['e_id']);
                $p_no = $row['p_no'];
                $query = "SELECT COUNT(*) as cnt from tb_work WHERE p_no = '{$p_no}'";
                $query = $this->db->query($query);
                $count_work = $query->row_array();
                $count_current = $count_work['cnt'];
                $count_before = $count_enterprise - $count_current;
                $pay_enterprise_count = 7 - $count_enterprise;
                if ($count_before >= 7) {
                    $pay_count = 0;
                } else {
                    if ($count_before + $count_current > 7) {
                        $pay_count = $count_current - (($count_current + $count_before) - 7);
                    } else {
                        $pay_count = $count_current;
                    }
                }
                $data[$key1]['pay_count'] = $pay_count;
                // print_r2($count_enterprise .'/'.$count_current.'/'.$data[$key1]['pay_count']."\n");
            }
            $status = "00";
        } else {
            $where["p_no"] = $p_no;
            //$query = "SELECT a.*,b.*,c.e_registration FROM tb_payment AS a LEFT JOIN tb_user AS b ON a.u_no = b.u_no left join tb_enterprise as c on a.e_id = c.e_id WHERE p_no = '{$p_no}'";
            $query = "SELECT a.*,b.*,c.*,COUNT(d.wp_no) AS cnt_cancel_work,SUM(d.total_price) AS cancel_price FROM tb_payment AS a 
            LEFT JOIN tb_user AS b ON a.u_no = b.u_no 
            LEFT JOIN tb_enterprise as c on a.e_id = c.e_id 
            LEFT JOIN tb_work AS d ON a.p_no = d.p_no 
            WHERE a.p_no = '{$p_no}' AND d.w_status < 0";
            $query = $this->db->query($query);
            $data = $query->row_array();
            $cnt=is_null($data) ? 0 : count($data);
            $status = $cnt>0?"00":"29";
        }
        return class_return_refactoring($status, $data);
    }

    public function get_payment_data($p_no)
    {
        $where["p_no"] = $p_no;
        $this->db->from('tb_payment');
        $this->db->where($where);
        $query = $this->db->get();
        $data = $query->row_array();
        return $data;
    }

    public function get_request($request_value=0)
    {
        $where["p_taxrequest"] = $request_value;
        $query = $this->db->get_where('tb_payment', $where);
        $data = $query->result_array();
        $status = "00";
        return class_return_refactoring($status, $data);
    }

    public function get_payment_work($p_no)
    {
        $where["tb_work.p_no"] = $p_no;
        $where["tb_work.w_status >="] = 0;
        $this->db->select('tb_work.*,tb_category.cp_id,case when w_status = -1 then "출품취소" when w_status = 1 then "심사중" when w_status = 2 then "출품완료" when w_status = 9 then "수상확정" when w_status = 0 then "출품대기" END AS STATUS,tb_category.c_title as c_type,tb_payment.p_totalprice', false);
        $this->db->from('tb_work');
        $this->db->join('tb_category', 'tb_work.c_id = tb_category.c_id', 'left');
        $this->db->join('tb_payment', 'tb_work.p_no = tb_payment.p_no', 'left');
        $this->db->where($where);
        //echo $query = $this->db->get_compiled_select();
        $query = $this->db->get();
        $data = $query->result_array();
        foreach ($data as $key1 => $row) {
            $payment_price = $row['p_totalprice'];
            $total_price_data = $row['total_price'];
            $show_payment_price = $payment_price>0?($payment_price<$total_price_data?$payment_price:$total_price_data):0;
            $data[$key1]['total_price'] = $show_payment_price;
            $payment_price -= $show_payment_price;
            $data[$key1]['price_dc'] = $show_payment_price < $total_price_data ? 'Y' : 'N';

            $arr_c_id = $data[$key1]['c_id'];
            $category_array = explode(",", $arr_c_id);
            foreach ($category_array as $key2 => $c_id) {
                $category_query = $this->db->get_where('tb_category', "c_id={$c_id}");
                $category_res = $category_query->result();
                $data[$key1]['category_data'][$key2] = $category_res[0];
            }
            $cptitle_query = "select * from tb_category where c_id = {$data[$key1]['cp_id']}";
            $cptitle_result = $this->db->query($cptitle_query)->result();
            $data[$key1]['cp_title'] = $cptitle_result[0]->c_title;
        }
        $status = "00";
        return class_return_refactoring($status, $data);
    }

    public function patch_payment($field, $value)
    {
        $pa_list = $this->input->input_stream('pa_list');
        $p_array = explode(",", $pa_list);//["N_2020200519_002"];
        $p_data[$field] = $value;
        if ($field=="p_status"&&$value=="9") {
            $p_data["p_findate"] = date("Y-m-d H:i:s");
        }
        $total_affected = 0;
        foreach ($p_array as $p_no) {
            $where = "p_no = '{$p_no}'";
            $query = $this->db->update('tb_payment', $p_data, $where);
            $cnt_affected = $this->db->affected_rows();
            // if ($field=="p_status"&&$value=="9") {
            //     $group = "pa";
            //     $type = "finish";
            //     $info = $this->get_payment_data($p_no);
            //     $res_mail = Notice::send_notification("mail", $info['u_no'], $group, $type, $p_no);
            //     $res_sms = Notice::send_notification("sms", $info['u_no'], $group, $type, $p_no);
            // }
            if ($cnt_affected>0) {
                $this->admin_model->logging_admin("결제건 {$p_no}의 상태값을 변경 ({$field} -> {$value})");
            }
            $total_affected += $cnt_affected;
        }
        $status = $total_affected>0?"00":"19";
        $data = ["cnt"=>$total_affected];
        return class_return_refactoring($status, $data);
    }

    public function cancel_payment($p_no)
    {
        /*
        1. 회원이 결제할때 e_id도 같이 받아온다.
        2. e_id를 통해 총 제출 작품수가 몇개인지 확인한다.
        3. 총 작품수가 more_works_than_this개 이상이면 6개부터는 무료로 진행
        4. 마찬가지로 총 작품수가 5개 이하가 아닐때는 캔슬 비용을 무료로 한다.
        5. p_no는 해당 결제건의 취소금액을 연산하기 위한 번호이다.

        1. 총 작품수는 3개
        2. 2개까지만 무료취소
        3. 나머지는 유료취소

        7-5 : 2
        6-5 : 1
        5-5 : 0

        취소작품 : 3
        무료취소 : 2
        3-2 = 1;

        */
        $p_row = $this->get_payment($p_no);
        $a_id = $p_row['data']['a_id'];
        $award_info = $this->award_model->get_award_by_field('a_id', $a_id);
        $more_works_than_this = $this->enroll_model->enroll_free_count;
        $global_work_price = $award_info['work_price'];

        $e_id = $p_row['data']['e_id'];
        $before_total_enters_works = $this->payment_model->get_eterprise_total_works($e_id);
        $able_free_cancel = ($before_total_enters_works>$more_works_than_this)?$before_total_enters_works-$more_works_than_this:0;

        echo $total_affected = $this->update_work_status($p_no, -1);

        $total_cancel_work=$this->get_total_cancel_inpayment($p_no);
        $cnt_calculator_work = $able_free_cancel>0?$total_cancel_work-$able_free_cancel:$total_cancel_work;
        $price_total_cancel = $cnt_calculator_work>0?$cnt_calculator_work*$global_work_price:0;

        $cancelquery = "update tb_payment set p_cancelprice = {$price_total_cancel},p_payprice = p_totalprice-{$price_total_cancel} where p_no = '{$p_no}'";
        $this->db->query($cancelquery);
        echo $cancel_affected = $this->db->affected_rows();

        $payresult["p_no"] = $p_no;
        $query = $this->db->get_where('tb_work', $payresult);
        $data = $query->row_array();
        $msg = $total_affected>0?"총 {$total_affected}개의 작품을 취소하였습니다.":"취소한 작품이 없습니다.";
        //$returArr = ["able_free_cancel"=>$able_free_cancel,"total_cancel_work"=>$total_cancel_work,"price_total_cancel"=>$price_total_cancel,"cnt_calculator_work"=>$cnt_calculator_work];
        //return $returArr;

        $status = $total_affected>0&&$cancel_affected>0?"00":"19";
        $data = ["cnt"=>$total_affected];

        return class_return_refactoring($status, $data, $msg);
    }

    public function patch_status($p_no, $status)
    {
        $total_affected = $this->update_work_status($p_no, $status);
        $re_status = $total_affected>0?"00":"19";
        $data = ["cnt"=>$total_affected];
        return class_return_refactoring($re_status, $data);
    }

    public function update_work_status($p_no, $value)
    {
        $w_list = $this->input->input_stream('w_list');
        $w_array = explode(",", $w_list);
        $total_affected = 0;
        foreach ($w_array as $w_id) {
            $field = "w_status";
            $w_data = [
                $field=>$value
                ];
            $where = "wp_no = '{$w_id}' and p_no = '{$p_no}' and is_del = 'N'";
            $query = $this->db->update('tb_work', $w_data, $where);
            //echo $query = $this->db->set($w_data)->where($where)->get_compiled_update('tb_work') ;
            $cnt_affected = $this->db->affected_rows();
            $total_affected += $cnt_affected;
            if ($cnt_affected>0) {
                $this->admin_model->logging_admin("{$p_no}의 작품 출품취소");
            }
        }
        return $total_affected;
    }

    public function get_eterprise_total_works($e_id)
    {
        $this->db->select('w_id');
        $this->db->from('tb_work');
        $this->db->where('e_id', $e_id);
        $count = $this->db->count_all_results();
        return $count;
    }

    public function get_total_cancel_inpayment($p_no)
    {
        $this->db->select('w_id');
        $this->db->from('tb_work');
        $this->db->where('p_no', $p_no);
        $this->db->where('is_del', 'N');
        $this->db->where('w_status', '-1');
        $count = $this->db->count_all_results();
        return $count;
    }

    public function request_tax($p_no)
    {
        $p_data['p_taxrequest'] = '2';
        $where = "p_no = '{$p_no}'";
        $query = $this->db->set($p_data)->where($where)->update('tb_payment') ;
        $cnt_affected = $this->db->affected_rows();
        if ($cnt_affected>0) {
            $this->admin_model->logging_admin("결제건 {$p_no}의 세금계산서 발행");
        }
        $status = $cnt_affected>0?"00":"19";
        $data = ["cnt"=>$cnt_affected];
        return class_return_refactoring($status, $data);
    }

    public function get_taxbill_info($p_no)
    {
        $query = "SELECT a.u_no,a.p_no,b.e_number,b.e_company,b.e_name,b.e_address1,b.e_address2,b.e_business,b.e_sector,a.p_totalprice,a.p_method,a.e_id,a.enrollee_taxmail FROM tb_payment AS a
        LEFT JOIN tb_enterprise AS b ON a.e_id = b.e_id
        WHERE a.p_no = '{$p_no}' AND a.p_status = 9";
        $sql = $this->db->query($query);
        return $row = $sql->row_array();
    }
        
    public function get_taxBillNo($taxBillNo)
    {
        $query = "select taxBillNo from tb_taxbill where taxBillNo = '{$taxBillNo}'";
        $sql = $this->db->query($query);
        return $row = $sql->row_array();
    }

    public function create_taxBillNo($p_no)
    {
        $rand = rand(0, 999);
        $rand = str_pad($rand, "3", "0", STR_PAD_LEFT);
        $billNo = $p_no.date("ymd").$rand;
        $billArr = $this->get_taxBillNo($billNo);
        if (is_null($billArr)==false) {
            return $this->create_taxBillNo($p_no);
        } else {
            return $billNo;
        }
    }

    public function insert_taxbill($info_array)
    {
        $data = [
                "taxBillNo"=>$info_array['taxBillNo'],
                "splyrcptrBno"=>$info_array['splyrcptrBno'],
                "splyrcptrBzstNm"=>$info_array['splyrcptrBzstNm'],
                "splyrcptrReprNm"=>$info_array['splyrcptrReprNm'],
                "wrtYmd"=>$info_array['wrtYmd'],
                "taxamt"=>$info_array['taxamt'],
                "totAmt"=>$info_array['totAmt'],
                "e_id"=>$info_array['e_id'],
                "wdate"=>$info_array['w_date'],
            ];
        $this->db->set($data);
        $query = $this->db->insert('tb_taxbill');
        $affected_count = $this->db->affected_rows();
        $code = $affected_count>0?"00":"09";
        $return = ["status"=>$code];
        return $return;
    }

    public function export_paylist()
    {
        $p_no = $this->input->input_stream('p_no');
        if ($p_no>0) {
            $plist = $p_no;
        } else {
            $plist = $this->input->input_stream('pay_list');
        }
        $data = [];
        if ($plist) {
            $query = "select a.p_no,a.p_wdate,b.u_id,c.e_company,a.p_title,case when a.p_method = 'credit' then '신용카드' when a.p_method = 'deposit' then '무통장입금' else '기타' end as method,FORMAT(a.p_totalprice,0),a.p_subdate
                from tb_payment as a
                left join tb_user as b on a.u_no = b.u_no
                left join tb_enterprise as c on b.e_id = c.e_id
                where a.p_no in ({$plist})";
            $query = $this->db->query($query);
            $data = $query->result_array();
        }
        return $data;
    }

    public function get_cancel_payment()
    {
        $data = [];
        $query = "SELECT * FROM tb_payment WHERE p_status = 1 AND p_limitdate < NOW()";
        $query = $this->db->query($query);
        $data = $query->result_array();
        return $data;
    }

    public function update_status_payment($p_no)
    {
        $data = ["p_status"=>"-1"];
        $where = ["p_no"=>$p_no];
        $query = $this->db->update('tb_payment', $data, $where);
    }
}
