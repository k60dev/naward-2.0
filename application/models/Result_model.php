<?php
class Result_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->load->model('award_model');
    }

    public function get_result($a_id, $w_id=false, $j_id=false, $param=false)
    {
        $where['tb_work.a_id'] = $a_id;
        if ($w_id === false) {
            $g_where['tb_payment.p_status'] = "9";
            $or_where['tb_payment.p_status'] = "1";
            if ($param != false) {
                if ($param == 'process') {
                    $where['tb_work.w_status'] = "1";
                } elseif ($param == 'hold') {
                    $where['tb_work.w_status'] = "3";
                } elseif ($param == 'finish') {
                    $where['tb_work.w_status'] = "2";
                } elseif ($param == 'confirm') {
                    $where['tb_work.w_status'] = "9";
                } else {
                    $where['tb_work.w_status'] = "0";
                }
            } else {
                // $where['tb_work.w_status !='] = null;
                $where['tb_work.w_status !='] = "-1";
            }

            $this->db->select('tb_work.*,tb_category.cp_id,tb_enterprise.e_company,tb_payment.p_method,tb_payment.p_subdate');
            $this->db->from('tb_work');
            $this->db->join('tb_payment', 'tb_work.p_no = tb_payment.p_no', 'left');
            $this->db->join('tb_category', 'tb_work.c_id = tb_category.c_id', 'left');
            $this->db->join('tb_enterprise', 'tb_work.e_id = tb_enterprise.e_id', 'left');
            $this->db->where($where);
            $this->db->group_start();
            $this->db->where($g_where);
            $this->db->or_where($or_where);
            $this->db->group_end();
            $this->db->order_by('p_subdate', 'asc');
            $query = $this->db->get();
            // $query = $this->db->get_compiled_select();
            // print_r2($query);
            $data = $query->result_array();
            foreach ($data as $key1 => $row) {
                $arr_c_id = $data[$key1]['c_id'];
                $category_array = explode(",", $arr_c_id);
                foreach ($category_array as $key2 => $c_id) {
                    $category_query = $this->db->get_where('tb_category', "c_id={$c_id}");
                    $category_res = $category_query->result();
                    $data[$key1]['category_data'][$key2] = $category_res[0];
                }
                $cptitle_query = "select * from tb_category where c_id = {$data[$key1]['cp_id']}";
                $cptitle_result = $this->db->query($cptitle_query)->result();
                $data[$key1]['cp_title'] = $cptitle_result[0]->c_title;
                $child_query = $this->db->get_where('tb_work_output', "w_id={$data[$key1]['w_id']}");
                $data[$key1]['child_data'] = $child_query->result_array();
            }
            $status = "00";
        } else {
            $where["tb_work.w_id"] = $w_id;
            $this->db->select('tb_work.*,tb_user.*,tb_category.c_type,tb_category.cp_id,tb_category.c_title,tb_enterprise.e_company,tb_payment.p_status,tb_work_judgement.jm_id,tb_work_judgement.j_id,tb_work_judgement.jm_score,tb_work_judgement.jm_date');
            $this->db->from('tb_work');
            $this->db->join('tb_user', 'tb_work.u_no = tb_user.u_no', 'left');
            $this->db->join('tb_category', 'tb_work.c_id = tb_category.c_id', 'left');
            $this->db->join('tb_enterprise', 'tb_work.e_id = tb_enterprise.e_id', 'left');
            $this->db->join('tb_payment', 'tb_work.p_no = tb_payment.p_no', 'left');
            if ($j_id !== false) {
                $this->db->join('tb_work_judgement', 'tb_work.w_id = tb_work_judgement.w_id and '.$j_id.' = tb_work_judgement.j_id', 'left');
            } else {
                $this->db->join('tb_work_judgement', 'tb_work.w_id = tb_work_judgement.w_id', 'left');
            }
            $this->db->where($where);
            $query = $this->db->get();
            $data = $query->row_array();

            $arr_c_id = $data['c_id'];
            $category_array = explode(",", $arr_c_id);
            $data['category_data'] = [];
            if ($arr_c_id!='undefined') {
                foreach ($category_array as $c_id) {
                    $category_query = $this->db->get_where('tb_category', "c_id={$c_id}");
                    $category_res = $category_query->result_array();
                    if ($category_res) {
                        array_push($data['category_data'], $category_res[0]);
                    }
                }
                $cptitle_query = "select * from tb_category where c_id = {$data['cp_id']}";
                $cptitle_result = $this->db->query($cptitle_query)->result();
                $data['cp_title'] = $cptitle_result[0]->c_title;
            }
            $child_query = $this->db->get_where('tb_work_output', "w_id={$w_id}");
            $data['child_data'] = $child_query->result_array();
            $cnt=is_null($data) ? 0 : count($data);
            $status = $cnt>0?"00":"29";
        }
        return class_return_refactoring($status, $data);
    }

    public function get_cart($a_id)
    {
        // $where['tb_work.a_id'] = $a_id;
        $where['tb_work.w_status'] = "0";
        $where['tb_payment.p_no IS NULL'] = null;
        $or_where['tb_payment.p_status'] = "0";

        $this->db->select('tb_work.*,tb_user.*,tb_payment.*,tb_category.cp_id,tb_enterprise.e_company');
        $this->db->from('tb_work');
        $this->db->join('tb_payment', 'tb_work.p_no = tb_payment.p_no', 'left');
        $this->db->join('tb_category', 'tb_work.c_id = tb_category.c_id', 'left');
        $this->db->join('tb_enterprise', 'tb_work.e_id = tb_enterprise.e_id', 'left');
        $this->db->join('tb_user', 'tb_work.u_no = tb_user.u_no', 'left');
        $this->db->where($where);
        $this->db->or_where($or_where);
        $this->db->order_by('w_date', 'asc');
        $query = $this->db->get();
        // $query = $this->db->get_compiled_select();
        // print_r2($query);
        $data = $query->result_array();
        $status = "00";

        return class_return_refactoring($status, $data);
    }

    public function get_category_in_result($a_id, $w_id)
    {
        $where["w_id"] = $w_id;
        $this->db->from('tb_work');
        $this->db->where($where);
        $query = $this->db->get();
        $data = $query->row_array();
        
        
        $where["w_id"] = $w_id;
        $this->db->select('c_id,c_title,');
        $query = $this->db->get_where('tb_category', "a_id = {$a_id} and c_id in ({$data['c_id']})");
        $categories = $query->result_array();
        return $categories;
    }

    public function patch_result($field, $value)
    {
        $table = "tb_work";
        $cntAffected = 0;
        $w_id = $this->input->post('w_id');
        if ($w_id>0) {
            $w_array[0] = $w_id;
        } else {
            $w_list = $this->input->post('w_list');
            $w_array = explode(",", $w_list);
        }
        $status = "19";
        $winstatus = "00";
        $data = [];
        $msg = '';
        foreach ($w_array as $w_id) {
            if ($field=='w_status'&&$value==9) {
                $a_id = $this->input->post('a_id');
                $z_id = $this->input->post('z_id');
                $c_id = $this->input->post('c_id') ? $this->input->post('c_id') : 0;
                // print_r2($a_id.'/'.$z_id.'/'.$c_id);
                $result = $this->win_result($a_id, $z_id, $w_id, $c_id);
                $winstatus = $result['status'];
                $msg = $result['msg'];
            }
            if ($winstatus=="00") {
                $is_exist = $this->award_model->get_table_by_field($table, 'w_id', $w_id, true);
                if (is_null($is_exist)==false) {
                    $ab_data = [
                        $field=>$value
                        ];
                    $where = "w_id = '{$w_id}'";
                    $query = $this->db->update('tb_work', $ab_data, $where);
                    $affected = $query;//$this->db->affected_rows();
                    $cntAffected += $affected;
                }
            }
        }
        $status = $cntAffected>0?"00":"19";
        $data = ["cnt"=>$cntAffected];
        return class_return_refactoring($status, $data, $msg);
    }

    public function batch_result($status)
    {
        $result = $this->patch_result("w_status", $status);
        return class_return_refactoring($result['status'], $result['data'], $result['msg']);
    }

    public function get_judge_in_award($a_id, $j_id)
    {
        $where['a_id'] = $a_id;
        $where['j_id'] = $j_id;
        $query = $this->db->get_where('tb_judge', $where);
        return $query->result_array();
    }

    public function get_judge_party_in_award($a_id, $jp_id)
    {
        $where['a_id'] = $a_id;
        $where['jp_id'] = $jp_id;
        $query = $this->db->get_where('tb_judge_party', $where);
        return $query->result_array();
    }

    public function get_judge_in_result($a_id, $w_id)
    {
        $where["tb_work_judge.w_id"] = $w_id;
        $where["tb_judge.a_id"] = $a_id;
        $where["tb_judge_group.a_id"] = $a_id;
        // $where["tb_judge_group.jg_status"] = "1";
        $this->db->select('*');
        $this->db->from('tb_work_judge');
        $this->db->join('tb_judge', 'tb_work_judge.j_id = tb_judge.j_id', 'left');
        $this->db->join('tb_judge_group', 'tb_judge.jg_id = tb_judge_group.jg_type', 'left');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_judge_party_in_result($a_id, $w_id)
    {
        $where["tb_work_judge.w_id"] = $w_id;
        $where["tb_judge_party.a_id"] = $a_id;
        $this->db->select('tb_judge_party.jp_id,tb_judge_party.j_ids');
        $this->db->from('tb_work_judge');
        $this->db->join('tb_judge_party', 'tb_work_judge.jp_id = tb_judge_party.jp_id', 'left');
        $this->db->where($where);
        $query = $this->db->get();
        $rows = $query->result_array();

        $judge_party = [];
        foreach ($rows as $row) {
            $jp_id = $row['jp_id'];
            $j_id_arr = explode(',', $row['j_ids']);

            foreach ($j_id_arr as $j_id) {
                $where_j["tb_judge.j_id"] = $j_id;
                $where_j["tb_judge.a_id"] = $a_id;

                $this->db->select('*');
                $this->db->from('tb_judge');
                $this->db->where($where_j);
                $query = $this->db->get();
                $res = $query->row_array();
                $res['jp_id'] = $jp_id;
                array_push($judge_party, $res);
            }
        }

        return $judge_party;
    }

    public function get_judge_party_exist_in_result($a_id)
    {
        $table = "tb_work_judge";
        $w_list = $this->input->input_stream('w_list');
        $w_array = explode(",", $w_list);
        $w_exist = [];

        foreach ($w_array as $w_id) {
            $where_judge_party['w_id'] = $w_id;
            $where_judge_party['jp_id IS NOT NULL'] = null;
            $is_exist = $this->award_model->get_table_by_fields($table, $where_judge_party);
            if ($is_exist != 0) {
                // array_push($w_exist, $w_id);
                $where["tb_work.w_id"] = $w_id;
                // $this->db->select('*');
                $this->db->select('tb_work.w_id, tb_work.w_title, tb_work.p_no, tb_payment.p_subdate');
                $this->db->from('tb_work');
                $this->db->join('tb_payment', 'tb_work.p_no = tb_payment.p_no', 'left');
                $this->db->where($where);
                $query = $this->db->get();
                array_push($w_exist, $query->row_array());
            }
        }
        usort($w_exist, function($a, $b) { return strtotime($a['p_subdate']) - strtotime($b['p_subdate']); });
        return $w_exist;
    }

    public function get_judge_out_result($a_id, $w_id)
    {
        $where["tb_work_judge.j_id"] = null;
        $where["tb_judge.a_id"] = $a_id;
        $where["tb_judge_group.a_id"] = $a_id;
        // $where["tb_judge_group.jg_status"] = "1";
        $this->db->select('tb_judge.*,tb_judge_group.*');
        $this->db->from('tb_judge');
        $this->db->join('tb_work_judge', 'tb_work_judge.j_id = tb_judge.j_id', 'left');
        $this->db->join('tb_judge_group', 'tb_judge.jg_id = tb_judge_group.jg_id', 'left');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }


    public function set_judge($w_id)
    {
        $table = "tb_work";
        $is_exist = $this->award_model->get_table_by_field($table, 'w_id', $w_id);
        $status = "29";
        $a_id = $is_exist['a_id'];
        $wj_stats=0;
        $total_affected = 0;
        $msg = "";
        $data = [];
        if (count($is_exist)>0) {
            $j_list = $this->input->input_stream('j_list');
            $j_arr = explode(",", $j_list);
            $this->db->where('w_id', $w_id);
            $this->db->delete('tb_work_judge');
            foreach ($j_arr as $j_id) {
                $is_judge_in_award = $this->get_judge_in_award($a_id, $j_id);
                if (count($is_judge_in_award)>0) {
                    $where["w_id"] = $w_id;
                    $where["j_id"] = $j_id;
                    $query = $this->db->get_where('tb_work_judge', $where);
                    $j_row = $query->row_array();
                    if ($j_row==null) {
                        $jw_data = [
                            'w_id'=>$w_id,
                            'j_id'=>$j_id,
                            'wj_status'=>$wj_stats
                        ];
                        $query = $this->db->insert('tb_work_judge', $jw_data);
                        // $cntAffected = $this->db->affected_rows();
                        // $total_affected +=$cntAffected;
                    }
                }
            }
            // $data = ["cnt"=>$total_affected];
            // $status = $total_affected>0?"00":"19";
            $data = [];
            $status = "00";
        }
        return class_return_refactoring($status, $data);
    }

    public function set_judge_onlist()
    {
        $table = "tb_work";
        $w_list = $this->input->input_stream('w_list');
        $w_array = explode(",", $w_list);

        $is_exist_res = 1;
        foreach ($w_array as $w_id) {
            $is_exist = $this->award_model->get_table_by_field($table, 'w_id', $w_id);
            if ($is_exist == 0) {
                $is_exist_res = 0;
            }
        }

        $status = "29";
        $a_id = $is_exist['a_id'];
        $wj_stats=0;
        $total_affected = 0;
        $msg = "";
        $data = [];
        if ($is_exist_res>0) {
            $j_list = $this->input->input_stream('j_list');
            $j_arr = explode(",", $j_list);

            foreach ($w_array as $w_id) {
                $this->db->where('w_id', $w_id);
                $this->db->delete('tb_work_judge');
                foreach ($j_arr as $j_id) {
                    $is_judge_in_award = $this->get_judge_in_award($a_id, $j_id);
                    if (count($is_judge_in_award)>0) {
                        $where["w_id"] = $w_id;
                        $where["j_id"] = $j_id;
                        $query = $this->db->get_where('tb_work_judge', $where);
                        $j_row = $query->row_array();
                        if ($j_row==null) {
                            $jw_data = [
                                'w_id'=>$w_id,
                                'j_id'=>$j_id,
                                'wj_status'=>$wj_stats
                            ];
                            $query = $this->db->insert('tb_work_judge', $jw_data);
                            // $cntAffected = $this->db->affected_rows();
                            // $total_affected +=$cntAffected;
                        }
                    }
                }
            }
            // $data = ["cnt"=>$total_affected];
            // $status = $total_affected>0?"00":"19";
            $data = [];
            $status = "00";
        }
        return class_return_refactoring($status, $data);
    }

    public function set_judge_party_onlist()
    {
        $table = "tb_work";
        $w_list = $this->input->input_stream('w_list');
        $w_array = explode(",", $w_list);

        $is_exist_res = 1;
        foreach ($w_array as $w_id) {
            $is_exist = $this->award_model->get_table_by_field($table, 'w_id', $w_id);
            if ($is_exist == 0) {
                $is_exist_res = 0;
            }
        }

        $status = "29";
        $a_id = $is_exist['a_id'];
        $wj_stats=0;
        $total_affected = 0;
        $msg = "";
        $data = [];
        if ($is_exist_res>0) {
            $jp_list = $this->input->input_stream('jp_list');
            $j_arr = explode(",", $jp_list);

            foreach ($w_array as $w_id) {
                $this->db->where('w_id', $w_id);
                $this->db->delete('tb_work_judge');
                foreach ($j_arr as $jp_id) {
                    $is_judge_party_in_award = $this->get_judge_party_in_award($a_id, $jp_id);
                    if (count($is_judge_party_in_award)>0) {
                        $where["w_id"] = $w_id;
                        $where["jp_id"] = $jp_id;
                        $query = $this->db->get_where('tb_work_judge', $where);
                        $jp_row = $query->row_array();
                        if ($jp_row==null) {
                            $jw_data = [
                                'w_id'=>$w_id,
                                'jp_id'=>$jp_id,
                                'wj_status'=>$wj_stats
                            ];
                            $query = $this->db->insert('tb_work_judge', $jw_data);
                            // $cntAffected = $this->db->affected_rows();
                            // $total_affected +=$cntAffected;
                        }
                    }
                }
            }
            // $data = ["cnt"=>$total_affected];
            // $status = $total_affected>0?"00":"19";
            $data = [];
            $status = "00";
        }
        return class_return_refactoring($status, $data);
    }

    public function set_judge_party_reset()
    {
        $w_list = $this->input->input_stream('w_list');
        $w_array = explode(",", $w_list);

        $status = "29";
        $total_affected = 0;
        $msg = "";
        $data = [];

        foreach ($w_array as $w_id) {
            $this->db->where('w_id', $w_id);
            $this->db->delete('tb_work_judge');
            $cntAffected = $this->db->affected_rows();
            $total_affected += $cntAffected;
        }
        $data = ["cnt"=>$total_affected];
        $status = $total_affected>0?"00":"19";

        return class_return_refactoring($status, $data);
    }

    public function set_judgement($a_id, $w_id)
    {
        $table = "tb_work_judgement";
        $j_id = $this->input->input_stream('j_id');
        $jm_score = $this->input->input_stream('scores');
        $score_array = explode(",", $jm_score);

        // $where["tb_work_judge.w_id"] = $w_id;
        // $where["tb_judge_party.a_id"] = $a_id;
        // $this->db->select('tb_judge_party.jp_id,tb_judge_party.j_ids');
        // $this->db->from('tb_work_judge');
        // $this->db->join('tb_judge_party', 'tb_work_judge.jp_id = tb_judge_party.jp_id', 'left');
        // $this->db->where($where);
        // $query = $this->db->get();
        // $rows = $query->result_array();

        // $jp_ids = '';
        // foreach ($rows as $row) {
        //     $jp_ids != "" && $jp_ids .= ",";
        //     $jp_ids .= $row['jp_id'];
        // }

        $status = "29";
        $cntAffected = 0;
        $msg = "";
        $data = [];

        $where_j['w_id'] = $w_id;
        $where_j['j_id'] = $j_id;
        $this->db->where($where_j);
        $this->db->delete($table);

        $jm_data = [
            'w_id'=>$w_id,
            'j_id'=>$j_id,
            'jm_score'=>$jm_score,
            'jm_date'=>date('Y-m-d H:i:s')
        ];
        $query = $this->db->insert('tb_work_judgement', $jm_data);
        $affected = $query;
        $cntAffected += $affected;

        $status = $cntAffected>0?"00":"29";
        $data = ["cnt"=>$cntAffected];
        return class_return_refactoring($status, $data);
    }

    public function get_result_judgement($a_id)
    {
        $where["tb_work.a_id"] = $a_id;
        $this->db->select('tb_work.*,tb_user.*,tb_category.c_type,tb_category.cp_id,tb_enterprise.e_company,tb_work_judgement.jm_id,tb_work_judgement.j_id,tb_work_judgement.jm_score,tb_work_judgement.jm_date,tb_judge.j_name,tb_judge.jg_id,tb_judge.j_job,tb_judge.j_company');
        $this->db->from('tb_work_judgement');
        $this->db->join('tb_work', 'tb_work_judgement.w_id = tb_work.w_id', 'left');
        $this->db->join('tb_user', 'tb_work.u_no = tb_user.u_no', 'left');
        $this->db->join('tb_category', 'tb_work.c_id = tb_category.c_id', 'left');
        $this->db->join('tb_enterprise', 'tb_work.e_id = tb_enterprise.e_id', 'left');
        $this->db->join('tb_judge', 'tb_work_judgement.j_id = tb_judge.j_id', 'left');
        $this->db->where($where);
        $query = $this->db->get();
        $data = $query->result_array();

        foreach ($data as $key1 => $row) {
            $arr_c_id = $data[$key1]['c_id'];
            $category_array = explode(",", $arr_c_id);
            foreach ($category_array as $key2 => $c_id) {
                $category_query = $this->db->get_where('tb_category', "c_id={$c_id}");
                $category_res = $category_query->result();
                $data[$key1]['category_data'][$key2] = $category_res[0];
            }
            // $arr_jp_id = $data[$key1]['jp_ids'];
            // $jp_array = explode(",", $arr_jp_id);
            // $j_ids_all = '';
            // foreach ($jp_array as $jp_id) {
            //     $jp_query = $this->db->get_where('tb_judge_party', "jp_id={$jp_id}");
            //     $jp_res = $jp_query->row_array();
            //     $j_ids_all != "" && $j_ids_all .= ",";
            //     $j_ids_all .= $jp_res['j_ids'];
            // }
            // $j_array = explode(",", $j_ids_all);
            // $judge_data = [];
            // foreach ($j_array as $key2 => $j_id) {
            //     $j_query = $this->db->get_where('tb_judge', "j_id={$j_id}");
            //     $j_res = $j_query->row_array();
            //     $judge_data[$key2] = $j_res;

            // }
            // $data[$key1]['judge_data'] = $judge_data;
            $cptitle_query = "select * from tb_category where c_id = {$data[$key1]['cp_id']}";
            $cptitle_result = $this->db->query($cptitle_query)->result();
            $data[$key1]['cp_title'] = $cptitle_result[0]->c_title;
            $child_query = $this->db->get_where('tb_work_output', "w_id={$data[$key1]['w_id']}");
            $data[$key1]['child_data'] = $child_query->result_array();
        }
        $cnt=is_null($data) ? 0 : count($data);
        $status = $cnt>0?"00":"29";
        return class_return_refactoring($status, $data);
    }

    public function remove_result()
    {
        return $this->patch_result("is_del", "Y");
    }

    public function get_prize_by_field($a_id, $field, $value)
    {
        $where[$field] = $value;
        $where['a_id'] = $a_id;
        $where['is_del']='N';

        $query = $this->db->get_where('tb_prize', $where);
        return $query->row_array();
    }

    public function get_work_by_field($a_id, $field, $value)
    {
        $where[$field] = $value;
        $where['a_id'] = $a_id;
        $where['is_del']='N';
        $query = $this->db->get_where('tb_work', $where);
        return $query->row_array();
    }

    public function win_result($a_id, $z_id, $w_id, $c_id)
    {
        $a_table = "tb_award";
        $z_table = "tb_prize";
        $w_table = "tb_work";
        $c_table = "tb_category";
        $is_exist_a = $this->award_model->get_table_by_field($a_table, 'a_id', $a_id);
        $is_exist_z = $this->get_prize_by_field($a_id, 'z_id', $z_id);
        $is_exist_w = $this->get_work_by_field($a_id, 'w_id', $w_id);
        // $is_exist_c = $this->award_model->get_table_by_field($c_table, 'c_id', $c_id);
        $status = "29";
        $cntAffected = 0;
        $data = [];
        $msg = "수상할 데이터를 확인할 수 없습니다.";

        if (is_null($is_exist_a)==false&&is_null($is_exist_z)==false&&is_null($is_exist_w)==false) {
            $where["a_id"] = $a_id;
            $pr_row = null;
            if ($is_exist_z["z_only"]=="Y") {
                $where["z_id"] = $z_id;
                $query = $this->db->get_where('tb_prize_receiv', $where);
                $pr_row = $query->row_array();
                if (is_null($pr_row)==false) {
                    // $msg = "1회만 수상가능한 클래스 입니다.";
                    $msg = "prizeOnly";
                }
            }
            $where["w_id"] = $w_id;
            $query = $this->db->get_where('tb_prize_receiv', $where);
            $my_pr_row = $query->row_array();
            if (is_null($my_pr_row)==false) {
                // $msg = "이미 수상한 출품작 입니다.";
                // $msg = "prizeAlready";
                $query = $this->db->delete('tb_prize_receiv', $where);
            }

            $zr_id = 0;
            // if (is_null($pr_row)==true&&is_null($my_pr_row)==true) {
            if (is_null($pr_row)==true) {
                $zr_data = [
                    'a_id'=>$a_id,
                    'z_id'=>$z_id,
                    'w_id'=>$w_id,
                    'u_no'=>$is_exist_w['u_no'],
                    'e_id'=>$is_exist_w['e_id'],
                    'r_status'=>'1'
                ];
                if ($c_id >0) {
                    $zr_data['c_id'] = $c_id;
                }
                $query = $this->db->insert('tb_prize_receiv', $zr_data);
                $zr_id = $this->db->insert_id();
            }
            $status = $zr_id>0?"00":"29";
            if ($status=="00") {
                $this->admin_model->logging_admin("{$is_exist_w['w_title']} 수상처리 ({$is_exist_z['z_title']}상 수상)");
            }
            $result = $zr_id>0?$this->award_model->get_table_by_field('tb_prize_receiv', 'zr_id', $zr_id):"";
            $data = $result;
        }

        return class_return_refactoring($status, $data, $msg);
    }

    public function export_resultlist()
    {
        $w_id = $this->input->input_stream('w_id');
        if ($w_id>0) {
            $rlist = $w_id;
        } else {
            $rlist = $this->input->input_stream('result_list');
        }
        $data = [];

        if ($rlist) {
            $query = "SELECT @rn:=@rn+1 AS rank, z.*
                FROM (
                    select d.cp_id,d.c_title,c.e_company,a.w_title,a.w_url,a.w_start,a.w_end,a.w_client,a.w_coworker,a.w_comment,a.w_descript,b.u_id,g.p_subdate,a.wp_no,f.j_ids,case when a.w_status = '1' then '출품심사중' when a.w_status = '2' then '출품종료' when a.w_status = '3' then '출품보류' when a.w_status = '9' then '수상확정' when a.w_status = '-1' then '출품취소' else '출품대기' end as status,a.f_name,a.f_position,a.f_phone1,a.f_phone2,a.f_email
                    from tb_work as a
                    left join tb_user as b on a.u_no = b.u_no
                    left join tb_enterprise as c on b.e_id = c.e_id
                    left join tb_category as d on a.c_id = d.c_id
                    left join tb_work_judge as e on a.w_id = e.w_id
                    left join tb_judge_party as f on e.jp_id = f.jp_id
                    left join tb_payment as g on a.p_no = g.p_no
                    where a.w_id in ({$rlist})
                    GROUP BY wp_no
                    ORDER BY g.p_subdate ASC
                ) z,
                (SELECT @rn:=0) y";
            $query = $this->db->query($query);
            $data = $query->result_array();
            array_multisort(array_column($data, 'p_subdate'), SORT_ASC, $data);

            foreach ($data as $key => $row) {
                $cptitle_query = "select * from tb_category where c_id = {$row['cp_id']}";
                $cptitle_result = $this->db->query($cptitle_query)->result();
                $data[$key]['cp_id'] = $cptitle_result[0]->c_title;

                $j_array = explode(",", $row['j_ids']);
                $judge_data = [];
                foreach ($j_array as $key2 => $j_id) {
                    $j_query = $this->db->get_where('tb_judge', "j_id={$j_id}");
                    $j_res = $j_query->row_array();
                    $judge_data[$key2] = $j_res['j_name'];

                }
                $judge = implode("/", $judge_data);
                $data[$key]['j_ids'] = $judge;
            }
        }
        return $data;
    }

    public function export_judgement()
    {
        $w_id = $this->input->input_stream('w_id');
        if ($w_id>0) {
            $jlist = $w_id;
        } else {
            $jlist = $this->input->input_stream('judgement_list');
        }
        $data = [];

        if ($jlist) {
            $query = "SELECT @rn:=@rn+1 AS rank, z.*
                FROM (
                    select d.cp_id,d.c_title,c.e_company,a.w_title,a.w_url,a.w_start,a.w_end,a.w_client,a.w_coworker,a.w_comment,a.w_descript,b.u_id,g.p_subdate,a.wp_no,h.jp_ids,
                        (substring_index(h.jm_score, ',', 1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 2), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 3), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 4), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 5), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 6), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 7), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 8), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 9), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 10), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 11), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 12), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 13), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 14), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 15), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 16), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 17), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 18), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 19), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 20), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 21), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 22), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 23), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 24), ',', -1) + substring_index(substring_index(concat(h.jm_score, ',0'), ',', 25), ',', -1)) as scores,h.jm_score,h.jm_date
                    from tb_work as a
                    left join tb_user as b on a.u_no = b.u_no
                    left join tb_enterprise as c on b.e_id = c.e_id
                    left join tb_category as d on a.c_id = d.c_id
                    left join tb_work_judge as e on a.w_id = e.w_id
                    left join tb_judge as f on e.j_id = f.j_id
                    left join tb_payment as g on a.p_no = g.p_no
                    left join tb_work_judgement as h on a.w_id = h.w_id
                    where a.w_id in ({$jlist})
                    GROUP BY wp_no
                    ORDER BY h.jm_date ASC
                ) z,
                (SELECT @rn:=0) y";
            $query = $this->db->query($query);
            $data = $query->result_array();
            array_multisort(array_column($data, 'jm_date'), SORT_ASC, $data);

            foreach ($data as $key => $row) {
                $cptitle_query = "select * from tb_category where c_id = {$row['cp_id']}";
                $cptitle_result = $this->db->query($cptitle_query)->result();
                $data[$key]['cp_id'] = $cptitle_result[0]->c_title;

                $arr_jp_id = $row['jp_ids'];
                $jp_array = explode(",", $arr_jp_id);
                $j_ids_all = '';
                foreach ($jp_array as $jp_id) {
                    $jp_query = $this->db->get_where('tb_judge_party', "jp_id={$jp_id}");
                    $jp_res = $jp_query->row_array();
                    $j_ids_all != "" && $j_ids_all .= ",";
                    $j_ids_all .= $jp_res['j_ids'];
                }
                $j_array = explode(",", $j_ids_all);
                $judge_data = [];
                foreach ($j_array as $key2 => $j_id) {
                    $j_query = $this->db->get_where('tb_judge', "j_id={$j_id}");
                    $j_res = $j_query->row_array();
                    $judge_data[$key2] = $j_res['j_name'];

                }
                $judge = implode("/", $judge_data);
                $data[$key]['jp_ids'] = $judge;
            }
            
        }
        return $data;
    }
}