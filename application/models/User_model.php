<?php
class User_model extends CI_Model
{
    public $default_rest_value = "N";
    public function __construct()
    {
        $this->load->database();
        $this->load->model('admin_model');
    }

    public function get_user($u_no = false, $rest_value=false)
    {
        $where['is_del'] = "N";
        if ($u_no === false) {
            if ($rest_value !== false) {
                $where["is_rest"] = $rest_value;
            }
            $this->db->select('tb_user.*,tb_enterprise.e_company');
            $this->db->from('tb_user');
            $this->db->join('tb_enterprise', 'tb_user.e_id = tb_enterprise.e_id', 'left');
            $this->db->where($where);
            $query = $this->db->get();
            $data = $query->result_array();
            $status = "00";
        } else {
            $where["u_no"] = $u_no;
            // $query = $this->db->get_where('tb_user', $where);
            $this->db->select('tb_user.*,tb_enterprise.e_company');
            $this->db->from('tb_user');
            $this->db->join('tb_enterprise', 'tb_user.e_id = tb_enterprise.e_id', 'left');
            $this->db->where($where);
            $query = $this->db->get();
            $data = $query->row_array();
            $cnt=is_null($data) ? 0 : count($data);
            $status = $cnt>0?"00":"29";
        }

        return class_return_refactoring($status, $data);
    }

    public function get_user_all()
    {
        $query = "select * from tb_user";
        $query = $this->db->query($query);
        $data = $query->result_array();
        $cnt=is_null($data) ? 0 : count($data);
        $status = $cnt>0?"00":"29";
        return class_return_refactoring($status, $data);
    }

    public function get_user_info($u_no)
    {
        $query = "SELECT a.*,b.*,case when c.cnt is null then 0 ELSE c.cnt end AS cnt_w,case when d.cnt is null then 0 ELSE d.cnt end  AS cnt_pr from tb_user as a 
        LEFT JOIN tb_enterprise as b on a.e_id = b.e_id 
        LEFT JOIN (SELECT COUNT(*) AS cnt,e_id from tb_work where e_id is not null group by e_id) AS c ON b.e_id = c.e_id
        LEFT JOIN (SELECT COUNT(*) AS cnt,e_id from tb_prize_receiv where e_id is not null group by e_id) AS d ON b.e_id = d.e_id
        where a.u_no = {$u_no}";
        $query = $this->db->query($query);
        $data = $query->row_array();
        $cnt=is_null($data) ? 0 : count($data);
        $status = $cnt>0?"00":"29";
        return class_return_refactoring($status, $data);
    }

    public function get_user_by_field($field, $value)
    {
        $where = [$field=>$value];
        $where['is_del']='N';
        $query = $this->db->get_where('tb_user', $where);
        return $query->row_array();
    }

    public function get_user_by_fields($where)
    {
        $where['is_del']='N';
        $query = $this->db->get_where('tb_user', $where);
        return $query->row_array();
    }

    public function get_enterprise_by_field($field, $value)
    {
        $where = [$field=>$value];
        $query = $this->db->get_where('tb_enterprise', $where);
        return $query->row_array();
    }

    public function get_enterprise_by_keyword($value)
    {
        $value = urldecode($value);
        $query = "select * from tb_enterprise where e_name like '%{$value}%' or e_company like '%{$value}%'";
        $query = $this->db->query($query);
        $rows = $query->result_array();
        $status = is_null($rows)==false?"00":"29";
        $return = ["msg"=>"","data"=>$rows,"status"=>$status];
        return $return;
    }

    public function get_enterprise_by_id($e_id)
    {
        $where = "e_id = '{$e_id}'";
        $query = $this->db->get_where('tb_enterprise', $where);
        $rows = $query->result_array();
        $status = is_null($rows)==false?"00":"29";
        $return = ["msg"=>"","data"=>$rows,"status"=>$status];
        return $return;
    }

    public function get_enterprise_allnew()
    {
        $query = "select * FROM tb_enterprise WHERE e_id > 498 ORDER BY e_company ASC";
        $query = $this->db->query($query);
        $data = $query->result_array();
        $cnt=is_null($data) ? 0 : count($data);
        $status = $cnt>0?"00":"29";
        return class_return_refactoring($status, $data);
    }

    public function set_enterprise($enterinfo)
    {
        $e_row = $this->get_enterprise_by_field("e_number", $enterinfo['e_number']);
        if ($e_row!=null) {
            $e_id = $e_row['e_id'];
        /*
        $e_data = [
                'e_company'=>$e_company,
                'e_name'=>$e_name,
                'e_email'=>$e_email,
                'e_business'=>$e_business,
                'e_sector'=>$e_sector,
                'e_taxemail'=>$e_taxemail,
                'e_registration'=>$e_registration
                ];

        $where = "e_id = '{$e_id}'";
        $query = $this->db->update('tb_enterprise', $u_data,$where);
        */
        } else {
            $query = $this->db->insert('tb_enterprise', $enterinfo);
            $e_id = $this->db->insert_id();
        }
        return $e_id;
    }

    public function update_enterprise($enterinfo)
    {
        $e_data = [];
        $affected_count = 0;
        if ($enterinfo['e_company']!=null) {
            $e_data['e_company'] = $enterinfo['e_company'];
        }
        if ($enterinfo['e_name']!=null) {
            $e_data['e_name'] = $enterinfo['e_name'];
        }
        if ($enterinfo['e_email']!=null) {
            $e_data['e_email'] = $enterinfo['e_email'];
        }
        // if ($enterinfo['e_site']!=null) {
            $e_data['e_site'] = $enterinfo['e_site'];
        // }
        if ($enterinfo['e_zipcode']!=null) {
            $e_data['e_zipcode'] = $enterinfo['e_zipcode'];
        }
        if ($enterinfo['e_address1']!=null) {
            $e_data['e_address1'] = $enterinfo['e_address1'];
        }
        if ($enterinfo['e_address2']!=null) {
            $e_data['e_address2'] = $enterinfo['e_address2'];
        }
        if ($enterinfo['e_number']!=null) {
            $e_data['e_number'] = $enterinfo['e_number'];
        }
        if ($enterinfo['e_business']!=null) {
            $e_data['e_business'] = $enterinfo['e_business'];
        }
        if ($enterinfo['e_sector']!=null) {
            $e_data['e_sector'] = $enterinfo['e_sector'];
        }
        // if ($enterinfo['e_taxemail']!=null) {
        //     $e_data['e_taxemail'] = $enterinfo['e_taxemail'];
        // }
        $where = "e_id = {$enterinfo['e_id']}";
        if (count($e_data)>0) {
            $query = $this->db->update('tb_enterprise', $e_data, $where);
            //$affected_count = $this->db->affected_rows();
        }
        return $query;
    }

    public function insert_user()
    {
        $u_id = $this->input->post('u_id');
        $is_exist = $this->get_user_by_field("u_id", $u_id);
        $status = "09";
        $msg = "u_id is already exist.";
        $data = [];
        if (is_null($is_exist)==true) {
            $u_password = $this->input->post('u_password');
            $u_name = $this->input->post('u_name');
            $u_phone1 = $this->input->post('u_phone1');
            $u_phone2 = $this->input->post('u_phone2');
            $u_email = $this->input->post('u_email');
            $agree_sms = $this->input->post('agree_sms');
            $agree_email = $this->input->post('agree_email');
            $is_auth = $this->input->post('is_auth');

            $e_number = $this->input->post('e_number');
            $e_company = $this->input->post('e_company');
            $e_name = $this->input->post('e_name');
            $e_email = $this->input->post('e_email');
            $e_site = $this->input->post('e_site');
            $e_zipcode = $this->input->post('e_zipcode');
            $e_address1 = $this->input->post('e_address1');
            $e_address2 = $this->input->post('e_address2');
            $e_phone = $this->input->post('e_phone');
            $e_business = $this->input->post('e_business');
            $e_sector = $this->input->post('e_sector');
            $e_taxemail = $this->input->post('e_taxemail');
            $e_registration = $this->input->post('e_registration');
    
            $e_row = $this->get_enterprise_by_field("e_number", $e_number);
            if ($e_row!=null) {
                $e_id = $e_row['e_id'];
            /*
            $e_data = [
                'e_company'=>$e_company,
                'e_name'=>$e_name,
                'e_email'=>$e_email,
                'e_business'=>$e_business,
                'e_sector'=>$e_sector,
                'e_taxemail'=>$e_taxemail,
                'e_registration'=>$e_registration
                ];

            $where = "e_id = '{$e_id}'";
            $query = $this->db->update('tb_enterprise', $u_data,$where);
            */
            } else {
                $e_data = [
                    'e_number'=>$e_number,
                    'e_company'=>$e_company,
                    'e_name'=>$e_name,
                    'e_email'=>$e_email,
                    'e_site'=>$e_site,
                    'e_zipcode'=>$e_zipcode,
                    'e_address1'=>$e_address1,
                    'e_address2'=>$e_address2,
                    'e_phone'=>$e_phone,
                    'e_business'=>$e_business,
                    'e_sector'=>$e_sector,
                    'e_taxemail'=>$e_taxemail,
                    'e_registration'=>$e_registration
                    ];
                $query = $this->db->insert('tb_enterprise', $e_data);
                $e_id = $this->db->insert_id();
            }
    
            $userinfo = [
                'u_id'=>$u_id,
                'u_name'=>$u_name,
                'u_phone1'=>$u_phone1,
                'u_phone2'=>$u_phone2,
                'u_email'=>$u_email,
                'e_id'=>$e_id,
                'agree_sms'=>$agree_sms,
                'agree_email'=>$agree_email,
                'is_auth'=>$is_auth,
                'u_datetime' => date('Y-m-d H:i:s')
            ];

            $u_no = $this->insert_user_data($userinfo, $u_password);
            $data = $this->get_user_by_field('u_no', $u_no);
            $cntAffected = is_null($data) ? 0 : count($data);
            $status = $cntAffected>0?"00":"29";
            
            if ($status=="00") {
                $this->admin_model->logging_admin("{$u_name}({$u_id})의 신규사용자 아이디 생성", $u_no);
            }
            $msg = "";
        }

        return class_return_refactoring($status, $data, $msg);
    }
    
    public function update_user()
    {
        $u_no = $this->input->post('u_no');
        $u_row = $this->get_user($u_no);
        $status = "19";
        $msg = "user info is not be founded";
        $data = [];
        if ($u_row['status']=="00") {
            // $u_password = $this->input->post('u_password');
            $u_name = $this->input->post('u_name');
            $u_phone1 = $this->input->post('u_phone1');
            $u_phone2 = $this->input->post('u_phone2');
            $u_email = $this->input->post('u_email');
            $agree_sms = $this->input->post('agree_sms');
            $agree_email = $this->input->post('agree_email');
            $userinfo =[
                'u_name'=>$u_name,
                'u_phone1'=>$u_phone1,
                'u_phone2'=>$u_phone2,
                'u_email'=>$u_email,
                'agree_sms'=>$agree_sms,
                'agree_email'=>$agree_email
            ];

            $e_id = $this->input->post('e_id');
            $e_company = $this->input->post('e_company');
            $e_name = $this->input->post('e_name');
            $e_email = $this->input->post('e_email');
            $e_site = $this->input->post('e_site');
            $e_zipcode = $this->input->post('e_zipcode');
            $e_address1 = $this->input->post('e_address1');
            $e_address2 = $this->input->post('e_address2');
            $e_number = $this->input->post('e_number');
            $e_business = $this->input->post('e_business');
            $e_sector = $this->input->post('e_sector');
            // $e_taxemail = $this->input->post('e_taxemail');

            $enterinfo = [
                "e_id"=>$e_id,
                "e_company"=>$e_company,
                "e_name"=>$e_name,
                "e_email"=>$e_email,
                "e_site"=>$e_site,
                "e_zipcode"=>$e_zipcode,
                "e_address1"=>$e_address1,
                "e_address2"=>$e_address2,
                "e_number"=>$e_number,
                "e_business"=>$e_business,
                "e_sector"=>$e_sector,
                // "e_taxemail"=>$e_taxemail
            ];
            $cntAffected = $this->update_enterprise($enterinfo);
            $cntAffected += $this->update_user_data($userinfo, $u_no, $u_password);
            $result = $this->get_user_info($u_no);
            $data = $this->get_user_by_field('u_no', $u_no);
            $status = $cntAffected>0?"00":"19";
            $msg = $cntAffected>0?"success":"failed";
            if ($status=="00") {
                $this->admin_model->logging_admin("{$u_name}({$data['u_id']})의 사용자정보 수정", $u_no);
            }
        }

        return class_return_refactoring($status, $data, $msg);
    }
    
    public function insert_user_data($userinfo, $password)
    {
        $this->db->set($userinfo);
        $this->db->set('u_password', "password('" . $password . "')", false);
        $query = $this->db->insert('tb_user');
        $u_no = $this->db->insert_id();
        return $u_no;
    }

    public function update_user_data($userinfo, $u_no, $u_password=null)
    {
        $this->db->set($userinfo);
        if ($u_password!==null) {
            $this->db->set('u_password', "password('" . $u_password . "')", false);
        }
        $this->db->where('u_no', $u_no);
        $query = $this->db->update('tb_user');
        //$affected_count = $this->db->affected_rows();
        return $query;
    }

    public function reset_user_password()
    {
        $u_no = $this->input->post('u_no');
        $u_password = $this->input->post('u_password');
        $is_exist = $this->get_user_info($u_no);
        $result = false;
        $status = "19";
        $msg = "회원정보를 찾을 수 없습니다.";
        if (is_null($is_exist)==false) {
            $msg = "입력한 비밀번호가 없습니다.";
            if ($u_password!==null) {
                $this->db->set('u_password', "password('" . $u_password . "')", false);
                $this->db->where("u_no ", $u_no);
                $result = $this->db->update('tb_user');
                $status = $result!=false?"00":$status;
                $msg = $result!=false?"비밀번호를 변경하였습니다.":"비밀번호 변경이 실패하였습니다.";
            }
        }
        return class_return_refactoring($status, [], $msg);
    }

    public function patch_user($field, $value)
    {
        $u_no = $this->input->input_stream('u_no');
        if ($u_no>0) {
            $u_array[0] = $u_no;
        } else {
            $u_list = $this->input->input_stream('u_list');
            $u_array = explode(",", $u_list);
        }
        $total_affected = 0;
        $u_data = [
            $field=>$value
        ];
        foreach ($u_array as $u_no) {
            $u_row = $this->get_user($u_no);
            if ($u_row['status']=="00") {
                $where = "u_no = '{$u_no}'";
                $query = $this->db->update('tb_user', $u_data, $where);
                $cntAffected = $query==true?1:0;//$this->db->affected_rows();
                $total_affected += $cntAffected;
                $user_row = $this->get_user($u_no);
                $this->admin_model->logging_admin("{$user_row['data']['u_name']}({$user_row['data']['u_id']})의 상태값 수정", $u_no);
            }
        }
        $status = $total_affected>0?"00":"19";
        $result = $this->get_user(false, $this->default_rest_value);
        $data = $result['data'];
        return class_return_refactoring($status, $data);
    }

    public function delete_user()
    {
        $this->default_rest_value = "N";
        $u_no = $this->input->post('u_no');
        $u_password = $this->input->post('u_password');
        $this->db->where('u_no', $u_no);
        $this->db->where("u_password = password('" . $u_password . "')", null, false);
        $query = $this->db->get('tb_user');
        $data = $query->result_array();
        $msg = "회원정보를 찾을 수 없거나 비밀번호가 일치하지 않습니다.";
        $status = "19";
        $result_data = [];
        if (count($data)>0) {
            $data_set = [
                "u_password"=>"",
                "u_name"=>"no data",
                "u_phone1"=>"",
                "u_phone2"=>"",
                "u_email"=>"",
                "e_id"=>0,
                "is_del"=>"Y",
                "u_deldate"=>"curdate()"
            ];
            $this->db->where('u_no', $u_no);
            $this->db->where("u_password = password('" . $u_password . "')", null, false);
            $query = $this->db->update('tb_user', $data_set);
            $status = $query?"00":"19";
            $msg = $query?"탈퇴처리가 완료되었습니다.":$msg;
        }
        return class_return_refactoring($status, $result_data, $msg);
    }

    public function get_enterprise($e_id = false)
    {
        if ($e_id === false) {
            $query = $this->db->get('tb_enterprise');
            return $query->result_array();
        }
        $where["e_id"] = $e_id;
        $query = $this->db->get_where('tb_enterprise', $where);
        return $query->row_array();
    }

    public function get_olduser_auth()
    {
        $userId = $this->input->post('old_id');//soccer211
        $password =  $this->input->post('old_password');//soccer211
        //echo $password_decoding = $this->olduser_decoding_passwd($password);
        $query = "select * from userinfo where UserId = '{$userId}'";
        $result =  $this->db->query($query);
        $rows = $result->row_array();
        return $rows;
    }

    public function get_olduser_info($userId)
    {
        $query = "select * from userinfo where UserId = '{$userId}'";
        $result =  $this->db->query($query);
        $rows = $result->row_array();
        return $rows;
    }

    public function olduser_decoding_passwd($pwd)
    {
        $key = "123456789abcdef";
        $ciphertext = openssl_encrypt($pwd, 'AES-128-CBC', $key, false, null);
        // decrypt에서 오류가 발생해 PKCS5로 패딩을 맞춰주고 보내야한다면.
        echo $ciphertext;
    }

    public function update_registration($e_id, $img_path)
    {
        $img_data = ["e_registration"=>$img_path];
        $this->db->set($img_data);
        $this->db->where('e_id', $e_id);
        $query = $this->db->update('tb_enterprise');
    }

    public function export_userlist()
    {
        $u_no = $this->input->input_stream('u_no');
        if ($u_no>0) {
            $ulist = $u_no;
        } else {
            $ulist = $this->input->input_stream('u_list');
        }
        $data = [];
        if ($ulist) {
            $query = "select a.u_id,a.u_name,a.u_phone1,a.u_phone2,a.u_email,b.e_company,b.e_name,b.e_email,b.e_address1,b.e_address2,b.e_zipcode,b.e_number,b.e_business,b.e_sector,a.u_datetime,a.u_lastime
                from tb_user as a
                left join tb_enterprise as b on a.e_id = b.e_id
                where a.u_no in ({$ulist})";
            $query = $this->db->query($query);
            $data = $query->result_array();
        }
        return $data;
    }

    public function update_rest_notice()
    {
        $group = "us";
        $type = "rest";
        $query = "select * from tb_user where u_lastime < DATE_sub(NOW(), INTERVAL 3 year) and is_del = 'N' and is_rest = 'N' and is_rest_notice = 'N';";
        $u_data = ["is_rest"=>"Y","is_rest_notice"=>"Y"];
        $query = $this->db->query($query);
        foreach ($query->result() as $row) {
            $res_sms = Notice::send_notification("sms", $row->u_no, $group, $type);
            $res_mail = Notice::send_notification("mail", $row->u_no, $group, $type);
            $where = ["u_no"=>$row->u_no];
            $query = $this->db->update('tb_user', $u_data, $where);
        }
    }

    public function update_email_auth($u_no)
    {
        $u_data = ["is_auth"=>"Y"];
        $where = ["u_no"=>$u_no];
        $query = $this->db->update('tb_user', $u_data, $where);
        return $query;
    }

    public function update_delete_by_schedule()
    {
        $data_set = [
            "u_password"=>"",
            "u_name"=>"no data",
            "u_phone1"=>"",
            "u_phone2"=>"",
            "u_email"=>"",
            "e_id"=>0,
            "is_del"=>"Y",
            "u_deldate"=>"curdate()"
        ];
        $this->db->where("u_deldate = curdate()", null, false);
        $query = $this->db->update('tb_user', $data_set);
        $this->db->query($query);
    }
}
