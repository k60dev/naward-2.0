<?php
class Live_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get_live($type, $menu, $l_id=false)
    {
        $where["l_type"] = $type;
        $where["l_menu"] = $menu;
        if ($l_id === false) {
            $query = $this->db->get_where('tb_live', $where);
            $data = $query->result_array();
            $status = "00";
        } else {
            $where["l_id"] = $l_id;
            $query = $this->db->get_where('tb_live', $where);
            $data = $query->row_array();
            $cnt=is_null($data) ? 0 : count($data);
            $status = $cnt>0?"00":"29";
        }

        return class_return_refactoring($status, $data);
    }

    public function insert_live($info)
    { //어워드 정보 구하기
        $l_type = $info['type'];
        $l_menu = $info['menu'];
        $l_contents = $info['contents'];
        $l_url = $info['url'];
        $l_data = [
            "l_type"=>$l_type,
            "l_menu"=>$l_menu,
            "l_contents"=>$l_contents,
            "l_url"=>$l_url
        ];
        $this->db->set($l_data);
        $this->db->set('wdate', "now()", false);
        $query = $this->db->insert('tb_live');
        $l_id = $this->db->insert_id();
        $status = $l_id>0?"00":"09";
        $result = $this->get_live($l_type, $l_menu);
        $data = $status=="00"?$result['data']:[];
        $msg = $l_id>0?"입력 성공!":"라이브데이터 입력이 실패하였습니다.";
        return class_return_refactoring($status, $data, $msg);
    }

    public function modify_live($l_id, $info)
    {
        $l_type = $info['type'];
        $l_menu = $info['menu'];
        $l_contents = $info['contents'];
        $l_url = $info['url'];
        $l_data = [
            "l_type"=>$l_type,
            "l_menu"=>$l_menu,
            "l_contents"=>$l_contents,
            "l_url"=>$l_url
        ];
        $this->db->set($l_data);
        $this->db->set('wdate', "now()", false);
        $this->db->where('l_id', $l_id);
        $query = $this->db->update('tb_live');
        $cnt = $this->db->affected_rows();
        $status = $cnt>0?"00":"19";
        $result = $this->get_live($l_type, $l_menu);
        $data = $status=="00"?$result['data']:[];
        $msg = $l_id>0?"수정 성공!":"라이브데이터 입력이 실패하였습니다.";
        return class_return_refactoring($status, $data, $msg);
    }
}
