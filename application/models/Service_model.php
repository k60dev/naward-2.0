<?php
class Service_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->load->model('award_model');
        $this->load->model('user_model');
    }
    
    public function get_olduserinfo()
    {
        /*
        $userId = $this->input->post('userId');//soccer211
        $password =  $this->input->post('password');//soccer211
        $query = "select * from userinfo where UserId = '{$userId}'";
        $result =  $this->db->query($query);
        $rows = $result->result_array();
        print_r($rows);
        echo $password_decoding = $this->user_model->olduser_decoding_passwd($password);
        */
        $plaintext = "kdea7206@";
        $key = "123456789abcdef";

        $ciphertext = openssl_encrypt($plaintext, 'aes-128-ecb', $key, true);
        // decrypt에서 오류가 발생해 PKCS5로 패딩을 맞춰주고 보내야한다면.
        echo $ciphertext = base64_encode($ciphertext).strlen(base64_encode($ciphertext));
        echo "<br>SxAGiPz+1JuzqZgZau8b9A==".strlen("SxAGiPz+1JuzqZgZau8b9A==");
        echo "<Br>";
        echo $ciphertext = "SxAGiPz+1JuzqZgZau8b9A==";
        echo $plaintext = openssl_decrypt($ciphertext, 'aes-128-ecb', $key, true);
        /*
        */
        echo" <pre>";
        $ciphers = openssl_get_cipher_methods();
        print_r($ciphers);
        echo "</pre>";
    }

    public function get_award_router($router)
    {
        $query = "select * from tb_award where award_route = '{$router}'";
        $sql = $this->db->query($query);
        $data = $sql->row_array();
        return $data;
    }

    public function get_award_exist($a_id)
    {
        $query = "select * from tb_award where a_id = '{$a_id}'";
        $sql = $this->db->query($query);
        $data = $sql->row_array();
        return $data;
    }

    public function get_before_award()
    {
        $query = "SELECT a.* FROM tb_award AS a LEFT JOIN tb_award AS b ON a.a_id < b.a_id WHERE b.a_status = 1 ORDER BY a.a_id DESC LIMIT 1";
        $sql = $this->db->query($query);
        $data = $sql->row_array();
        return $data;
    }

    private function pbkdf1($pass, $salt, $countBytes)
    {
        if ($this->pbkdfState == 0) {
            $this->pbkdfHashno = 0;
            $this->pbkdfState = 1;
            $key = $pass . $salt;
            $this->pbkdfBase = sha1($key, true);
            for ($i = 2; $i < $this->iterations; $i++) {
                $this->pbkdfBase = sha1($this->pbkdfBase, true);
            }
        }
        $result = '';
        if ($this->pbkdfExtracount > 0) {
            $rlen = strlen($this->pbkdfExtra) - $this->pbkdfExtracount;
            if ($rlen >= $countBytes) {
                $result = substr($this->pbkdfExtra, $this->pbkdfExtracount, $countBytes);
                if ($rlen > $countBytes) {
                    $this->pbkdfExtracount += $countBytes;
                } else {
                    $this->pbkdfExtra = null;
                    $this->pbkdfExtracount = 0;
                }
                return $result;
            }
            $result = substr($this->pbkdfExtra, $rlen, $rlen);
        }
        $current = '';
        $clen = 0;
        $remain = $countBytes - strlen($result);
        while ($remain > $clen) {
            if ($this->pbkdfHashno == 0) {
                $current = sha1($this->pbkdfBase, true);
            } elseif ($this->pbkdfHashno < 1000) {
                $num = sprintf('%d', $this->pbkdfHashno);
                $tmp = $num . $this->pbkdfBase;
                $current .= sha1($tmp, true);
            }
            $this->pbkdfHashno++;
            $clen = strlen($current);
        }
        // $current now holds at least as many bytes as we need
        $result .= substr($current, 0, $remain);
        // Save any left over bytes for any future requests
        if ($clen > $remain) {
            $this->pbkdfExtra = $current;
            $this->pbkdfExtracount = $remain;
        }
        return $result;
    }
    
    public function set_join($auth_code, $str_date)
    {
        $join_date = date("Y-m-d H:i:s", $str_date);
        $input = $this->input;
        $u_id = $input->post('u_id');
        $u_name = $input->post('u_name');
        $u_password = $input->post('u_password');
        $u_phone1 = $input->post('u_phone1');
        $u_phone2 = $input->post('u_phone2');
        $u_email = $input->post('u_email');
        $agree_sms = $input->post('agree_sms')?$input->post('agree_sms'):0;
        $agree_email = $input->post('agree_email')?$input->post('agree_email'):0;

        /* 회사정보 */
        $e_company = $input->post('e_company');
        $e_name = $input->post('e_name');
        $e_email = $input->post('e_email');
        $e_site = $input->post('e_site');

        $e_zipcode = $input->post('e_zipcode');
        $e_address1 = $input->post('e_address1');
        $e_address2 = $input->post('e_address2');
        $e_number = $input->post('e_number');
        // $e_registration = $input->post('e_registration');
        $e_business = $input->post('e_business');
        $e_sector = $input->post('e_sector');
        // $e_taxemail = $input->post('e_taxemail');
        
        $status = "09";

        $msg = $u_id==""?"회원아이디를 입력해 주세요.":"";
        $msg = $msg==""&&$u_password==""?"회원 비밀번호를 입력해 주세요.":$msg;
        $msg = $msg==""&&$u_name==""?"회원이름을 입력해 주세요.":$msg;
        // $msg = $msg==""&&$u_phone1==""?"유선 전화번호를 입력해 주세요.":$msg;
        $msg = $msg==""&&$u_phone2==""?"무선 전화번호를 입력해 주세요.":$msg;
        $msg = $msg==""&&$u_email==""?"이메일 주소를 입력해 주세요.":$msg;
        $msg = $msg==""&&$e_company==""?"회사이름을 입력해 주세요.":$msg;
        $msg = $msg==""&&$e_name==""?"대표자 성함을 입력해 주세요.":$msg;
        $msg = $msg==""&&$e_email==""?"회사 메일을 입력해 주세요.":$msg;
        $msg = $msg==""&&($e_address1==""||$e_zipcode=="")?"회사 주소를 입력해 주세요.":$msg;
        $msg = $msg==""&&$e_address2==""?"회사 상세주소를 입력해 주세요.":$msg;
        $msg = $msg==""&&$e_number==""?"사업자 등록번호를 입력해 주세요.":$msg;
        // $msg = $msg==""&&$e_registration!=""?"사업자등록증을 입력해 주세요.":$msg;
        $msg = $msg==""&&$e_business==""?"업태를 입력해 주세요.":$msg;
        $msg = $msg==""&&$e_sector==""?"업종을 입력해 주세요.":$msg;
        // $msg = $msg==""&&$e_taxemail==""?"전자세금계산서 수취이메일을 입력해 주세요.":$msg;
        
        if ($msg=="") {
            $status = "09";
            $e_data = [
                'e_number'=>$e_number,
                'e_company'=>$e_company,
                'e_name'=>$e_name,
                'e_email'=>$e_email,
                'e_site'=>$e_site,
                'e_zipcode'=>$e_zipcode,
                'e_address1'=>$e_address1,
                'e_address2'=>$e_address2,
                'e_business'=>$e_business,
                'e_sector'=>$e_sector,
                // 'e_taxemail'=>$e_taxemail
                ];
            $e_id = $this->user_model->set_enterprise($e_data);
            $msg = "이미 사용중인 회원 아이디입니다.";
            $is_exist = $this->user_model->get_user_by_field("u_id", $u_id);
            $delete_str = strtotime(date("Y-m-d"))+86400;
            $delete_date = date("Y-m-d", $delete_str);

            if (is_null($is_exist)==true) {
                $userinfo = [
                    'u_id'=>$u_id,
                    'u_name'=>$u_name,
                    'u_phone1'=>$u_phone1,
                    'u_phone2'=>$u_phone2,
                    'u_email'=>$u_email,
                    'e_id'=>$e_id,
                    'agree_sms'=>$agree_sms,
                    'agree_email'=>$agree_email,
                    'u_datetime' => $join_date,
                    'u_deldate' => $delete_date,
                    'auth_code'=>$auth_code
                ];
                $u_no = $this->user_model->insert_user_data($userinfo, $u_password);
                $msg = $u_no>0?"회원가입이 완료되었습니다.":"회원가입을 실패하였습니다.";
                $status = $u_no>0?"00":"09";
            }
        }

        $result = ["msg"=>$msg,"status"=>$status,"data"=>["u_no"=>$u_no,"u_id"=>$u_id,"u_email"=>$u_email,"e_id"=>$e_id]];
        return $result;
    }

    public function find_userid()
    {
        $input = $this->input;
        $u_id = $input->post('u_id');
        $row = $this->user_model->get_user_by_field("u_id", $u_id);
        $result_msg = is_null($row)==true?"사용 가능한 아이디 입니다.":"사용할 수 없는 아이디 입니다.";
        $result_code = is_null($row)==true?"00":"89";
        $data = [];
        return class_return_refactoring($result_code, $data, $result_msg);
    }

    public function find_useremail()
    {
        $input = $this->input;
        $u_email = $input->post('u_email');
        $row = $this->user_model->get_user_by_field("u_email", $u_email);
        $result_msg = is_null($row)==true?"사용 가능한 이메일 입니다.":"사용할 수 없는 이메일 입니다.";
        $result_code = is_null($row)==true?"00":"89";
        $data = [];
        return class_return_refactoring($result_code, $data, $result_msg);
    }

    public function get_userinfo()
    {
        $input = $this->input;
        $u_id = $input->post('u_id');
        $u_password = $input->post('u_password');
        $query = "SELECT a.*,b.ua_id,b.ua_auth,is_super FROM tb_user AS a LEFT JOIN tb_user_admin AS b ON a.u_no = b.u_no where a.u_id = '{$u_id}' and a.u_password = password('{$u_password}')";
        $sql = $this->db->query($query);
        $data = $sql->row_array();
        $return['data'] = $data;
        $return['code'] = '00';
        if (is_null($data)==true) {
            $return['code'] = '69';
            $uid_query = "select * from tb_user where u_id = '{$u_id}'";
            $uid_sql = $this->db->query($uid_query);
            $uid_data = $uid_sql->row_array();

            $upw_query = "select password('{$u_password}') as upw";
            $upw_sql = $this->db->query($upw_query);
            $upw_data = $upw_sql->row_array();
            $return['is_exist_id'] = is_null($uid_data)==true?false:true;
            $return['is_right_pw'] = is_null($uid_data)==true?true:($uid_data['u_password']==$upw_data['upw']?true:false);
        }

        return $return;
    }

    public function stamp_lastime($u_no)
    {
        $query = "update tb_user set u_lastime = now(), u_deldate = ADDDATE(now(), INTERVAL 731 day) where u_no = {$u_no}";//2년뒤 다음날 일괄삭제 스크립트 구동
        $this->db->query($query);
    }

    public function auto_delete_user()
    {
        $query = "update tb_user set is_del = 'Y' where u_deldate = curdate()";//2년뒤 다음날 일괄삭제 스크립트 구동
        $this->db->query($query);
    }

    public function get_judge($a_id)
    {
        $status = "00";
        $query = "SELECT * FROM tb_judge_group AS a LEFT JOIN tb_judge AS b ON a.jg_id = b.jg_id where a.a_id = {$a_id} and a.jg_status = 0";
        $result = $this->db->query($query);
        $data = $result->result_array();
        return class_return_refactoring($status, $data);
    }

    public function get_judgement_data($a_id, $route, $j_id)
    {
        $data = [];
        $win_class_query = "SELECT c_title,c_describe,c_id,cp_id 
            from tb_category 
            where a_id = {$a_id} and cp_id != 0 
            order by c_order";
        $win_class_result = $this->db->query($win_class_query);
        foreach ($win_class_result->result() as $row) {
            $win_query = "SELECT b.*,e.jm_id,e.j_id,e.jm_score,e.jm_date,d.e_company,CONCAT(c.wo_url,c.wo_name) AS resource 
            from tb_judge_party AS a
            LEFT JOIN tb_work_judge AS z ON a.jp_id = z.jp_id
            LEFT JOIN tb_work AS b ON z.w_id = b.w_id
            LEFT JOIN tb_work_output AS c ON b.w_id = c.w_id
            LEFT JOIN tb_enterprise AS d ON b.e_id = d.e_id
            LEFT JOIN tb_work_judgement AS e ON b.w_id = e.w_id AND {$j_id} = e.j_id
            WHERE a.jp_access = '{$route}' AND a.a_id = {$a_id} AND b.c_id = {$row->c_id}
            GROUP BY c.w_id
            ORDER BY
            (case when ASCII(SUBSTRING(b.w_title, 1)) = 0 then 9
                when (ASCII(SUBSTRING(b.w_title, 1)) >= 33 and ASCII(SUBSTRING(b.w_title, 1)) <= 47) then 1
                when (ASCII(SUBSTRING(b.w_title, 1)) >= 58 and ASCII(SUBSTRING(b.w_title, 1)) <= 64) then 2
                when (ASCII(SUBSTRING(b.w_title, 1)) >= 91 and ASCII(SUBSTRING(b.w_title, 1)) <= 96) then 3
                when (ASCII(SUBSTRING(b.w_title, 1)) >= 123 and ASCII(SUBSTRING(b.w_title, 1)) <= 126) then 4
                when (ASCII(SUBSTRING(b.w_title, 1)) >= 48 and ASCII(SUBSTRING(b.w_title, 1)) <= 57) then 5
                when ASCII(SUBSTRING(b.w_title, 1)) > 128 then 7
                when ASCII(SUBSTRING(b.w_title,1)) = 32 then 8
                else 6 end ),binary(b.w_title)";

            $win_result = $this->db->query($win_query);
            foreach ($win_result->result() as $row2) {
                $row->child[] = $row2;
            }
            $data[] = $row;
        }
        
        $status = count($data)>0?"00":"29";
        $msg = count($data)>0?"":"작품 정보가 없습니다.";
        $result = ["status"=>$status,"data"=>$data,"msg"=>$msg];
        return $result;
    }

    public function give_prize()
    {
        $input = $this->input;
        $a_id = $input->post('a_id');
        $w_id = $input->post('w_id');
        $z_id = $input->post('z_id');
        $j_id = $input->post('j_id');
        $config_where['a_id'] = $a_id;

        $j_where = $config_where;
        $j_where['j_id'] = $j_id;
        $j_data = $this->award_model->get_table_by_fields("tb_judge", $j_where);

        $w_where = $config_where;
        $w_where['w_id'] = $w_id;
        $w_data = $this->award_model->get_table_by_fields("tb_work", $w_where);
        
        $z_where = $config_where;
        $z_where['z_id'] = $z_id;
        $z_data = $this->award_model->get_table_by_fields("tb_prize", $z_where, true);
        $status = "19";
        $msg = "관련 데이터를 확인할 수 없습니다.";

        $return_data = [];
        if (is_null($j_data)==false&&is_null($w_data)==false&&is_null($z_data)==false) {
            $able_to_give=true;
            if ($z_data['z_only']=='Y') {
                $before_givecheck_data = $this->award_model->get_table_by_field("tb_prize_receiv", "z_id", $z_id);
                $able_to_give = is_null($before_givecheck_data)==true?true:false;
                $msg = $able_to_give==false?"단일 수상만 가능한 클래스입니다.":$msg;
            }

            $duplicate_work_prize["w_id"] = $w_id;
            $duplicate_work_prize["z_id"] = $z_id;
            $duplicate_work_prize["a_id"] = $a_id;
            $duplicate_givecheck_data = $this->award_model->get_table_by_fields("tb_prize_receiv", $duplicate_work_prize);
            $able_to_give = is_null($duplicate_givecheck_data)==true?true:false;
            $msg = $able_to_give==false?"해당 작품은 같은상을 수상하였습니다.":$msg;

            if ($able_to_give==true) {
                $receiver_info = $this->award_model->get_table_by_field("tb_user", "u_no", $w_data['u_no']);
                $pr_data = [
                    "a_id"=>$a_id,
                    "z_id"=>$z_id,
                    "w_id"=>$w_id,
                    "u_no"=>$w_data['u_no'],
                    "e_id"=>$receiver_info['e_id']
                ];

                $query = $this->db->insert('tb_prize_receiv', $pr_data);
                $zr_id = $this->db->insert_id();
                $status = $zr_id>0?"00":"19";
                $return_data = $zr_id>0?$this->award_model->get_table_by_field("tb_prize_receiv", "zr_id", $zr_id):[];
                $msg = "성공적으로 수상처리 하였습니다.";
            }
        }
        $result = ["status"=>$status,"data"=>$return_data,"msg"=>$msg];
        return $result;
    }

    public function authorize_mail()
    {
        $input = $this->input;
        $auth_code = $this->input->get("code");
        $stamp = $this->input->get("stamp");
        $idx = $this->input->get("idx");
        $date = date("Y-m-d H:i:s", $stamp);
        $u_id = base64_decode($idx);

        $where['u_datetime'] = $date;
        $where['u_id'] = $u_id;
        $data = [];
        $is_exist = $this->award_model->get_table_by_fields("tb_user", $where, true);

        $status = "19";
        $msg = "인증정보를 찾을 수 없습니다.";
        if (is_null($is_exist)==false) {
            $msg = "이미 메일인증을 완료한 회원입니다.";
            if ($is_exist['is_auth']=='N') {
                $msg = "인증코드가 일치하지 않습니다.";
                if ($is_exist['auth_code']==$auth_code) {
                    $u_data = [
                        "is_auth" => 'Y'
                    ];
                    $this->stamp_lastime($is_exist['u_no']);
                    $query = $this->db->update('tb_user', $u_data, $where);
                    $cnt_affected = $this->db->affected_rows();
                    $status = $cnt_affected>0?"00":$status;
                    $msg = $cnt_affected>0?"메일인증에 성공하였습니다.":$msg;
                    $data = ['u_no'=>$is_exist['u_no']];
                }
            }
        }

        $result = ["status"=>$status,"msg"=>$msg,"data"=>$data];
        return $result;
    }

    public function get_award_by_route($router)
    {
        $router_info = $this->award_model->get_table_by_field("tb_award", "award_route", $router);
        return $router_info;
    }

    public function find_useremil()
    {
        $input = $this->input;
        $u_email = $input->post('u_email');
        $row = $this->user_model->get_user_by_field("u_email", $u_email);
        return $row;
    }

    public function find_userpassword()
    {
        $input = $this->input;
        // $u_id = $input->post('u_id');
        // $u_email = $input->post('u_email');
        // $where = ["u_id"=>$u_id,"u_email"=>$u_email];
        // $row = $this->user_model->get_user_by_fields($where);
        $u_email = $input->post('u_email');
        $row = $this->user_model->get_user_by_field("u_email", $u_email);
        return $row;
    }

    public function update_authcode($u_id, $auth_code)
    {
        $this->db->set('auth_code', $auth_code);
        $this->db->where('u_id', $u_id);
        $query = $this->db->update('tb_user');
    }

    public function update_password()
    {
        $status = "19";
        $input = $this->input;
        $u_password = $input->post('u_password');
        $auth_code = $input->post('auth_code');
        // $u_id = $input->post('u_id');
        $msg = "비밀번호를 입력해 주세요.";
        if ($u_password!="") {
            $this->db->set('u_password', "password('" . $u_password . "')", false);
            // $this->db->where('u_id', $u_id);
            $this->db->where('auth_code', $auth_code);
            $query = $this->db->update('tb_user');
            $cnt_affected = $this->db->affected_rows();
            $status = $cnt_affected>0?"00":$status;
            $msg = $cnt_affected>0?"비밀번호 변경이 성공하였습니다.":"잘못된 인증번호 입니다.";
        }
        $result = ["status"=>$status,"msg"=>$msg,"data"=>[]];
        return $result;
    }
}
