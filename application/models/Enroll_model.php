<?php
class Enroll_model extends CI_Model
{
    public $enroll_free_count = 7;
    public function __construct()
    {
        $this->load->database();
        $this->load->model('award_model');
        $this->load->model('user_model');
        $this->load->model('payment_model');
    }

    public function get_work_before_payment($a_id, $u_no)
    {
        // $query = "SELECT a.*,b.*,c.*,e.cp_id,e.c_title,case when CURDATE() > d.reaward_end then 1 ELSE 0 end as is_able_modify 
        $query = "SELECT a.*,b.sort_id,b.wo_name,b.wo_url,c.*,e.cp_id,e.c_title,case when NOW() > d.award_end then 1 ELSE 0 end as is_able_modify 
        FROM tb_work AS a 
        LEFT JOIN (
            SELECT w_id,sort_id,wo_name,wo_url FROM tb_work_output GROUP BY w_id ORDER BY sort_id desc
            ) AS b ON a.w_id = b.w_id 
        LEFT JOIN tb_payment AS c ON a.p_no = c.p_no 
        LEFT JOIN tb_award AS d ON a.a_id = d.a_id 
        LEFT JOIN tb_category AS e ON a.c_id = e.c_id 
        WHERE (c.p_no IS NULL OR c.p_status = 0) and a.u_no = {$u_no} AND d.a_id = {$a_id} and a.w_status >= 0
        ORDER BY a.w_id DESC";
        $status = "29";
        $msg = "등록한 내용이 없습니다.";
        $query = $this->db->query($query);
        $data = [];
        $payment_price_data = $this->enroll_model->get_payment_before_price($a_id, $u_no);
        $payment_price = $payment_price_data['data']['total_price'];
        foreach ($query->result() as $key1 => $row) {
            //echo "<br />payment_price : {$payment_price} , total_price : {$row->total_price}";
            $total_price_data = $row->total_price;
            $show_payment_price = $payment_price>0?($payment_price<$total_price_data?$payment_price:$total_price_data):0;
            $row->total_price = $show_payment_price;
            /* 7개 이상 무료 임시 제거 : start */
            // $row->total_price = $total_price_data;
            /* 7개 이상 무료 임시 제거 : end */
            $payment_price -= $show_payment_price;
            $row->price_dc = $show_payment_price < $total_price_data ? 'Y' : 'N';
            $data[] = $row;

            $arr_c_id = $data[$key1]->c_id;
            $category_array = explode(",", $arr_c_id);
            foreach ($category_array as $key2 => $c_id) {
                $category_query = $this->db->get_where('tb_category', "c_id={$c_id}");
                $category_res = $category_query->result();
                $data[$key1]->category_data[$key2] = $category_res[0];
            }
            $cptitle_query = "select * from tb_category where c_id = {$data[$key1]->cp_id}";
            $cptitle_result = $this->db->query($cptitle_query)->result();
            $data[$key1]->cp_title = $cptitle_result[0]->c_title;
        }

        $status = count($data)>0?"00":$status;
        $msg = count($data)>0?"":"결과값이 없습니다.";
        $result = ["status"=>$status,"data"=>$data,"msg"=>$msg];
        return $result;
    }

    public function get_work_before_payment_count($a_id, $u_no)
    {
        $query = "SELECT COUNT(*)
            FROM tb_work AS a
            LEFT JOIN tb_payment AS b ON a.p_no = b.p_no
            WHERE (b.p_no IS NULL OR b.p_status = 0) and a.u_no = {$u_no} and a.w_status >= 0 and a.a_id = {$a_id}
            ORDER BY a.w_id DESC";
        $status = "29";
        $msg = "등록한 내용이 없습니다.";
        $query = $this->db->query($query);
        $cnt = $query->row_array();
        $data = [ 'count' => $cnt['COUNT(*)']];
        $status = count($data)>0?"00":$status;
        $msg = count($data)>0?"":"결과값이 없습니다.";
        $result = ["status"=>$status,"data"=>$data,"msg"=>$msg];
        return $result;
    }

    public function enroll($a_id, $image_list, $key_image, $u_no)
    {
        $u_info = $this->user_model->get_user_by_field("u_no", $u_no);
        $is_exist = $this->award_model->get_award_by_field('a_id', $a_id);
        $status = "09";
        $msg = "어워드 정보를 찾을 수 없습니다.";
        if (is_null($is_exist)==false) {
            $table = "tb_work";
            $input = $this->input;
            $w_id = $input->post('w_id');
            if ($w_id) {
                $query = "select * from tb_work as a left join tb_payment as b on a.p_no = b.p_no where a.w_id = '{$w_id}'";
                $query = $this->db->query($query);
                $w_data = $query->row_array();
            }
            $c_id = $input->post('c_id');
            $w_title = $input->post('w_title');
            $w_url = $input->post('w_url');
            $w_start = $input->post('w_start');
            $w_end = $input->post('w_end');
            $w_type = $input->post('w_type');
            $w_coworker = $input->post('w_coworker');
            $w_client = $input->post('w_client');
            $w_comment = $input->post('w_comment');
            $w_descript = $input->post('w_descript');
            $f_name = $input->post('f_name');
            $f_position = $input->post('f_position');
            $f_phone1 = $input->post('f_phone1');
            $f_phone2 = $input->post('f_phone2');
            $f_email = $input->post('f_email');
            $e_id = $u_info['e_id'];

            $msg = $this->db->error();

            // 다중 카테고리 출품시 개별 등록 수정
            $array_c_id = explode(",", $c_id);

            foreach ($array_c_id as $key => $single_c_id) {
                $total_category = 1;
                $total_price = (int)$total_category*(int)$is_exist['work_price'];

                $j_data[$key] = [
                    "c_id"=>$single_c_id,
                    "a_id"=>$a_id,
                    "u_no"=>$u_no,
                    "e_id"=>$e_id,
                    "w_title"=>$w_title,
                    "w_url"=>$w_url,
                    "w_start"=>$w_start,
                    "w_end"=>$w_end,
                    "w_type"=>$w_type,
                    "w_client"=>$w_client,
                    "w_coworker"=>$w_coworker,
                    "w_comment"=>$w_comment,
                    "w_descript"=>$w_descript,
                    "f_name"=>$f_name,
                    "f_position"=>$f_position,
                    "f_phone1"=>$f_phone1,
                    "f_phone2"=>$f_phone2,
                    "f_email"=>$f_email,
                    //"w_thumbnail"=>$key_image['file_url']."/".$key_image['file_name'],
                    "total_category"=>$total_category,
                    "total_price"=>$total_price
                ];
                if ($key_image!=[]) {
                    $thumb = $key_image['file_url']."/".$key_image['file_name'];
                    $j_data[$key]['w_thumbnail'] = $thumb;
                    if ($w_id>0) {
                        storage_url_delete($w_data['w_thumbnail'], '');
                    }
                }
            }
            // 다중 카테고리 출품시 개별 등록 수정 전
            // $total_category = count($array_c_id);
            // $total_price = (int)$total_category*(int)$is_exist['work_price'];
            //
            // $j_data = [
            //     "c_id"=>$c_id,
            //     "a_id"=>$a_id,
            //     "u_no"=>$u_no,
            //     "e_id"=>$e_id,
            //     "w_title"=>$w_title,
            //     "w_url"=>$w_url,
            //     "w_start"=>$w_start,
            //     "w_end"=>$w_end,
            //     "w_type"=>$w_type,
            //     "w_client"=>$w_client,
            //     "w_coworker"=>$w_coworker,
            //     "w_comment"=>$w_comment,
            //     "w_descript"=>$w_descript,
            //     "f_name"=>$f_name,
            //     "f_position"=>$f_position,
            //     "f_phone1"=>$f_phone1,
            //     "f_phone2"=>$f_phone2,
            //     "f_email"=>$f_email,
            //     //"w_thumbnail"=>$key_image['file_url']."/".$key_image['file_name'],
            //     "total_category"=>$total_category,
            //     "total_price"=>$total_price
            // ];
            // if ($key_image['file_name']) {
            //     $j_data['w_thumbnail'] = $thumb;
            //     if ($w_id>0) {
            //         storage_url_delete($w_data['w_thumbnail'], '');
            //     }
            // }

            $max_output_id = 0;
            $max_output_id_pre = 0;

            if ($w_id>0) {
                $msg = "데이터를 찾을 수 없습니다.";
                $where = "w_id = '{$w_id}'";
                $max_output_query = "select * from tb_work_output where w_id = {$w_id} order by sort_id desc limit 1";
                $max_output_query = $this->db->query($max_output_query);
                $max_output_data = $max_output_query->row_array();
                // 다중 카테고리 출품시 개별 등록 수정
                $max_output_id_pre = $max_output_data['sort_id'];
                // 다중 카테고리 출품시 개별 등록 수정 전
                // $max_output_id = $max_output_data['sort_id'];

                $w_data = $query->row_array();
                $c_arr = explode(",", $c_id);
                $c_data_arr = explode(",", $w_data['c_id']);
                $w_data['p_status'];
                if ($w_data['w_status']>=0) {
                    if ($w_data['p_status']>0) {
                        $msg = "수정할 카테고리 갯수가 일치하지 않습니다.";
                        if (count($c_arr)==count($c_data_arr)) {
                            $u_data = ["c_id"=>$c_id];
                            $this->db->where('w_id', $w_id);
                            $result = $this->db->update($table, $u_data);
                            $status = $result==true?"00":"19";
                            $msg = $result==true?"수정이 완료되었습니다.":"수정 중 시스템 에러가 발생하였습니다.";
                        }
                    }
                    $query = $this->db->update($table, $j_data[0], $where);
                    $cnt_affected = $this->db->affected_rows();
                    $status = $cnt_affected>0?"00":$status;
                    $msg = $cnt_affected>0?"":$this->db->error();
                }
                $image_delete = $input->post('delete_list');
                if ($image_delete!="") {
                    $del_query = "select * from tb_work_output where wo_id in ({$image_delete})";
                    $del_query = $this->db->query($del_query);
                    foreach ($del_query->result() as $row) {
                        $dir = $row->wo_url;
                        $file = $row->wo_name;
                        storage_url_delete($dir, $file);
                        $this->db->where('wo_id', $row->wo_id);
                        $this->db->delete('tb_work_output');
                    }
                }
            } else {
                // 다중 카테고리 출품시 개별 등록 수정
                $arr_w_id = [];
                foreach ($j_data as $key => $single_j_data) {
                    $query = $this->db->insert($table, $single_j_data);
                    $arr_w_id[$key] = $this->db->insert_id();
                    $status = $arr_w_id[$key]>0?"00":$status;
                    $msg = $arr_w_id[$key]>0?"":$this->db->error();
                }
                // 다중 카테고리 출품시 개별 등록 수정 전
                // $query = $this->db->insert($table, $j_data);
                // $w_id = $this->db->insert_id();
                // $status = $w_id>0?"00":$status;
                // $msg = $w_id>0?"":$this->db->error();
            }
            /* 여기부터는 작업물 업로드 */
            // 다중 카테고리 출품시 개별 등록 수정
            if ($w_id>0 || count($arr_w_id)>0) {
                if ($w_id>0 && !is_array($w_id)) {
                    $arr_w_id[0] = $w_id;
                }
                foreach ($arr_w_id as $w_id) {
                    $max_output_id = $max_output_id_pre>0 ? $max_output_id_pre : 0;
                    foreach ($image_list as $img) {
                        $max_output_id++;
                        $child_table = "tb_work_output";
                        $wo_name = $img['file_name'];
                        $wo_origin_name = $img['origin_name'];
                        $wo_url = $img['file_url'];
                        $img_data = [
                            "a_id"=>$a_id,
                            "wo_name"=>$wo_name,
                            "wo_origin_name"=>$wo_origin_name,
                            "wo_url"=>$wo_url."/",
                            "w_id"=>$w_id,
                            "sort_id"=>$max_output_id
                        ];
                        $query = $this->db->insert($child_table, $img_data);
                    }
                }
            }
            // 다중 카테고리 출품시 개별 등록 수정 전
            // if ($w_id>0) {
            //     foreach ($image_list as $img) {
            //         $max_output_id++;
            //         $child_table = "tb_work_output";
            //         $wo_name = $img['file_name'];
            //         $wo_origin_name = $img['origin_name'];
            //         $wo_url = $img['file_url'];
            //         $img_data = [
            //             "a_id"=>$a_id,
            //             "wo_name"=>$wo_name,
            //             "wo_origin_name"=>$wo_origin_name,
            //             "wo_url"=>$wo_url."/",
            //             "w_id"=>$w_id,
            //             "sort_id"=>$max_output_id
            //         ];
            //         $query = $this->db->insert($child_table, $img_data);
            //     }
            // }
        }

        $result = ["status"=>$status,"data"=>[],"msg"=>$msg];
        return $result;
    }

    /*
    public function enroll_modify()
    {
        $table = "tb_work";
        $input = $this->input;
        $w_id = $input->post('w_id');
        $c_id = $input->post('c_id');
        $query = $this->db->get_where($table, ["w_id"=>$w_id]);
        $w_data = $query->row_array();
        $c_arr = explode(",", $c_id);
        $c_data_arr = explode(",", $w_data['c_id']);
        $status = "19";
        $msg = "데이터를 찾을 수 없습니다.";
        if (is_null($w_data)==false) {
            $msg = "카테고리 갯수가 일치하지 않습니다.";
            if (count($c_arr)>0&&count($c_arr)==count($c_data_arr)) {
                $u_data = ["c_id"=>$c_id];
                $this->db->where('w_id', $w_id);
                $result = $this->db->update($table, $u_data);
                $status = $result==true?"00":"19";
                $msg = $result==true?"수정이 완료되었습니다.":"수정 중 시스템 에러가 발생하였습니다.";
            }
        }
        $result = ["status"=>$status,"data"=>[],"msg"=>$msg];
        return $result;
    }
    */

    public function enroll_delete()
    {
        $table = "tb_work";
        $input = $this->input;
        $w_id = $input->post('w_id');
        $query = $this->db->get_where($table, ["w_id"=>$w_id]);
        $w_data = $query->row_array();
        $status = "19";
        $msg = "데이터를 찾을 수 없습니다.";
        if (is_null($w_data)==false) {
            $this->db->where('w_id', $w_id);
            $u_data = ["w_status"=>-1];
            $result = $this->db->update($table, $u_data);
            $status = $result==true?"00":"19";
            $msg = $result==true?"삭제가 완료되었습니다.":"처리 중 시스템 에러가 발생하였습니다.";
        }
        $result = ["status"=>$status,"data"=>[],"msg"=>$msg];
        return $result;
    }

    public function set_payment($a_id, $u_no)
    {
        $is_exist = $this->award_model->get_award_by_field('a_id', $a_id);
        $status = "09";
        $payment_info = [];
        $msg = "존재하지 않는 어워드 입니다.";
        if (is_null($is_exist)==false) {
            $input = $this->input;
            $table = "tb_payment";
            $child_table = "tb_work";
            //$w_list = "2630,2631";//$input->post("w_list");
            // 결제아이디가 붙은 아이템이라도 결제가 진행되지 않았다면 결제를 진행할지,,,?
            //$is_able_query = "SELECT * FROM {$child_table} as a left join {$table} as b on a.p_no = b.p_no WHERE a.w_id IN ({$w_list}) AND (a.p_no != '' OR a.p_no IS NULL) AND b.p_status = 0";
            //$is_able_query = "SELECT * FROM {$child_table} WHERE (p_no = '' OR p_no IS NULL) and u_no = {$u_no}"; //w_id IN ({$w_list}) AND
            $is_able_query = "SELECT * FROM {$child_table} as a left join {$table} as b on a.p_no = b.p_no  WHERE ( b.p_status IS null or b.p_status = 0 ) and a.u_no = {$u_no} and a.w_status >= 0 and a.a_id = {$a_id}";
            $is_able_result = $this->db->query($is_able_query);
            $w_list = "";
            foreach ($is_able_result->result() as $row) {
                $w_list .= ($w_list!=""?",":"").$row->w_id;
            }
            $w_array = explode(",", $w_list);
            $w_count = count($w_array);
            $is_able = $w_list != "" ?true:false;

            $msg = "결제대상 출품물이 없습니다.";
            if ($is_able==true) {
                $pay_no = $this->get_pno($a_id);
                $u_info = $this->user_model->get_user_by_field("u_no", $u_no);
                $e_id = $u_info['e_id'];
                $e_info = $this->user_model->get_enterprise_by_field("e_id", $e_id);
                $p_wdate = date("Y-m-d H:i:s");

                $idx = 0;
                foreach ($w_array as $w_id) {
                    $idx ++;
                    $pad = str_pad($idx, "3", "0", STR_PAD_LEFT);
                    $wp_no = "{$pay_no}-{$pad}";
                    $w_data = [
                                'p_no'=>$pay_no,
                                'wp_no'=>$wp_no,
                                'e_id'=>$e_id
                            ];
                    $this->db->where('w_id', $w_id);
                    $query = $this->db->update($child_table, $w_data);
                }
                $query = "SELECT sum(a.total_category) as total_category, case when SUM(a.total_price) IS NOT NULL then SUM(a.total_price) ELSE 0 END AS total_price,case when COUNT(a.w_id)>1 then CONCAT(b.w_title,' 외 ',COUNT(a.w_id)-1,'건') ELSE b.w_title end as p_title FROM tb_work AS a
                    LEFT JOIN (SELECT * from tb_work AS small group by small.p_no ORDER BY small.w_id) AS b ON a.w_id = b.w_id
                    WHERE a.w_id IN ({$w_list}) and a.w_status >= 0 and a.a_id = {$a_id}";
                $sql = $this->db->query($query);
                $row = $sql->row_array();
                $p_title = $row['p_title'];
                $enterprise_count = $this->award_model->cnt_enterprise_enroll($a_id, $e_id);
                $my_count = $row['total_category'];
                $calcul_count = $enterprise_count-$this->enroll_free_count;
                if ($calcul_count>0) {
                    $paid_count = 0;
                } else {
                    $free_count = $my_count+$calcul_count;
                    $paid_count = $free_count>0?$my_count-$free_count:$my_count;
                }
                /* 7개 이상 무료 임시 제거 : start */
                // $paid_count = $w_count;
                /* 7개 이상 무료 임시 제거 : end */
                $total_price = $paid_count*(int)$is_exist['work_price'];
                $pay_price = $total_price;

                $pay_data = [
                        'p_no' => $pay_no,
                        'u_no' => $u_no,
                        'e_id' => $e_id,
                        'a_id' => $a_id,
                        'p_title' => $p_title,
                        'p_wdate' => $p_wdate,
                        'p_paidcount' => $paid_count,
                        'p_totalprice' => $total_price
                    ];

                $query = $this->db->insert($table, $pay_data);
                $affected_cnt = $this->db->affected_rows();
                if ($affected_cnt>0) {
                    $u_name = $u_info['u_name'];
                    $u_email = $u_info['u_email'];
                    $u_phone1 = $u_info['u_phone1'];
                    $u_phone2 = $u_info['u_phone2'];
                    
                    $status = "00";
                    $msg = "";
                    $payment_info = [
                        "p_no" => $pay_no,
                        "p_title" => $p_title,
                        "pay_price" => $pay_price,
                        "u_no" => $u_no,
                        "u_name" => $u_name,
                        "u_email" => $u_email,
                        "u_phone1" => $u_phone1,
                        "u_phone2" => $u_phone2,
                        "e_company" => $e_info['e_company'],
                        "e_name" => $e_info['e_name'],
                        "e_email" => $e_info['e_email'],
                        "e_site" => $e_info['e_site'],
                        "e_zipcode" => $e_info['e_zipcode'],
                        "e_address1" => $e_info['e_address1'],
                        "e_address2" => $e_info['e_address2'],
                    ];
                }
            }
        }
        $return = ["status"=>$status,"data"=>$payment_info,"msg"=>$msg];
        return $return;
    }
    
    public function get_payment_before_price($a_id, $u_no)//결제창 넘어오기 전 리스트에서 체크
    {
        $table = "tb_payment";
        $child_table = "tb_work";
        $award_info = $this->award_model->get_award_by_field('a_id', $a_id);
        //$w_list = "2630,2631";//$input->post("w_list");
        // 결제아이디가 붙은 아이템이라도 결제가 진행되지 않았다면 결제를 진행할지,,,?
        //$is_able_query = "SELECT * FROM {$child_table} as a left join {$table} as b on a.p_no = b.p_no WHERE a.w_id IN ({$w_list}) AND (a.p_no != '' OR a.p_no IS NULL) AND b.p_status = 0";
        //$is_able_query = "SELECT * FROM {$child_table} WHERE (p_no = '' OR p_no IS NULL) and u_no = {$u_no}"; //w_id IN ({$w_list}) AND
        $is_able_query = "SELECT * FROM {$child_table} as a left join {$table} as b on a.p_no = b.p_no  WHERE ( b.p_status IS null or b.p_status = 0 ) and a.u_no = {$u_no} and a.w_status >= 0 and a.a_id = {$a_id}";
        $is_able_result = $this->db->query($is_able_query);
        $w_list = "";
        foreach ($is_able_result->result() as $row) {
            $w_list .= ($w_list!=""?",":"").$row->w_id;
        }
        $w_array = explode(",", $w_list);
        $w_count = count($w_array);
        $is_able = $w_list != "" ?true:false;
        $pay_price = 0;
        if ($is_able==true) {
            $u_info = $this->user_model->get_user_by_field("u_no", $u_no);
            $e_id = $u_info['e_id'];
            $query = "SELECT sum(a.total_category) as total_category FROM tb_work AS a
                LEFT JOIN (SELECT * from tb_work AS small group by small.p_no ORDER BY small.w_id) AS b ON a.w_id = b.w_id  
                WHERE a.w_id IN ({$w_list}) and a.w_status >= 0 and a.a_id = {$a_id}";
            $sql = $this->db->query($query);
            $row = $sql->row_array();
            $enterprise_count = $this->award_model->cnt_enterprise_enroll($a_id, $e_id);
            $my_count = $row['total_category'];
            $calcul_count = ($enterprise_count)-($this->enroll_free_count);
            if ($calcul_count>0) {
                $free_count = $my_count;
                $paid_count = 0;
            } else {
                $free_count = $my_count+$calcul_count;
                $paid_count = $free_count>0?$my_count-$free_count:$my_count;
            }
            /* 7개 이상 무료 임시 제거 : start */
            // $paid_count = $w_count;
            /* 7개 이상 무료 임시 제거 : end */
            $total_price = $paid_count*(int)$award_info['work_price'];
            // $pay_price = $total_price;
            $pay_data = [
                "total_price" => $total_price,
                "my_count" => (int)$my_count,
                "paid_count" => $paid_count,
                "free_count" => $free_count,
            ];
        }

        // return $pay_price;
        $status = "00";
        $msg = "";
        $result = ["status"=>$status,"data"=>$pay_data,"msg"=>$msg];
        return $result;
    }
    
    public function get_payment_price($p_no)
    {
        $table = "tb_payment";
        $child_table = "tb_work";
        $p_info = $this->award_model->get_table_by_field($table, "p_no", $p_no);
        $a_id = $p_info['a_id'];
        $award_info = $this->award_model->get_award_by_field('a_id', $a_id);
        $e_id = $p_info['e_id'];
        $u_no = $p_info['u_no'];
        //$w_list = "2630,2631";//$input->post("w_list");
        // 결제아이디가 붙은 아이템이라도 결제가 진행되지 않았다면 결제를 진행할지,,,?
        //$is_able_query = "SELECT * FROM {$child_table} as a left join {$table} as b on a.p_no = b.p_no WHERE a.w_id IN ({$w_list}) AND (a.p_no != '' OR a.p_no IS NULL) AND b.p_status = 0";
        //$is_able_query = "SELECT * FROM {$child_table} WHERE (p_no = '' OR p_no IS NULL) and u_no = {$u_no}"; //w_id IN ({$w_list}) AND
        $is_able_query = "SELECT * FROM {$child_table} as a left join {$table} as b on a.p_no = b.p_no  WHERE ( b.p_status IS null or b.p_status = 0 ) and a.p_no = '{$p_no}'";
        $is_able_result = $this->db->query($is_able_query);
        $w_list = "";
        foreach ($is_able_result->result() as $row) {
            $w_list .= ($w_list!=""?",":"").$row->w_id;
        }
        $w_array = explode(",", $w_list);
        $w_count = count($w_array);
        $is_able = $w_list != "" ?true:false;
        $pay_price = 0;
        if ($is_able==true) {
            $query = "SELECT sum(a.total_category) as total_category FROM tb_work AS a
                LEFT JOIN (SELECT * from tb_work AS small group by small.p_no ORDER BY small.w_id) AS b ON a.w_id = b.w_id  
                WHERE a.w_id IN ({$w_list})";
            $sql = $this->db->query($query);
            $row = $sql->row_array();
            $enterprise_count = $this->award_model->cnt_enterprise_enroll($a_id, $e_id);
            $my_count = $row['total_category'];
            $calcul_count = ($enterprise_count)-($this->enroll_free_count);
            if ($calcul_count>0) {
                $paid_count = 0;
            } else {
                $free_count = $my_count+$calcul_count;
                $paid_count = $free_count>0?$my_count-$free_count:$my_count;
            }
            /* 7개 이상 무료 임시 제거 : start */
            // $paid_count = $w_count;
            /* 7개 이상 무료 임시 제거 : end */
            $total_price = $paid_count*(int)$award_info['work_price'];
            $pay_price = $total_price;
        }

        return $pay_price;
    }

    public function complete_payment($u_no, $pay_info)
    {
        $table = "tb_payment";
        $method_array = ["1000000000"=>"credit","0100000000"=>"deposit"];

        $input = $this->input;
        $p_no = $input->post('sndOrdernumber');
        $p_method = $input->post('sndPaymethod');
        $p_price = $input->post('sndAmount');
        $enrollee_name = $input->post('enrollee_name');
        $enrollee_job = $input->post('enrollee_job');
        $enrollee_phone1 = $input->post('enrollee_phone1');
        $enrollee_phone2 = $input->post('enrollee_phone2');
        $enrollee_mail = $input->post('enrollee_mail');
        $enrollee_taxmail = $input->post('enrollee_taxmail');
        $p_method = $method_array[$p_method];
        $p_subdate = date("Y-m-d H:i:s");
        $p_findate = $p_method=="credit"?date("Y-m-d H:i:s"):"0000-00-00 00:00:00";
        $p_status = $pay_info['resultcd']==0?($p_method=="credit"?9:1):$pay_info['resultcd'];
        $p_result = $pay_info['trno'];
        $p_account = $pay_info['isscd'];

        $pay_data = [
            "p_method"=>$p_method,
            "p_subdate"=>$p_subdate,
            "p_findate"=>$p_findate,
            "p_status"=>$p_status,
            "p_totalprice"=>$p_price,
            "p_result"=>$p_result,
            "p_account"=>$p_account,
            "enrollee_name"=>$enrollee_name,
            "enrollee_job"=>$enrollee_job,
            "enrollee_phone1"=>$enrollee_phone1,
            "enrollee_phone2"=>$enrollee_phone2,
            "enrollee_mail"=>$enrollee_mail,
            "enrollee_taxmail"=>$enrollee_taxmail
        ];
        if ($p_method=='deposit') {
            $pay_data['p_limitdate'] = date('Y-m-d H:i:s', strtotime($p_subdate.'+3 day'));
            $p_accountname = $input->post('p_accountname');
            $pay_data['p_accountname'] = $p_accountname;
        }

        $this->db->where('p_no', $p_no);
        $query = $this->db->update($table, $pay_data);
        $affected_cnt = $this->db->affected_rows();

        /* 기존에 시도된 결제건들은 모두 삭제하기, 어차피 새 p_no부여되서 다시 결제 됩니다. */
        if ($affected_cnt>0) {
            $query = "delete from tb_payment where u_no = {$u_no} and p_status = 0";
            $this->db->query($query);

            $result = $this->payment_model->get_payment($p_no);
            return $result['data'];
        }
    }

    public function fail_payment($u_no, $pay_info)
    {
        $table = "tb_payment";
        $table2 = "tb_work";
        $method_array = ["1000000000"=>"credit","0100000000"=>"deposit"];

        $input = $this->input;
        $p_no = $input->post('sndOrdernumber');
        $p_method = $input->post('sndPaymethod');
        $p_method = $method_array[$p_method];
        $p_status = $pay_info['resultcd']==0?($p_method=="credit"?9:1):$pay_info['resultcd'];
        $w_status = '-1';

        $pay_data = [
            "p_method"=>$p_method,
            "p_status"=>$p_status
        ];
        $work_data = [
            "w_status"=>$w_status
        ];

        $this->db->where('p_no', $p_no);
        $query = $this->db->update($table, $pay_data);
        $this->db->where('p_no', $p_no);
        $query2 = $this->db->update($table2, $work_data);
        $affected_cnt = $this->db->affected_rows();

        /* 기존에 시도된 결제건들은 모두 삭제하기, 어차피 새 p_no부여되서 다시 결제 됩니다. */
        if ($affected_cnt>0) {
            $query = "delete from tb_payment where u_no = {$u_no} and p_status = 0";
            $this->db->query($query);

            $result = $this->payment_model->get_payment($p_no);
            return $result['data'];
        }
    }

    public function complete_payment_bank($u_no, $p_no)
    {
        $table = "tb_payment";
        $input = $this->input;
        $p_price = $input->post('pay_price');
        $p_accountname = $input->post('pay_accountname');
        $enrollee_name = "";//$input->post('pay_enrollee_name');
        $enrollee_job = "";//$input->post('pay_enrollee_job');
        $enrollee_phone1 = "";//$input->post('pay_enrollee_phone1');
        $enrollee_phone2 = "";//$input->post('pay_enrollee_phone2');
        $enrollee_mail = "";//$input->post('pay_enrollee_mail');
        $enrollee_taxmail = $input->post('pay_enrollee_taxmail');
        $request_taxbill = $input->post('pay_request_taxbill')==true?1:0;
        $p_method = "deposit";
        $p_subdate = date("Y-m-d H:i:s");
        $p_findate = "0000-00-00 00:00:00";
        $p_status = 1;
        $p_result = "20";
        $p_account = "11603818118973";

        $pay_data = [
            "p_method"=>$p_method,
            "p_subdate"=>$p_subdate,
            "p_findate"=>$p_findate,
            "p_status"=>$p_status,
            "p_totalprice"=>$p_price,
            "p_result"=>$p_result,
            "p_account"=>$p_account,
            "enrollee_name"=>$enrollee_name,
            "enrollee_job"=>$enrollee_job,
            "enrollee_phone1"=>$enrollee_phone1,
            "enrollee_phone2"=>$enrollee_phone2,
            "enrollee_mail"=>$enrollee_mail,
            "enrollee_taxmail"=>$enrollee_taxmail,
            "p_limitdate"=>date('Y-m-d H:i:s', strtotime($p_subdate.'+3 day')),
            "p_accountname"=>$p_accountname,
            "p_taxrequest"=>$request_taxbill
        ];

        $this->db->where('p_no', $p_no);
        $query = $this->db->update($table, $pay_data);
        $affected_cnt = $this->db->affected_rows();

        /* 기존에 시도된 결제건들은 모두 삭제하기, 어차피 새 p_no부여되서 다시 결제 됩니다. */
        if ($affected_cnt>0) {
            $query = "delete from tb_payment where u_no = {$u_no} and p_status = 0";
            $this->db->query($query);

            $result = $this->payment_model->get_payment($p_no);
            return $result['data'];
        }
        return $affected_cnt>0?true:false;
    }

    public function request_taxbill($p_no)
    {
        $table = "tb_payment";
        $enrollee_taxmail = $this->input->post('enrollee_taxmail');
        $p_taxrequest = 1;

        $pay_data = [
            "enrollee_taxmail"=>$enrollee_taxmail,
            "p_taxrequest"=>$p_taxrequest
        ];

        $this->db->where('p_no', $p_no);
        $query = $this->db->update($table, $pay_data);
        $affected_cnt = $this->db->affected_rows();
        $status = $affected_cnt>0?"00":"19";
        $data = $p_no;
        $msg = $affected_cnt>0?"success":"failed";
        return class_return_refactoring($status, $data, $msg);
    }

    public function get_pno($a_id)
    {
        $date = date("ymd");
        $rand = rand(0, 999);
        $rand = str_pad($rand, "3", "0", STR_PAD_LEFT);
        $p_no = "n_{$a_id}{$date}{$rand}";
        $table = "tb_payment";
        $is_exist = $this->award_model->get_table_by_field($table, 'p_no', $p_no);
        if (is_null($is_exist)===false) {
            return $this->get_pno($a_id);
        } else {
            return $p_no;
        }
    }

    public function delete_bank_payment()
    {
        $group = "ps";
        $type = "over";
        $query = "select * from tb_payment where p_method = 'deposit' and p_limitdate = curdate()";
        $query = $this->db->query($query);
        foreach ($query->result() as $row) {
            $res_sms = Notice::send_notification("sms", $row['u_no'], $group, $type);
            $res_mail = Notice::send_notification("mail", $row['u_no'], $group, $type);
        }
        $query = "delete from tb_payment where p_method = 'deposit' and p_limitdate = curdate()";
        $this->db->query($query);
    }
}
