<?php
class Receiv_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->load->model('award_model');
        $this->load->helper('common');
    }

    public function get_receiv($a_id, $zr_id=false, $param=false)
    {
        $where["tb_prize_receiv.a_id"] = $a_id;
        $where["tb_work.w_status"] = 9;
        $msg = "";
        if ($zr_id === false) {
            if ($param != false) {
                if ($param == 'announcement-complete') {
                    $where["tb_prize_receiv.r_status"] = 1;
                } elseif ($param == 'yearbook-request') {
                    $where["tb_prize_receiv.r_status"] = 2;
                } elseif ($param == 'yearbook-complete') {
                    $where["tb_prize_receiv.r_status"] = 3;
                } elseif ($param == 'closing') {
                    $where["tb_prize_receiv.r_status"] = 9;
                } else {
                    $where["tb_prize_receiv.r_status"] = 0;
                }
            };

            $this->db->select('tb_prize_receiv.*,tb_prize.*,tb_work.*,tb_category.cp_id,tb_enterprise.e_company,tb_payment.p_subdate');
            $this->db->from('tb_prize_receiv');
            $this->db->join('tb_prize', 'tb_prize.z_id = tb_prize_receiv.z_id', 'left');
            $this->db->join('tb_work', 'tb_work.w_id = tb_prize_receiv. w_id', 'left');
            $this->db->join('tb_payment', 'tb_payment.p_no = tb_work.p_no', 'left');
            $this->db->join('tb_category', 'tb_category.c_id = tb_work.c_id', 'left');
            $this->db->join('tb_user', 'tb_user.u_no = tb_prize_receiv.u_no', 'left');
            $this->db->join('tb_enterprise', 'tb_work.e_id = tb_enterprise.e_id', 'left');
            $this->db->where($where);
            $this->db->order_by('p_subdate', 'asc');
            $query = $this->db->get();
            $data = $query->result_array();
            foreach ($data as $key1 => $row) {
                $arr_c_id = $data[$key1]['c_id'];
                $category_array = explode(",", $arr_c_id);
                foreach ($category_array as $key2 => $c_id) {
                    $category_query = $this->db->get_where('tb_category', "c_id={$c_id}");
                    $category_res = $category_query->result();
                    $data[$key1]['category_data'][$key2] = $category_res[0];
                }
                $cptitle_query = "select * from tb_category where c_id = {$data[$key1]['cp_id']}";
                $cptitle_result = $this->db->query($cptitle_query)->result();
                $data[$key1]['cp_title'] = $cptitle_result[0]->c_title;
                $child_query = $this->db->get_where('tb_work_output', "w_id={$data[$key1]['w_id']}");
                $data[$key1]['child_data'] = $child_query->result_array();
            }
            $status = "00";
        } else {
            $where["tb_prize_receiv.zr_id"] = $zr_id;
            $this->db->select('tb_prize_receiv.*,tb_prize.*,tb_work.*,tb_user.*,tb_category.c_type,tb_category.cp_id,tb_enterprise.e_company,tb_payment.p_status');
            $this->db->from('tb_prize_receiv');
            $this->db->join('tb_prize', 'tb_prize.z_id = tb_prize_receiv.z_id', 'left');
            $this->db->join('tb_work', 'tb_work.w_id = tb_prize_receiv. w_id', 'left');
            $this->db->join('tb_payment', 'tb_payment.p_no = tb_work.p_no', 'left');
            $this->db->join('tb_category', 'tb_category.c_id = tb_work.c_id', 'left');
            $this->db->join('tb_user', 'tb_user.u_no = tb_prize_receiv.u_no', 'left');
            $this->db->join('tb_enterprise', 'tb_enterprise.e_id = tb_work.e_id', 'left');
            $this->db->where($where);
            $query = $this->db->get();
            $data = $query->row_array();
            $arr_c_id = $data['c_id'];
            $category_array = explode(",", $arr_c_id);
            $data['category_data'] = [];
            if ($arr_c_id!='undefined') {
                foreach ($category_array as $c_id) {
                    $category_query = $this->db->get_where('tb_category', "c_id={$c_id}");
                    $category_res = $category_query->result_array();
                    if ($category_res) {
                        array_push($data['category_data'], $category_res[0]);
                    }
                }
                $cptitle_query = "select * from tb_category where c_id = {$data['cp_id']}";
                $cptitle_result = $this->db->query($cptitle_query)->result();
                $data['cp_title'] = $cptitle_result[0]->c_title;
            }
            $child_query = $this->db->get_where('tb_work_output', "w_id={$data['w_id']}");
            $data['child_data'] = $child_query->result_array();
            $cnt=is_null($data) ? 0 : count($data);
            $status = $cnt>0?"00":"29";
        }
        return class_return_refactoring($status, $data);
    }

    public function get_receiv_prize($a_id, $w_id)
    {
        $where['tb_work.a_id'] = $a_id;
        $where["tb_work.w_id"] = $w_id;
        $this->db->select('tb_work.w_id,tb_prize_receiv.zr_id,tb_prize_receiv.z_id,tb_prize.z_title');
        $this->db->from('tb_work');
        $this->db->join('tb_prize_receiv', 'tb_prize_receiv.w_id = tb_work.w_id', 'left');
        $this->db->join('tb_prize', 'tb_prize.z_id = tb_prize_receiv.z_id', 'left');
        $this->db->where($where);
        $query = $this->db->get();
        $data = $query->row_array();
        $cnt=is_null($data) ? 0 : count($data);
        $status = $cnt>0?"00":"29";
        return class_return_refactoring($status, $data);
    }

    public function get_work_output($w_id)
    {
        $where['w_id'] = $w_id;
        $this->db->from('tb_work_output');
        $this->db->where($where);
        $query = $this->db->get();
        $data = $query->row_array();
        return $data;
    }

    public function update_comment($zr_id)
    {
        $table = 'tb_prize_receiv';
        $is_exist = $this->award_model->get_table_by_field($table, 'zr_id', $zr_id);
        $data = [];
        $status = 19;
        $msg = "데이터를 찾을 수 없습니다.";
        if (is_null($is_exist)==false) {
            $r_comment = $this->input->post('r_comment');
            $zr_data = ['r_comment'=>$r_comment];
            $where = "zr_id = '{$zr_id}'";
            $query = $this->db->update('tb_prize_receiv', $zr_data, $where);
            $affected = $query;//$this->db->affected_rows();
            $data = ["affected"=>$affected];
            $status = $affected>0?"00":"19";
            $msg = $status=="00"?"데이터 반영이 완료되었습니다.":"데이터를 수정할 수 없습니다.";
        }
        return class_return_refactoring($status, $data, $msg);
    }

    public function fetch_receiv($field, $value)
    {
        $table = 'tb_prize_receiv';
        $zr_id = $this->input->input_stream('zr_id');
        $zr_list = $this->input->input_stream('zr_list');
        if ($zr_id>0) {
            $zr_array[0] = $zr_id;
        } else {
            $zr_list = $this->input->input_stream('zr_list');
            $zr_array = explode(",", $zr_list);
        }
        $total_affected = 0;
        foreach ($zr_array as $zr_id) {
            $is_exist = $this->award_model->get_table_by_field($table, 'zr_id', $zr_id);
            $status = "19";
            $cntAffected = 0;
            $data = [];
            $msg = "";
            if (is_null($is_exist)==false) {
                $ab_data = [
                    $field=>$value
                    ];
                $where = "zr_id = '{$zr_id}'";
                $query = $this->db->update('tb_prize_receiv', $ab_data, $where);
                $total_affected += $query;//$this->db->affected_rows();
            }
        }
        $status = $total_affected>0?"00":"19";
        if ($status=="00") {
            $this->admin_model->logging_admin("수상관리 상태변경 ({$field} -> {$value}({$zr_id}))");
        }
        $data = ["total_affected"=>$total_affected];
        return class_return_refactoring($status, $data);
    }

    public function batch_receiv($status)
    {
        $data = $this->fetch_receiv("r_status", $status);
        return $data;
    }

    public function request_receiv()
    {
        $data = $this->fetch_receiv("r_request", "1");
        $data = $this->fetch_receiv("r_status", "2");
        return $data;
    }

    public function export_winnerlist()
    {
        $zr_id = $this->input->input_stream('zr_id');
        if ($zr_id>0) {
            $rlist = $zr_id;
        } else {
            $rlist = $this->input->input_stream('winner_list');
        }
        $data = [];

        if ($rlist) {
            $query = "SELECT @rn:=@rn+1 AS rank, z.*
                FROM (
                    select d.cp_id,d.c_title,c.e_company,a.w_title,a.w_url,a.w_start,a.w_end,a.w_client,a.w_coworker,a.w_comment,a.w_descript,b.u_id,g.p_subdate,a.wp_no,f.j_ids,h.z_title,case when x.r_status = '1' then '공고완료' when x.r_status = '2' then '연감요청' when x.r_status = '3' then '연감완료' when x.r_status = '9' then '종결' else '공고예정' end as status,a.f_name,a.f_position,a.f_phone1,a.f_phone2,a.f_email
                    from tb_prize_receiv as x
                    left join tb_work as a on x.w_id = a.w_id
                    left join tb_user as b on a.u_no = b.u_no
                    left join tb_enterprise as c on b.e_id = c.e_id
                    left join tb_category as d on a.c_id = d.c_id
                    left join tb_work_judge as e on a.w_id = e.w_id
                    left join tb_judge_party as f on e.jp_id = f.jp_id
                    left join tb_payment as g on a.p_no = g.p_no
                    left join tb_prize as h on x.z_id = h.z_id
                    where x.zr_id in ({$rlist})
                    GROUP BY wp_no
                    ORDER BY g.p_subdate ASC
                ) z,
                (SELECT @rn:=0) y";
            $query = $this->db->query($query);
            $data = $query->result_array();

            foreach ($data as $key => $row) {
                $cptitle_query = "select * from tb_category where c_id = {$row['cp_id']}";
                $cptitle_result = $this->db->query($cptitle_query)->result();
                $data[$key]['cp_id'] = $cptitle_result[0]->c_title;

                $j_array = explode(",", $row['j_ids']);
                $judge_data = [];
                foreach ($j_array as $key2 => $j_id) {
                    $j_query = $this->db->get_where('tb_judge', "j_id={$j_id}");
                    $j_res = $j_query->row_array();
                    $judge_data[$key2] = $j_res['j_name'];

                }
                $judge = implode("/", $judge_data);
                $data[$key]['j_ids'] = $judge;
            }
        }
        return $data;
    }
}