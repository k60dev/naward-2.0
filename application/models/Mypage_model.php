<?php
class Mypage_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->load->model('award_model');
        $this->load->model('user_model');
    }

    public function get_recipient($u_no, $limit = 0)
    {
        // $query = "SELECT a.w_id,a.w_title,a.w_date,a.w_start,a.w_end,a.p_no,a.wp_no,a.w_status,a.w_thumbnail,case when CURDATE() > b.reaward_end then 1 ELSE 0 end as is_able_modify
        $query = "SELECT a.a_id,a.w_id,a.w_title,a.w_date,a.w_start,a.w_end,a.p_no,a.wp_no,a.w_status,a.w_thumbnail,case when NOW() > b.award_end then 1 ELSE 0 end as is_able_modify
        FROM tb_work AS a
        LEFT JOIN tb_award AS b ON a.a_id = b.a_id
        WHERE a.u_no = '{$u_no}' and a.is_del = 'N'
        ORDER BY a.w_id desc ";
        $query .= $limit>0?"limit {$limit}":"";
        $sql = $this->db->query($query);
        return $sql->result_array();
    }

    public function get_payment($u_no, $limit = 0)
    {
        $query = "SELECT a.p_no,a.p_status,a.p_method,a.p_paidcount,a.p_totalprice,a.p_payprice,a.p_subdate,c.w_thumbnail,a.p_title,a.p_wdate
        FROM tb_payment AS a 
        LEFT JOIN (SELECT * from tb_work AS small group by small.p_no ORDER BY small.w_id) AS c ON a.p_no = c.p_no 
        LEFT JOIN (SELECT cnt.p_no,COUNT(w_id) AS w_cnt from tb_work AS cnt group by cnt.p_no ORDER BY cnt.w_id) AS d ON a.p_no = d.p_no 
        where a.u_no = '{$u_no}' and a.p_status != 0 
        order by a.p_wdate desc ";
        $query .= $limit>0?"limit {$limit}":"";
        $sql = $this->db->query($query);
        return $sql->result_array();
    }

    public function get_prize($u_no, $limit = 0)
    {
        $query = "SELECT b.a_id,b.a_year,a.c_id,c.z_title,d.w_id,d.w_title,e.cp_id,e.c_title,a.r_status,a.r_yearbook 
            FROM tb_prize_receiv AS a 
            LEFT JOIN tb_award AS b ON a.a_id = b.a_id 
            LEFT JOIN tb_prize AS c ON a.z_id = c.z_id 
            LEFT JOIN tb_work AS d ON a.w_id = d.w_id 
            LEFT JOIN tb_category AS e ON a.c_id = e.c_id 
            WHERE a.u_no = '{$u_no}' 
            order by a.zr_id desc ";
        $query .= $limit>0?"limit {$limit}":"";
        $sql = $this->db->query($query);
        $data = $sql->result_array();
        foreach ($data as $key1 => $row) {
            $cptitle_query = "select * from tb_category where c_id = {$row['cp_id']}";
            $cptitle_result = $this->db->query($cptitle_query)->result();
            $data[$key1]['cp_title'] = $cptitle_result[0]->c_title;
        }
        return $data;
    }

    public function get_profile($u_no)
    {
        $query = "select * from tb_user as a left join tb_enterprise as b on a.e_id = b.e_id where u_no = {$u_no}";
        $sql = $this->db->query($query);
        return $sql->row_array();
    }

    public function set_profile($u_no)
    {
        $input = $this->input;
        $u_name = $input->post('u_name');
        $u_phone1 = $input->post('u_phone1');
        $u_phone2 = $input->post('u_phone2');
        $u_email = $input->post('u_email');
        $agree_sms = $input->post('agree_sms')?$input->post('agree_sms'):0;
        $agree_email = $input->post('agree_email')?$input->post('agree_email'):0;
                
        $userinfo = [
                    'u_name'=>$u_name,
                    'u_phone1'=>$u_phone1,
                    'u_phone2'=>$u_phone2,
                    'u_email'=>$u_email,
                    'agree_sms'=>$agree_sms,
                    'agree_email'=>$agree_email,
                    'u_datetime' => date('Y-m-d H:i:s')
                ];
        $affected_count = $this->user_model->update_user_data($userinfo, $u_no);
        $status = $affected_count>0?"00":"99";
        return $status;
    }
}
