<?php
class Admin_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->load->model('user_model');
    }

    public function get_admin($ua_id=false)
    {
        if ($ua_id === false) {
            $string = "select * from tb_user_admin as a left join tb_user as b on a.u_no = b.u_no where b.is_del = 'N'";
            $query = $this->db->query($string);
            $data = $query->result_array();
            $status = "00";
        } else {
            $string = "select * from tb_user_admin as a left join tb_user as b on a.u_no = b.u_no where a.ua_id = {$ua_id} and b.is_del = 'N'";
            $query = $this->db->query($string);
            
            $data = $query->row_array();
            $cnt=is_null($data) ? 0 : count($data);
            $status = $cnt>0?"00":"29";
        }
        return class_return_refactoring($status, $data);
    }
    
    public function get_admin_by_field($field, $value)
    {
        $where = [$field=>$value];
        $query = "select * from tb_user_admin as a left join tb_user as b on a.u_no = b.u_no where a.{$field} = '{$value}'";
        $query = $this->db->query($query);
        return $query->row_array();
    }

    public function get_user_admin_by_field($field, $value)
    {
        $query = $this->db->get_where("select * from tb_user_admin as a left join tb_user as b on a.u_no = b.u_no where {$field} = {$value} and b.is_del = 'N'");
        return $query->row_array();
    }

    public function create_admin()
    {

        $u_id = $this->input->post('u_id');
        $u_password = $this->input->post('u_password');
        $u_name = $this->input->post('u_name');
        $u_phone1 = $this->input->post('u_phone1');
        $u_phone2 = $this->input->post('u_phone2');
        $u_email = $this->input->post('u_email');
        $agree_sms = '1';
        $agree_email = '1';
        $is_auth = 'Y';
        $e_id = '500';
        // $e_number = '000-00-00000';
        // $e_company = '앤어워드';

        $userinfo = [
            'u_id'=>$u_id,
            'u_name'=>$u_name,
            'u_phone1'=>$u_phone1,
            'u_phone2'=>$u_phone2,
            'u_email'=>$u_email,
            'e_id'=>$e_id,
            'agree_sms'=>$agree_sms,
            'agree_email'=>$agree_email,
            'is_auth'=>$is_auth,
            'u_datetime' => date('Y-m-d H:i:s')
        ];

        $u_no = $this->user_model->insert_user_data($userinfo, $u_password);

        $user_info_row = $this->user_model->get_user_by_field('u_no', $u_no);

        $u_no = $user_info_row['u_no'];
        $is_exist = $this->get_admin_by_field('u_no', $u_no);
        $status = "09";
        $cntAffected = 0;
        $data = [];
        $msg = "user admin is already exist";

        if (is_null($is_exist)==true) {
            $u_name = $user_info_row['u_name'];
            $ua_auth = $this->input->post('ua_auth');
            $is_super = $this->input->post('is_super');
            $u_data = [
                'u_no'=>$u_no,
                'ua_auth'=>$ua_auth,
                'is_super'=>$is_super
                ];
            $query = $this->db->insert('tb_user_admin', $u_data);
            $ua_id = $this->db->insert_id();
            
            $cntAffected = $this->db->affected_rows();
            $status = $cntAffected>0?"00":"09";
            if ($status=="00") {
                $this->logging_admin("{$u_name}의 신규관리자 아이디 생성", $u_no);
            }
            $msg = $status=="00"?"":"";
            $data = $this->get_admin_by_field("ua_id", $ua_id);
        }

        return class_return_refactoring($status, $data, $msg);
    }

    public function modify_admin()
    {
        $ua_id = $this->input->post('ua_id');
        $ua_row = $this->get_admin_by_field("ua_id", $ua_id);
        $status = "19";
        $data = [];
        if (is_null($ua_row)==false) {
            $ua_auth = $this->input->post('ua_auth');
            $is_super = $this->input->post('is_super');
            $u_name = $this->input->post('u_name');
            $u_phone1 = $this->input->post('u_phone1');
            $u_phone2 = $this->input->post('u_phone2');
            $u_email = $this->input->post('u_email');
            $ua_data = [
                'ua_auth'=>$ua_auth,
                'is_super'=>$is_super
                ];
            $where = "ua_id = '{$ua_id}'";
            $query = $this->db->update('tb_user_admin', $ua_data, $where);
            $u_no = $ua_row['u_no'];
            $u_where['u_no'] = $u_no;
            $u_data = [
                'u_name'=>$u_name,
                'u_phone1'=>$u_phone1,
                'u_phone2'=>$u_phone2,
                'u_email'=>$u_email
            ];
            $query = $this->db->update('tb_user', $u_data, $u_where);
            $u_row = $this->user_model->get_user_by_field("u_no", $u_no);
            $cntAffected = $this->db->affected_rows();
            $status = $cntAffected>0?"00":"19";
            if ($status=="00") {
                $this->logging_admin("{$u_name}의 관리자정보 수정");
            }
            $data = $this->get_admin_by_field("ua_id", $ua_id);
        }
        return class_return_refactoring($status, $data);
    }

    public function patch_admin($field, $value)
    {
        $ua_list = $this->input->post('ua_list');
        $ua_array = explode(",", $ua_list);
        $total_affected = 0;
        foreach ($ua_array as $ua_id) {
            $u_data = [
                $field=>$value
                ];
            $where = "ua_id = '{$ua_id}'";
            $query = $this->db->update('tb_user_admin', $u_data, $where);
            $cntAffected = $this->db->affected_rows();
            $total_affected += $cntAffected;
        }
        $status = $total_affected>0?"00":"19";
        $result = $this->get_admin();
        $data = $result['data'];
        return class_return_refactoring($status, $data);
    }

    public function batch_admin()
    {
        $auth_list = $this->input->post('auth_list');
        return $this->patch_admin("ua_auth", $auth_list);
    }

    public function remove_admin()
    {
        $u_no = $this->input->post('u_no');
        $ua_id = $this->input->post('ua_id');
        $query_u = "delete from tb_user where u_no = {$u_no}";
        $this->db->query($query_u);
        $query_ua = "delete from tb_user_admin where ua_id = {$ua_id}";
        $this->db->query($query_ua);
        $status = "00";
        $data = "";
        return class_return_refactoring($status, $data);
    }

    public function get_logs()
    {
        $where['1'] = '1';
        $query = $this->db->get_where('tb_user_admin_log', $where);
        $data = $query->result_array();
        $status = "00";
        return class_return_refactoring($status, $data);
    }
    
    public function logging_admin($label, $user_no=false)
    {
        $userstring = "{$this->session->userdata('u_name')}({$this->session->userdata('u_id')})";
        $log_text = "{$userstring}님이 {$label}";
        $ua_id = "1";
        $u_id = $this->session->userdata('u_id');
        $u_data = [
            'ua_id'=>$ua_id,
            'u_id'=>$u_id,
            'log_text'=>$log_text
            ];

        if ($user_no!==false) {
            $user_info = $this->user_model->get_user($user_no);
            $u_data['user_no'] = $user_no;
            $u_data['user_id'] = $user_info['data']['u_id'];
        }
        $query = $this->db->insert('tb_user_admin_log', $u_data);
        return $this->db->insert_id();
    }

    public function find_user_id()
    {
        $u_id = $this->input->input_stream('u_id');
        $data = $this->award_model->get_table_by_field('tb_user', 'u_id', $u_id, true);
        $data = $data==null?[]:$data;
        return $data;
    }
}
