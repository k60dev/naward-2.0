<script language="javascript" src="https://kspay.ksnet.to/store/KSPayWebV1.4/js/kspay_web_ssl.js"></script>
<script language="javascript">
function _submit(_frm)
{
    _frm.sndReply.value = getLocalUrl("process");
    _pay(_frm);
}

function getLocalUrl(mypage)
{
    var myloc = location.href;
    return myloc.substring(0, myloc.lastIndexOf('/')) + '/' + mypage;
}

// goResult() - 함수설명 : 결재완료후 결과값을 지정된 결과페이지(kspay_wh_result.php)로 전송합니다.
function goResult()
{
    document.KSPayWeb.target = "";
    document.KSPayWeb.action = "./receipt";
    document.KSPayWeb.submit();
}

// eparamSet() - 함수설명 : 결재완료후 (kspay_wh_rcv.php로부터)결과값을 받아 지정된 결과페이지(kspay_wh_result.php)로 전송될 form에 세팅합니다.
function eparamSet(rcid, rctype, rhash)
{
    document.KSPayWeb.reWHCid.value 	= rcid;
    document.KSPayWeb.reWHCtype.value = rctype  ;
    document.KSPayWeb.reWHHash.value 	= rhash  ;
}

function mcancel()
{
    // 취소
    closeEvent();
}
</script>

<body>
<form name=KSPayWeb method=post>
  <input type=hidden name=sndReply value="">
  <input type=hidden name=sndCharSet value="UTF-8">
  <input type=hidden name=sndGoodType value="2">
  <input type=hidden name=sndShowcard value="C">
  <input type=hidden name=sndCurrencytype value="WON">
  <input type=hidden name=iframeYn value="Y">
  <input type=hidden name=sndInstallmenttype value="ALL(0:2:3:4:5:6:7:8:9:10:11:12)">
  <input type=hidden name=sndInteresttype value="NONE">
  <input type=hidden name=sndStoreCeoName value="">
  <input type=hidden name=sndStorePhoneNo value="">
  <input type=hidden name=sndStoreAddress value="">
  <input type=hidden name=reWHCid value="">
  <input type=hidden name=reWHCtype value="">
  <input type=hidden name=reWHHash value="">
  <input type=hidden name=sndEscrow value="0">
  <input type=hidden name=sndCashReceipt value="0">

  <input type='hidden' name='sndStoreid' value='2756100009'>
  <select name=sndPaymethod>
    <option value="1000000000" selected>신용카드</option>
    <option value="0010000000">계좌이체</option>
  </select>
  <input type='text' name='sndOrdernumber' value='<?=$p_no?>' size='30'>
  <input type='text' name='sndGoodname' value='<?=$p_title?>' size='30'>
  <input type='text' name='sndAmount' value='<?=$pay_price?>' size='15' maxlength='9'>
  <input type='text' name='sndOrdername' value='<?=$u_name?>' size='30'>
  <input type='text' name='sndEmail' value='<?=$u_email?>' size='30'>
  <input type='text' name='sndMobile' value='<?=$u_phone1?>' size='12' maxlength='12'>
  <input type="button" value="결 제" onClick="javascript:_submit(document.KSPayWeb);">
</form>