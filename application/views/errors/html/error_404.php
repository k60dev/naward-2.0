<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>404 Page Not Found</title>
<link href="https://cdn.jsdelivr.net/npm/xeicon@2.3.3/xeicon.min.css" rel="stylesheet">
<style type="text/css">
*,
*:before,
*:after {
    margin: 0;
    padding: 0;
    box-sizing: inherit;
}
@font-face {
    font-family:'NotoSansKR';
    font-weight:300;
    font-style:normal;
    src:url("/assets/font/notosanskr/NotoSans-Light.eot");
    src:url("/assets/font/notosanskr/NotoSans-Light.eot?#iefix") format("embedded-opentype"),url("/assets/font/notosanskr/NotoSans-Light.woff2") format("woff2"),url("/assets/font/notosanskr/NotoSans-Light.woff") format("woff");
}
@font-face {
    font-family:'NotoSansKR';
    font-weight:400;
    font-style:normal;
    src:url("/assets/font/notosanskr/NotoSans-Regular.eot");
    src:url("/assets/font/notosanskr/NotoSans-Regular.eot?#iefix") format("embedded-opentype"),url("/assets/font/notosanskr/NotoSans-Regular.woff2") format("woff2"),url("/assets/font/notosanskr/NotoSans-Regular.woff") format("woff");
}
@font-face {
    font-family:'NotoSansKR';
    font-weight:500;
    font-style:normal;
    src:url("/assets/font/notosanskr/NotoSans-Medium.eot");
    src:url("/assets/font/notosanskr/NotoSans-Medium.eot?#iefix") format("embedded-opentype"),url("/assets/font/notosanskr/NotoSans-Medium.woff2") format("woff2"),url("/assets/font/notosanskr/NotoSans-Medium.woff") format("woff");
}
@font-face {
    font-family:'NotoSansKR';
    font-weight:700;
    font-style:normal;
    src:url("/assets/font/notosanskr/NotoSans-Bold.eot");
    src:url("/assets/font/notosanskr/NotoSans-Bold.eot?#iefix") format("embedded-opentype"),url("/assets/font/notosanskr/NotoSans-Bold.woff2") format("woff2"),url("/assets/font/notosanskr/NotoSans-Bold.woff") format("woff");
}


@font-face {
    font-family:'Inter';
    font-weight:300;
    font-style:normal;
    src:url("/assets/font/inter/Inter-Light.eot");
    src:url("/assets/font/inter/Inter-Light.eot?#iefix") format("embedded-opentype"),url("/assets/font/inter/Inter-Light.woff2") format("woff2"),url("/assets/font/inter/Inter-Light.woff") format("woff");
}
@font-face {
    font-family:'Inter';
    font-weight:400;
    font-style:normal;
    src:url("/assets/font/inter/Inter-Regular.eot");
    src:url("/assets/font/inter/Inter-Regular.eot?#iefix") format("embedded-opentype"),url("/assets/font/inter/Inter-Regular.woff2") format("woff2"),url("/assets/font/inter/Inter-Regular.woff") format("woff");
}
@font-face {
    font-family:'Inter';
    font-weight:500;
    font-style:normal;
    src:url("/assets/font/inter/Inter-Medium.eot");
    src:url("/assets/font/inter/Inter-Medium.eot?#iefix") format("embedded-opentype"),url("/assets/font/inter/Inter-Medium.woff2") format("woff2"),url("/assets/font/inter/Inter-Medium.woff") format("woff");
}
@font-face {
    font-family:'Inter';
    font-weight:700;
    font-style:normal;
    src:url("/assets/font/inter/Inter-Bold.eot");
    src:url("/assets/font/inter/Inter-Bold.eot?#iefix") format("embedded-opentype"),url("/assets/font/inter/Inter-Bold.woff2") format("woff2"),url("/assets/font/inter/Inter-Bold.woff") format("woff");
}
body {
    font-family: Inter, NotoSansKR, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Arial, sans-serif;
    overflow-x: hidden;
    min-width: 320px;
    line-height: 1.7;
    letter-spacing: -0.05em;
    color: #fff;
    background-color: #000;
}
html {
    box-sizing: border-box;
    line-height: 1.15;
    -webkit-text-size-adjust: 100%;
}
a {
    color: inherit;
    background-color: transparent;
    outline: 0;
    text-decoration: none;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0) !important;
}
ol,
ul,
li {
    list-style: none;
}
.layout-default {
    height: 100%;
}
.layout-default #header {
    position: fixed;
    width: 100vw;
    z-index: 102;
}
.layout-default #header .header-inner {
    padding: 40px 60px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
    -ms-flex-flow: row nowrap;
    flex-flow: row nowrap;
    position: relative;
    z-index: 9
}
.layout-default #header .header-inner #nav {
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: start;
    -ms-flex-pack: start;
    justify-content: flex-start;
    -webkit-box-align: start;
    -ms-flex-align: start;
    align-items: flex-start
}
.layout-default #header .header-inner #nav ul li {
    padding: 2px 0
}
.layout-default #header .header-inner #nav ul li a {
    font-size: 20px;
    font-weight: 700;
    color: #acacac;
    pointer-events: auto
}
.layout-default #header .header-inner .logo {
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-box-align: start;
    -ms-flex-align: start;
    align-items: flex-start
}
.layout-default #header .header-inner .logo .holder {
    width: 156px;
    height: 64px;
    background: url(/assets/img/common/logo-white.svg);
    background-repeat: no-repeat;
    background-position: center;
    background-size: contain;
    pointer-events: auto
}
.layout-default #header .header-inner .txt-header {
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-pack: start;
    -ms-flex-pack: start;
    justify-content: flex-start;
    -webkit-box-align: end;
    -ms-flex-align: end;
    align-items: flex-end
}
.layout-default #header .header-inner .txt-header .holder {
    font-family: Inter;
    letter-spacing: normal;
    font-size: 14px;
    font-weight: 700;
    color: #a1a1a1
}
.layout-default #footer {
    position: fixed;
    bottom: 0;
    width: 100vw;
    z-index: 100;
}
.layout-default #footer .footer-inner {
    padding: 40px 50px 40px 60px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
    -ms-flex-flow: row nowrap;
    flex-flow: row nowrap
}
.layout-default #footer .footer-inner .description {
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-align: start;
    -ms-flex-align: start;
    align-items: flex-start;
    padding-top: 5px;
    font-size: 14px
}
.layout-default #footer .footer-inner .description .notice {
    font-weight: 700;
    pointer-events: auto;
    color: #fff
}
.layout-default #footer .footer-inner .description .company {
    margin-top: 22px;
    font-weight: 500;
    color: #a1a1a1;
    pointer-events: auto
}
.layout-default #footer .footer-inner .description .slogan {
    margin-top: 12px;
    font-weight: 500;
    color: #aaa;
    pointer-events: auto
}
.layout-default #footer .footer-inner .content {
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-align: end;
    -ms-flex-align: end;
    align-items: flex-end;
    -webkit-box-pack: end;
    -ms-flex-pack: end;
    justify-content: flex-end;
}
.layout-default #footer .footer-inner .content .cart .btn-type-link {
    font-weight: inherit;
    pointer-events: auto;
    -webkit-backdrop-filter: blur(0.8);
    backdrop-filter: blur(0.8)
}
.layout-default #footer .footer-inner .content .member {
    margin-bottom: 10px;
    font-size: 16px;
    font-weight: 700
}
.layout-default #footer .footer-inner .content .member .btn-type-link {
    padding: 0 10px;
    height: 32px;
    font-weight: inherit;
    pointer-events: auto;
    -webkit-backdrop-filter: blur(0.8);
    backdrop-filter: blur(0.8)
}
.layout-default #footer .footer-inner .content .term {
    font-size: 14px;
    font-weight: 500
}
.layout-default #footer .footer-inner .content .term .btn-type-link {
    padding: 0 10px;
    height: 32px;
    font-weight: inherit;
    color: #a1a1a1;
    pointer-events: auto
}
.layout-default #footer .footer-inner .content .term .btn-type-link:first-of-type::after {
    content: "";
    display: block;
    position: absolute;
    right: -2px;
    top: calc(50% + 1px);
    -webkit-transform: translateY(-50%);
    transform: translateY(-50%);
    width: 1px;
    height: 13px;
    background-color: #999;
}
.layout-default #footer .footer-inner .content .sns {
    margin-top: 5px;
    font-size: 14px;
    font-weight: 700
}
.layout-default #footer .footer-inner .content .sns .btn-type-link {
    padding: 0 10px;
    height: 32px;
    font-weight: inherit;
    letter-spacing: 0;
    pointer-events: auto
}
.layout-default #footer .footer-inner .content .copyright {
    margin-top:4px;
    padding-right: 10px;
    font-size: 13px;
    font-weight: 700;
    letter-spacing: 0;
    pointer-events: auto
}
.layout-default #footer .footer-inner .content .copyright .xi {
    vertical-align: middle;
    font-size: 16px;
}
.layout-default #footer .footer-inner .content .browser {
    padding-right: 10px;
    font-size: 13px;
    font-weight: 500;
    letter-spacing: -0.05em;
    color: #fff
}
.layout-default #footer .footer-inner.footer-mobile {
    display: none;
}
.layout-default .container {
    position:absolute;
    left:50%;
    top:50%;
    transform: translate(-50%, -50%);
    text-align:center;
}
/*
.layout-default .container .img_404 {
    width: 478px;
    height:287px;
    background:url(/assets/img/common/404.svg) center center no-repeat;
}
.layout-default .container .img_404 .svg {
    width:100%;
}
*/
.layout-default .container .txt_404 {
    font-size: 240px;
    font-weight: 500;
    line-height: 1;
    letter-spacing: -0.57px;
    color: #000;
    text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff;
}
.layout-default .container .tit {
    margin-top:50px;
    font-size: 34px;
    font-weight: 700;
}
.layout-default .container .dsc {
    margin-top:30px;
    font-size: 22px;
    font-weight: 700;
}

@media screen and (max-width:960px) {
    .layout-default {
        height: auto;
    }
    .layout-default .container {
        position: relative;
        left: auto;
        top: auto;
        transform: none;
        padding: 200px 24px 100px;
    }
    .layout-default #header .header-inner #nav {
        display:none;
    }
    .layout-default #header .header-inner .txt-header {
        display: none;
    }
    .layout-default .container .txt_404 {
        font-size: 100px;
    }
    .layout-default .container .tit {
        margin-top:30px;
        font-size: 21px;
    }
    .layout-default .container .dsc {
        font-size: 14px;
    }
    .layout-default #footer {
        position: relative;
    }
    .layout-default #footer .footer-inner {
        padding:24px;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
    }
    .layout-default #footer .footer-inner .content {
        margin-top: 22px;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start
    }
    .layout-default #footer .footer-inner .content .sns {
        margin-left: -9px;
    }
}
</style>
</head>
<body class="layout-default">
    <header id="header" :class="{'theme--light': onScroll }">
        <div class="header-inner">
            <nav id="nav">
                <ul>
                    <li><a href="/about">소개</a></li>
                    <li><a href="/enroll">출품</a></li>
                    <li><a href="/winners">수상</a></li>
                    <li><a href="">아카이브</a></li>
                    <li><a href="/faq">안내</a></li>
                </ul>
            </nav>
            <div class="logo">
                <a href="/"><div class="holder"></div></a>
            </div>
            <div class="txt-header">
                <div class="holder">A.N.D. AWARD 2020</div>
            </div>
        </div>
    </header>

    <div class="container">
        <!-- <div class="img_404"></div> -->
        <div class="txt_404">404</div>
        <div class="tit">요청하신 페이지를 찾을 수 없습니다</div>
        <div class="dsc">페이지의 주소를 잘못 입력하였거나,<br>다른 페이지로 변경 또는 삭제되어 찾을 수 없습니다.</div>
    </div>

    <footer id="footer">
        <div class="footer-inner">
            <div class="description">
                <div class="notice dsc1 dsc1-1">사단법인. 한국디지털기업협회</div>
                <div class="slogan dsc1 dsc1-3">서울시 강남구 도산대로176 503호(논현동, 대광빌딩)<br>대표자 : 고경구 &nbsp; 사업자등록번호 : 201-82-05787<br>전화번호 : 02-3462-5627 &nbsp; 팩스번호 : 02-3462-5630<br>E-mail : master@dea.or.kr</div>
            </div>

            <div class="content">
                <!-- <div class="member">
                    <v-btn :to="{name: 'userLogin', query: { redirect: $router.currentRoute.path}}" depressed tile text small class="btn-type-link">로그인</v-btn>
                    <v-btn href="/join-guide" depressed tile text small class="btn-type-link">가입하기</v-btn>
                </div>
                <div class="member">
                    <v-btn href="/logout" depressed tile text small class="btn-type-link">로그아웃</v-btn>
                    <v-btn href="/mypage" depressed tile text small class="btn-type-link">마이페이지</v-btn>
                </div> -->
                <!-- <div class="term">
                    <v-btn depressed tile text x-small @click="openTerms(2)" class="btn-type-link">개인정보취급방침</v-btn>
                    <v-btn depressed tile text x-small @click="openTerms(5)" class="btn-type-link">법적고지</v-btn>
                </div> -->
                <div class="sns">
                    <a href="https://www.facebook.com/nawards" depressed tile text x-small target="_blank" class="btn-type-link">Facebook <span class="xi xi-external-link"></span></a>
                </div>
                <div class="copyright">
                    <span class="xi xi-copyright"></span> Copyright 2020 A.N.D. AWARD
                </div>
                <div class="browser">본 사이트는 크롬에 최적화되어 있습니다</div>
            </div>
        </div>

        <div class="footer-inner footer-mobile">
            <div class="description">
                <div class="notice dsc1 dsc1-1">사단법인. 한국디지털기업협회</div>
                <div class="slogan dsc1 dsc1-3">서울시 강남구 도산대로176 503호(논현동, 대광빌딩)<br>대표자 : 고경구 &nbsp; 사업자등록번호 : 201-82-05787<br>전화번호 : 02-3462-5627 &nbsp; 팩스번호 : 02-3462-5630<br>E-mail : master@dea.or.kr</div>
            </div>

            <div class="content">
                <!-- <div class="term">
                    <v-btn depressed tile text x-small @click="openTerms(2)" class="btn-type-link">개인정보취급방침</v-btn>
                    <v-btn depressed tile text x-small @click="openTerms(5)" class="btn-type-link">법적고지</v-btn>
                </div> -->
                <div class="sns">
                    <a href="https://www.facebook.com/nawards" depressed tile text x-small target="_blank" class="btn-type-link">Facebook <span class="xi xi-external-link"></span></a>
                </div>
                <div class="copyright">
                    <span class="xi xi-copyright"></span>Copyright 2020 A.N.D. AWARD
                </div>
                <div class="browser">본 사이트는 크롬에 최적화되어 있습니다</div>
            </div>
        </div>
    </footer>
</body>
</html>