<table border='0' cellpadding='0' cellspacing='0' align='center' width='100%' style='border-collapse:collapse;'>
    <tr>
        <td style='margin:0;padding:0;'>
            <table border='0' cellpadding='0' cellspacing='0' align='center' width='600' style='border-collapse:collapse;'>
                <tr>
                    <td height='60' style='margin:0;padding:0;font-size:0;line-height:0;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;'><img src='https://naward.or.kr/assets/img/external/logo_mail@2x.png' alt='앤어워드' width='93' height='38' style='display:block;'></td>
                </tr>
                <tr>
                    <td height='40' style='margin:0;padding:0;font-size:0;line-height:0;height:40px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#000000;font-size:34px;line-height:44px;font-weight:700;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'><b>출품결제가<br>완료 되었습니다</b></td>
                </tr>
                <tr>
                    <td height='22' style='margin:0;padding:0;font-size:0;line-height:0;height:22px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#333333;font-size:16px;font-weight:500;line-height:20px;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'>안녕하세요 <b></b>{{담당자명}}</b>님,<br />회원님의 [<b>{{결제유형}}</b>] 결제가 정상처리 되었습니다</td>
                </tr>
                <tr>
                    <td height='20' style='margin:0;padding:0;font-size:0;line-height:0;height:20px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#808080;font-size:11px;font-weight:500;line-height:20px;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'>고객님께서 결제하신 내역은 다음과 같습니다</td>
                </tr>
                <tr>
                    <td height='44' style='margin:0;padding:0;font-size:0;line-height:0;height:44px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#333333;font-size:16px;font-weight:700;line-height:20px;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'><b>결제 안내</b></td>
                </tr>
                <tr>
                    <td height='8' style='margin:0;padding:0;font-size:0;line-height:0;height:8px;'></td>
                </tr>
                <tr>
                    <td height='1' bgcolor='#606060' style='margin:0;padding:0;font-size:0;line-height:0;height:1px;'></td>
                </tr>
                <tr>
                    <td height='30' style='margin:0;padding:0;font-size:0;line-height:0;height:30px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#808080;font-size:11px;line-height:14px;font-weight:500;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'>결제번호</td>
                </tr>
                <tr>
                    <td height='6' style='margin:0;padding:0;font-size:0;line-height:0;height:6px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#333333;font-size:14px;font-weight:700;line-height:20px;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'><b>{{결제번호}}</b></td>
                </tr>
                <tr>
                    <td height='28' style='margin:0;padding:0;font-size:0;line-height:0;height:28px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#808080;font-size:11px;line-height:14px;font-weight:500;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'>결제유형</td>
                </tr>
                <tr>
                    <td height='6' style='margin:0;padding:0;font-size:0;line-height:0;height:6px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#333333;font-size:14px;font-weight:700;line-height:20px;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'><b>{{결제유형}}</b></td>
                </tr>
                <tr>
                    <td height='28' style='margin:0;padding:0;font-size:0;line-height:0;height:28px;'></td>
                </tr>
                <tr>
                    <td height='60' style='margin:0;padding:0;font-size:0;line-height:0;height:60px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#333333;font-size:16px;font-weight:700;line-height:20px;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'><b>입금정보 안내</b></td>
                </tr>
                <tr>
                    <td height='8' style='margin:0;padding:0;font-size:0;line-height:0;height:8px;'></td>
                </tr>
                <tr>
                    <td height='1' bgcolor='#606060' style='margin:0;padding:0;font-size:0;line-height:0;height:1px;'></td>
                </tr>
                <tr>
                    <td height='30' style='margin:0;padding:0;font-size:0;line-height:0;height:30px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#808080;font-size:11px;line-height:14px;font-weight:500;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'>입금 계좌번호</td>
                </tr>
                <tr>
                    <td height='6' style='margin:0;padding:0;font-size:0;line-height:0;height:6px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#333333;font-size:14px;font-weight:700;line-height:20px;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'><b>기업은행 274-087499-01-014</b></td>
                </tr>
                <tr>
                    <td height='28' style='margin:0;padding:0;font-size:0;line-height:0;height:28px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#808080;font-size:11px;line-height:14px;font-weight:500;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'>예금주</td>
                </tr>
                <tr>
                    <td height='6' style='margin:0;padding:0;font-size:0;line-height:0;height:6px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#333333;font-size:14px;font-weight:700;line-height:20px;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'><b>(사)한국디지털기업협회</b></td>
                </tr>
                <tr>
                    <td height='28' style='margin:0;padding:0;font-size:0;line-height:0;height:28px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#808080;font-size:11px;line-height:14px;font-weight:500;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'>입금자명</td>
                </tr>
                <tr>
                    <td height='6' style='margin:0;padding:0;font-size:0;line-height:0;height:6px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#333333;font-size:14px;font-weight:700;line-height:20px;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'><b>{{입금자명}}</b></td>
                </tr>
                <tr>
                    <td height='28' style='margin:0;padding:0;font-size:0;line-height:0;height:28px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#808080;font-size:11px;line-height:14px;font-weight:500;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'>입금마감날짜</td>
                </tr>
                <tr>
                    <td height='6' style='margin:0;padding:0;font-size:0;line-height:0;height:6px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#333333;font-size:14px;font-weight:700;line-height:20px;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'><b>2020-11-27</b></td>
                </tr>
                <tr>
                    <td height='60' style='margin:0;padding:0;font-size:0;line-height:0;height:60px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#333333;font-size:16px;font-weight:700;line-height:20px;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'><b>결제내역 안내</b></td>
                </tr>
                <tr>
                    <td height='8' style='margin:0;padding:0;font-size:0;line-height:0;height:8px;'></td>
                </tr>
                <tr>
                    <td height='1' bgcolor='#606060' style='margin:0;padding:0;font-size:0;line-height:0;height:1px;'></td>
                </tr>
                <tr>
                    <td height='30' style='margin:0;padding:0;font-size:0;line-height:0;height:30px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#808080;font-size:11px;font-weight:500;line-height:20px;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'>※ 출품작이 7개 이상일 시 8번째 출품작부터 추가등록비가 면제 됩니다</td>
                </tr>
                <tr>
                    <td height='30' style='margin:0;padding:0;font-size:0;line-height:0;height:30px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;'>
                        {{결제리스트}}
                    </td>
                </tr>
                <tr>
                    <td height='90' style='margin:0;padding:0;font-size:0;line-height:0;height:90px;'></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height='1' bgcolor='#eaeaea' style='margin:0;padding:0;font-size:0;line-height:0;height:1px;'></td>
    </tr>
    <tr>
        <td bgcolor='#f6f6f6' style='margin:0;padding:0;'>
            <table border='0' cellpadding='0' cellspacing='0' align='center' width='600' style='border-collapse:collapse;'>
                <tr>
                    <td bgcolor='#f6f6f6' height='35' style='margin:0;padding:0;font-size:0;line-height:0;height:35px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#808080;font-size:10px;line-height:14px;font-weight:600;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'>본 메일은 발신전용 메일로 회신이 되지 않습니다<br />문의사항은 홈페이지 내 FAQ를 이용해주시거나 <a href='mailto:master@dea.or.kr' style='color:#808080;text-decoration:none;'><font color='#808080'>master@dea.or.kr</font></a> 또는 02-3462-5627로 문의 바랍니다</td>
                </tr>
                <tr>
                    <td bgcolor='#f6f6f6' height='20' style='margin:0;padding:0;font-size:0;line-height:0;height:20px;'></td>
                </tr>
                <tr>
                    <td bgcolor='#dddddd' height='1' style='margin:0;padding:0;font-size:0;line-height:0;height:1px;'></td>
                </tr>
                <tr>
                    <td bgcolor='#f6f6f6' height='20' style='margin:0;padding:0;font-size:0;line-height:0;height:20px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#444444;font-size:9px;line-height:14px;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'><b>(사)한국디지털기업협회</b></td>
                </tr>
                <tr>
                    <td bgcolor='#f6f6f6' height='4' style='margin:0;padding:0;font-size:0;line-height:0;height:4px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#444444;font-size:9px;line-height:14px;font-weight:500;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'>서울시 강남구 도산대로176 503호(논현동, 대광빌딩)<br />ⓒ A.N.D. AWARD ALL RIGHTS RESERVED.</td>
                </tr>
                <tr>
                    <td bgcolor='#f6f6f6' height='13' style='margin:0;padding:0;font-size:0;line-height:0;height:13px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;'>
                        <table border='0' cellpadding='0' cellspacing='0' align='center' width='600' style='border-collapse:collapse;'>
                            <tr>
                                <td style='margin:0;padding:0;padding-right:6px;'><a href='https://www.facebook.com/nawards' target='_blank' style='color:#444444;font-size:9px;line-height:14px;font-weight:500;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;text-decoration:none;'><font color='#444444'>Facebook</font></a><span style='margin-left:5px;margin-right:5px;font-size:9px;'>|</span><a href='https://naward.or.kr/main?terms=2' target='_blank' style='color:#444444;font-size:9px;line-height:14px;font-weight:500;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;text-decoration:none;'><font color='#444444'>개인정보취급방침</font></a><span style='margin-left:5px;margin-right:5px;font-size:9px;'>|</span><a href='https://naward.or.kr/main?terms=5' target='_blank' style='color:#444444;font-size:9px;line-height:14px;font-weight:500;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;text-decoration:none;'><font color='#444444'>법적고지</font></a></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td bgcolor='#f6f6f6' height='40' style='margin:0;padding:0;font-size:0;line-height:0;height:40px;'></td>
                </tr>
            </table>
        </td>
    </tr>
</table>