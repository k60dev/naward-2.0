<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>A.N.D. Award</title>
<link href="https://cdn.jsdelivr.net/npm/xeicon@2.3.3/xeicon.min.css" rel="stylesheet">
<style type="text/css">
*,
*:before,
*:after {
    margin: 0;
    padding: 0;
    box-sizing: inherit;
}
@font-face {
    font-family:'NotoSansKR';
    font-weight:300;
    font-style:normal;
    src:url("/assets/font/notosanskr/NotoSans-Light.eot");
    src:url("/assets/font/notosanskr/NotoSans-Light.eot?#iefix") format("embedded-opentype"),url("/assets/font/notosanskr/NotoSans-Light.woff2") format("woff2"),url("/assets/font/notosanskr/NotoSans-Light.woff") format("woff");
}
@font-face {
    font-family:'NotoSansKR';
    font-weight:400;
    font-style:normal;
    src:url("/assets/font/notosanskr/NotoSans-Regular.eot");
    src:url("/assets/font/notosanskr/NotoSans-Regular.eot?#iefix") format("embedded-opentype"),url("/assets/font/notosanskr/NotoSans-Regular.woff2") format("woff2"),url("/assets/font/notosanskr/NotoSans-Regular.woff") format("woff");
}
@font-face {
    font-family:'NotoSansKR';
    font-weight:500;
    font-style:normal;
    src:url("/assets/font/notosanskr/NotoSans-Medium.eot");
    src:url("/assets/font/notosanskr/NotoSans-Medium.eot?#iefix") format("embedded-opentype"),url("/assets/font/notosanskr/NotoSans-Medium.woff2") format("woff2"),url("/assets/font/notosanskr/NotoSans-Medium.woff") format("woff");
}
@font-face {
    font-family:'NotoSansKR';
    font-weight:700;
    font-style:normal;
    src:url("/assets/font/notosanskr/NotoSans-Bold.eot");
    src:url("/assets/font/notosanskr/NotoSans-Bold.eot?#iefix") format("embedded-opentype"),url("/assets/font/notosanskr/NotoSans-Bold.woff2") format("woff2"),url("/assets/font/notosanskr/NotoSans-Bold.woff") format("woff");
}


@font-face {
    font-family:'Inter';
    font-weight:300;
    font-style:normal;
    src:url("/assets/font/inter/Inter-Light.eot");
    src:url("/assets/font/inter/Inter-Light.eot?#iefix") format("embedded-opentype"),url("/assets/font/inter/Inter-Light.woff2") format("woff2"),url("/assets/font/inter/Inter-Light.woff") format("woff");
}
@font-face {
    font-family:'Inter';
    font-weight:400;
    font-style:normal;
    src:url("/assets/font/inter/Inter-Regular.eot");
    src:url("/assets/font/inter/Inter-Regular.eot?#iefix") format("embedded-opentype"),url("/assets/font/inter/Inter-Regular.woff2") format("woff2"),url("/assets/font/inter/Inter-Regular.woff") format("woff");
}
@font-face {
    font-family:'Inter';
    font-weight:500;
    font-style:normal;
    src:url("/assets/font/inter/Inter-Medium.eot");
    src:url("/assets/font/inter/Inter-Medium.eot?#iefix") format("embedded-opentype"),url("/assets/font/inter/Inter-Medium.woff2") format("woff2"),url("/assets/font/inter/Inter-Medium.woff") format("woff");
}
@font-face {
    font-family:'Inter';
    font-weight:700;
    font-style:normal;
    src:url("/assets/font/inter/Inter-Bold.eot");
    src:url("/assets/font/inter/Inter-Bold.eot?#iefix") format("embedded-opentype"),url("/assets/font/inter/Inter-Bold.woff2") format("woff2"),url("/assets/font/inter/Inter-Bold.woff") format("woff");
}
body {
    font-family: Inter, NotoSansKR, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Arial, sans-serif;
    overflow-x: hidden;
    min-width: 320px;
    line-height: 1.7;
    letter-spacing: -0.05em;
    color: #fff;
    background-color: #000;
}
html {
    box-sizing: border-box;
    line-height: 1.15;
    -webkit-text-size-adjust: 100%;
    height:100%;
}
a {
    color: inherit;
    background-color: transparent;
    outline: 0;
    text-decoration: none;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0) !important;
}
ol,
ul,
li {
    list-style: none;
}
.layout-default {
    height: 100%;
    background-color: #000;
    background-image: url(/assets/img/common/bg_browser.png);
    background-repeat: no-repeat;
    background-position: center;
    background-size: 538px 642px;
    pointer-events: auto
}
#container {
    position:absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    font-size: 60px;
    font-weight: 700;
    line-height: 1.4;
    letter-spacing: -0.05em;
}
.btn {
    margin-top: 40px;
    display: block;
    width: 260px;
    height: 74px;
    line-height: 72px;
    padding: 0 10px;
    border: 2px solid #fff;
    font-size: 26px;
    text-align: center;
}
.btn .xi {
    margin-left: 30px;
}
</style>
</head>
<body class="layout-default">
    <div id="container">
        <div>앤어워드 사이트는<br>크롬에 최적화 되어 있습니다</div>
        <a href="https://www.google.com/chrome/" target="_blank" class="btn">크롬 설치하기 <span class="xi xi-angle-right"></span></a>
    </div>
</body>
</html>