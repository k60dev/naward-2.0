<table border='0' cellpadding='0' cellspacing='0' align='center' width='100%' style='border-collapse:collapse;'>
    <tr>
        <td style='margin:0;padding:0;'>
            <table border='0' cellpadding='0' cellspacing='0' align='center' width='600' style='border-collapse:collapse;'>
                <tr>
                    <td height='60' style='margin:0;padding:0;font-size:0;line-height:0;height:60px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;'><img src='https://naward.or.kr/assets/img/external/logo_mail@2x.png' alt='앤어워드' width='93' height='38' style='display:block;'></td>
                </tr>
                <tr>
                    <td height='40' style='margin:0;padding:0;font-size:0;line-height:0;height:40px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#000000;font-size:34px;line-height:44px;font-weight:700;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'><b>앤어워드에서<br />알려드립니다</b></td>
                </tr>
                <tr>
                    <td height='45' style='margin:0;padding:0;font-size:0;line-height:0;height:45px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#333333;font-size:14px;line-height:20px;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'><?=$email_contents?></td>
                </tr>
                <tr>
                    <td height='80' style='margin:0;padding:0;font-size:0;line-height:0;height:80px;'></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height='1' bgcolor='#eaeaea' style='margin:0;padding:0;font-size:0;line-height:0;height:1px;'></td>
    </tr>
    <tr>
        <td bgcolor='#f6f6f6' style='margin:0;padding:0;'>
            <table border='0' cellpadding='0' cellspacing='0' align='center' width='600' style='border-collapse:collapse;'>
                <tr>
                    <td height='35' bgcolor='#f6f6f6' style='margin:0;padding:0;font-size:0;line-height:0;height:35px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#808080;font-size:10px;line-height:14px;font-weight:600;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'>본 메일은 발신전용 메일로 회신이 되지 않습니다<br />문의사항은 홈페이지 내 FAQ를 이용해주시거나 <a href='mailto:master@dea.or.kr' style='color:#808080;text-decoration:none;'><font color='#808080'>master@dea.or.kr</font></a> 또는 02-3462-5627로 문의 바랍니다</td>
                </tr>
                <tr>
                    <td height='20' bgcolor='#f6f6f6' style='margin:0;padding:0;font-size:0;line-height:0;height:20px;'></td>
                </tr>
                <tr>
                    <td height='1' bgcolor='#dddddd' style='margin:0;padding:0;font-size:0;line-height:0;height:1px;'></td>
                </tr>
                <tr>
                    <td height='20' bgcolor='#f6f6f6' style='margin:0;padding:0;font-size:0;line-height:0;height:20px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#444444;font-size:9px;line-height:14px;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'><b>(사)한국디지털기업협회</b></td>
                </tr>
                <tr>
                    <td height='4' bgcolor='#f6f6f6' style='margin:0;padding:0;font-size:0;line-height:0;height:4px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;color:#444444;font-size:9px;line-height:14px;font-weight:500;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;'>서울시 강남구 도산대로176 503호(논현동, 대광빌딩)<br />ⓒ A.N.D. AWARD ALL RIGHTS RESERVED.</td>
                </tr>
                <tr>
                    <td height='13' bgcolor='#f6f6f6' style='margin:0;padding:0;font-size:0;line-height:0;height:13px;'></td>
                </tr>
                <tr>
                    <td style='margin:0;padding:0;'>
                        <table border='0' cellpadding='0' cellspacing='0' align='center' width='600' style='border-collapse:collapse;'>
                            <tr>
                                <td style='margin:0;padding:0;padding-right:6px;'><a href='https://www.facebook.com/nawards' target='_blank' style='color:#444444;font-size:9px;line-height:14px;font-weight:500;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;text-decoration:none;'><font color='#444444'>Facebook</font></a><span style='margin-left:5px;margin-right:5px;font-size:9px;'>|</span><a href='https://naward.or.kr/main?terms=2' target='_blank' style='color:#444444;font-size:9px;line-height:14px;font-weight:500;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;text-decoration:none;'><font color='#444444'>개인정보취급방침</font></a><span style='margin-left:5px;margin-right:5px;font-size:9px;'>|</span><a href='https://naward.or.kr/main?terms=5' target='_blank' style='color:#444444;font-size:9px;line-height:14px;font-weight:500;font-family:-apple-system,BlinkMacSystemFont,Dotum,Helvetica,Roboto,Verdana,Arial,sans-serif;letter-spacing:-0.05em;text-decoration:none;'><font color='#444444'>법적고지</font></a></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height='40' bgcolor='#f6f6f6' style='margin:0;padding:0;font-size:0;line-height:0;height:40px;'></td>
                </tr>
            </table>
        </td>
    </tr>
</table>