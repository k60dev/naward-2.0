<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['api/admin/(:any)'] = function ($class) {
    $class  = $class ? strtolower($class) : exit;
    $path   = "apiadmin/".$class."_main";
    return $path;
};

$route['api/admin/(:any)/(:any)'] = function ($class, $method) {
    $class  = $class ? strtolower($class) : exit;
    $method = $method ? strtolower($method) : "main";
    $path   = "apiadmin/".$class."_".$method;
    return $path;
};

$route['api/admin/(:any)/(:any)/(:any)'] = function ($class, $method, $key=false) {
    $class  = $class ? strtolower($class) : exit;
    $method = $method ? strtolower($method) : "main";
    $key = $key ? strtolower($key) : "";
    $path   = "apiadmin/".$class."_".$method.($key?"/{$key}":"");
    return $path;
};

$route['api/admin/(:any)/(:any)/(:any)/(:any)'] = function ($class, $method, $key1, $key2) {
    $class  = $class ? strtolower($class) : exit;
    $method = $method ? strtolower($method) : "main";
    $path   = "apiadmin/".$class."_".$method."/".$key1."/".$key2;
    return $path;
};

$route['admin'] = 'dashboard/dashboard';
$route['admin/dashboard'] = 'dashboard/dashboard';
$route['admin/config/terms'] = 'terms/main';//약관
$route['admin/preview/(:any)'] = 'service/preview/$1';

$route['admin/config/notification'] = 'notification/main';// 알람설정
$route['admin/config/notification/(:any)'] = 'notification/view/$1';//특정약관만 가져오기
$route['admin/user'] = 'user/main'; //회원정보
    $route['admin/user/rest'] = 'user/main/Y';
    $route['admin/user/active'] = 'user/main/N';
    $route['admin/user/create/(:any)'] = 'user/view';//신규회원정보
    $route['admin/user/view/(:any)'] = 'user/view/$1';

// $route['admin/payments'] = 'payment/main';
// $route['admin/payments/(:any)'] = 'payment/main';
$route['admin/payment'] = 'payment/main';
$route['admin/payment/(:any)'] = 'payment/main';
$route['admin/payment/view/(:any)'] = 'payment/main';

$route['admin/cart'] = 'award/list';

// $route['admin/administrators'] = 'admin/main';
$route['admin/administrator'] = 'admin/main';
$route['admin/administrator/(:any)'] = 'admin/main';
$route['admin/administrator/view/(:any)'] = 'admin/main';
$route['admin/administrator/logs'] = 'admin/main';

$route['admin/award'] = 'award/list';
$route['admin/award/(:any)'] = 'award/list';
$route['admin/award/(:any)/main'] = 'award/list';
$route['admin/award/(:any)/common'] = 'award/list';
$route['admin/award/(:any)/common/base'] = 'award/list';
$route['admin/award/(:any)/common/judges'] = 'award/list';
$route['admin/award/(:any)/common/judgesparty'] = 'award/list';
$route['admin/award/(:any)/common/popup'] = 'award/list';
$route['admin/award/(:any)/common/descript'] = 'award/list';
$route['admin/award/(:any)/detail'] = 'award/list';
$route['admin/award/(:any)/detail/categories'] = 'award/list';
$route['admin/award/(:any)/detail/prizes'] = 'award/list';
$route['admin/award/(:any)/detail/result'] = 'award/list';
$route['admin/award/(:any)/detail/result/(:any)'] = 'award/list';
$route['admin/award/(:any)/detail/result/view/(:any)'] = 'award/list';
$route['admin/award/(:any)/followup'] = 'award/list';
$route['admin/award/(:any)/followup/winner'] = 'award/list';
$route['admin/award/(:any)/followup/winner/(:any)'] = 'award/list';
$route['admin/award/(:any)/followup/winner/view/(:any)'] = 'award/list';
$route['admin/award/(:any)/followup/judgement'] = 'award/list';
$route['admin/award/(:any)/followup/gallery'] = 'award/list';
$route['admin/award/(:any)/followup/speech'] = 'award/list';
$route['admin/award/(:any)/followup/speech/create'] = 'award/list';

$route['api/admin_award'] = 'apiaward/list';
$route['api/admin_award/(:num)'] = 'apiaward/view/$1';
//$route['api/admin/award/(:any)/judge'] = 'apiaward/judge';
$route['api/admin_award/(:any)'] = function ($class) {
    $class  = $class ? strtolower($class) : exit;
    $path   = "apiaward/".$class;
    return $path;
};
$route['api/admin_award/(:any)/(:any)'] = function ($a_id, $class) {
    $class  = $class ? strtolower($class) : exit;
    $path   = "apiaward/".$class;
    return $path;
};

//$route['api/admin/award/(:any)/view_category/(:any)'] = 'apiaward/view_category/$2';
$route['api/admin_award/(:any)/(:any)/(:any)'] = function ($a_id, $class, $key) {
    $class  = $class ? strtolower($class) : exit;
    $path   = "apiaward/{$class}/{$key}";
    return $path;
};



/* 사용자 서비스 관련 라우터 */
$route['api/(:any)/(:any)'] = function ($class, $method) {
    $class  = $class ? strtolower($class) : exit;
    $method = $method ? strtolower($method) : "get";
    $path   = "api/".$class."_".$method;
    return $path;
};

$route['api/(:any)/(:any)/(:any)'] = function ($class, $method, $key=null) {
    $class  = $class ? strtolower($class) : exit;
    $method = $method ? strtolower($method) : "get";
    $path   = "api/".$class."_".$method."/".$key;
    return $path;
};

$route['api/(:any)/(:any)/(:any)/(:any)'] = function ($class, $method, $key1, $key2) {
    $class  = $class ? strtolower($class) : exit;
    $method = $method ? strtolower($method) : "get";
    $path   = "api/".$class."_".$method."/".$key1."/".$key2;
    return $path;
};

$route['api/(:any)/(:any)/(:any)/(:any)/(:any)'] = function ($class, $method, $key1, $key2, $key3) {
    $class  = $class ? strtolower($class) : exit;
    $method = $method ? strtolower($method) : "get";
    $path   = "api/".$class."_".$method."/".$key1."/".$key2."/".$key3;
    return $path;
};


$route['browser'] = 'service/browser';

$route['main'] = 'service/main';
$route['terms_of_service'] = 'service/terms/1';
$route['privacy_statement'] = 'service/terms/2';
$route['faq'] = 'service/faq';

// $route['andaward'] = 'service/info_award';
// $route['andaward/(:any)'] = 'service/info_award';
$route['judges/(:any)'] = 'service/judges';
$route['category/(:any)'] = 'service/category';
$route['prizes/(:any)'] = 'service/info_prize';
$route['receiv_prize/(:any)'] = 'service/receiv_prize';
$route['receiv_prize/(:any)/(:any)'] = 'service/receiv_prize_detail/$2';

$route['judgement/(:any)/(:any)'] = 'service/judgement/$2';
$route['give_prize/(:any)'] = 'service/give_prize';

$route['archive/(:any)'] = 'service/archive';
$route['archive/(:any)/(:any)'] = 'service/archive/$2';

$route['winners'] = 'service/winners';
$route['winners/(:any)'] = 'service/winners';
$route['winners/(:any)/(:any)'] = 'service/winner/$2';

$route['archive/winners/(:any)'] = 'service/winners';
$route['archive/winners/(:any)/(:any)'] = 'service/winner/$2';
$route['archive/aboard/(:any)'] = 'aboard/list/$1';

$route['aboard/(:any)'] = 'aboard/list/$1';
$route['award_gallery/(:any)'] = 'aboard/gallery_usr/$1';
$route['award_speech/(:any)'] = 'aboard/speech_usr/$1';
$route['award_gallery/(:any)/(:any)'] = 'aboard/gallery_usr/$1/$2';
$route['award_speech/(:any)/(:any)'] = 'aboard/speech_usr/$1/$2';

$route['about'] = 'service/about';
    $route['about/judge'] = 'service/about/judge';
    $route['about/sponsor'] = 'service/about/sponsor';

$route['join'] = 'service/join';
$route['join-guide'] = 'service/join';
$route['olduser'] = 'service/get_olduser_info';
$route['login'] = 'auth/login';
$route['logout'] = 'auth/logout';
$route['find_userinfo'] = 'service/find_userinfo';
$route['change_password'] = 'service/change_password';
$route['scheduler'] = 'service/scheduler';

$route['mypage'] = 'mypage';
    $route['mypage/recipient'] = 'mypage/recipient_history';
    $route['mypage/payment'] = 'mypage/payment_history';
    $route['mypage/payment_detail/(:any)'] = 'mypage/payment_history';
    $route['mypage/prize'] = 'mypage/prize_history';
    $route['mypage/profile'] = 'mypage/profile';

$route['enroll'] = 'enroll/main';
    $route['enroll/category'] = 'enroll/category';
    $route['enroll/work'] = 'enroll/work';
    $route['enroll/cart'] = 'enroll/cart';
    $route['enroll/payment'] = 'enroll/payment';
    $route['enroll/process'] = 'enroll/process';
    $route['enroll/receipt'] = 'enroll/receipt';
    $route['enroll/receipt/(:any)'] = 'enroll/receipt/$1';
    $route['enroll/result'] = 'enroll/result';


/* 라이브컨텐츠 관련 라우터 */
$route['live/(:any)'] = "";
$route['api/get_current_award'] = "api/get_current_award";
$route['api/get_live_contents/(:any)/(:any)/(:any)'] = "api/get_live_contents/$1/$2/$3";
$route['api/upload_live_contents/(:any)/(:any)/(:any)'] = "api/upload_live_contents/$1/$2/$3";
$route['api/update_live_contents/(:any)/(:any)/(:any)'] = "api/update_live_contents/$1/$2/$3";
$route['api/find_user_id'] = 'api/service_find_user_id';
$route['api/find_user_email'] = 'api/service_find_user_email';
$route['api/change_password'] = 'api/service_find_user_id';
$route['api/get_archive_year'] = "api/get_archive_year";
$route['api/get_archive_year_array'] = "api/get_archive_year_array";


/* 개발관련 라우터 */
$route['migrate'] = 'migrate';
$route['(:any)/old_user_info'] = 'service/get_old_user';
$route['sendsms/(:any)/(:any)'] = 'notification/send_notification/sms/$1/$2';
$route['sendmail/(:any)/(:any)'] = 'notification/send_notification/mail/$1/$2';
$route['sendbill/(:any)'] = 'notification/send_bill/$1';
$route['upload/get_token'] = 'upload/get_token';
$route['upload'] = 'upload';
$route['upload/thumbnail'] = 'upload/make_thumbnail';
$route['upload/legacy_files'] = 'upload/huge_upload';
$route['storage_upload'] = 'upload/storage_upload';
$route['make_thumbnail'] = 'api/make_thumbnail';
$route['storage_file_delete'] = 'upload/storage_file_delete';
$route['storage_exist/(:any)'] = 'upload/storage_exist/$1';
$route['storage_create/(:any)'] = 'upload/storage_create/$1';
$route['mail_auth'] = "service/mail_auth";

$route['testimg'] = "service/testimg";


$route['default_controller'] = 'service';
$route['(:any)'] = 'service/award_redirect';
$route['(:any)/(:any)'] = 'service/info_award';
