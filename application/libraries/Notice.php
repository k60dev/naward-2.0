<?php
class Notice
{
    public $sender_address = "master@dea.or.kr";
    public $sender_number = "0234625627";
        
    public $config_sms_appkey = "XhT1N5cCVpUVJbMZ";
    public $config_sms_domain = "https://api-sms.cloud.toast.com";
    public $config_mail_appkey = "klY8qyWagIbJJZjo";
    public $config_mail_domain = "https://api-mail.cloud.toast.com";
    
    public $config_bill_appkey = "jL8NTUsgmM7fCfI4";
    public $config_bill_crtkey = "RJN68xobVLCBiVvJOdNmtmzaZJ209Wxd";
    public $config_bill_encrkey = "0pdS83EB9xqPZBVvX784H4QuBTmV3mYh";
    public $config_bill_domain = "https://api-bill.toast.com";
    
    public function __construct()
    {
        $CI =& get_instance();
        $CI->load->model('user_model');
        $CI->load->model('admin_model');
        $CI->load->model('payment_model');
        $CI->load->helper('common');
    }

    public function result_refactoring($result)
    {
        $this->return_status = $result['status']?$result['status']:$this->return_status;
        $this->return_data = $result['data']?$result['data']:$this->return_data;
        $this->return_msg = $result['msg']?$result['msg']:$this->return_msg;
        return return_refactoring($this->return_status, $this->return_data, $this->return_msg, $this->access_code);
    }



    public function replace_template($replace_a, $replace_b, $contents)
    {
        return str_replace($replace_a, $replace_b, $contents);
    }

    public function replace_result($u_no, $contents, $mode, $info_key=null)
    {
        $paytype = ["credit"=>"신용카드","deposit"=>"무통장입금"];
        $bankarray = [
            "01"=>"한국은행","02"=>"산업은행","03"=>"기업은행","04"=>"국민은행","05"=>"외환은행","06"=>"주택은행","07"=>"수협은행","08"=>"수출입","09"=>"장기신용","10"=>"신농협중앙",
            "11"=>"농협중앙","12"=>"농협회원","13"=>"농협회원","14"=>"농협회원","15"=>"농협회원","16"=>"축협중앙","20"=>"우리은행","21"=>"조흥은행","22"=>"상업은행","23"=>"제일은행",
            "24"=>"한일은행","25"=>"서울은행","26"=>"신한은행","27"=>"한미/시티은행","28"=>"동화은행","29"=>"동남은행","30"=>"대동은행","31"=>"대구은행","32"=>"부산은행","33"=>"충청은행","34"=>"광주은행","35"=>"제주은행","36"=>"경기은행","37"=>"전북은행","38"=>"강원은행","39"=>"경남은행","40"=>"충북은행","45"=>"새마을금고","53"=>"씨티은행","71"=>"우체국","76"=>"신용보증","81"=>"하나은행","82"=>"보람은행","83"=>"평화은행","88"=>"신한은행","89"=>"케이뱅크","90"=>"카카오뱅크"
        ];
        $cardarray = [
            "01"=>"비씨카드","02"=>"국민카드","03"=>"외환카드","04"=>"삼성카드","05"=>"신한카드","06"=>"현대카드","09"=>"롯데카드","11"=>"한미은행","12"=>"수협","14"=>"우리은행",
            "15"=>"농협","16"=>"제주은행","17"=>"광주은행","18"=>"전북은행","19"=>"조흥은행","23"=>"주택은행","24"=>"하나은행","26"=>"씨티은행","25"=>"해외카드사","99"=>"기타"
        ];
        $CI =& get_instance();
        $CI->load->model('user_model');
        $CI->load->helper('common');
        $CI->load->library('notice');
        $award_result = $CI->award_model->get_current_award();
        $user_result = $CI->user_model->get_user($u_no);
        $user_row = $user_result['data'];
        $enterprise_row = $CI->user_model->get_enterprise($user_row['e_id']);
        $mail_auth_code = create_mail_autoricate();
        $a_array = ["{{아이디}}","{{담당자명}}","{{담당자유선번호}}","{{담당자무선번호}}","{{담당자이메일}}","{{회사명}}","{{대표자명}}","{{회사대표이메일}}","{{사업자번호}}","{{메일인증코드}}"];
        $b_array = [$user_row['u_id'],$user_row['u_name'],$user_row['u_phone1'],$user_row['u_phone2'],$user_row['u_email'],$enterprise_row['e_company'],$enterprise_row['e_name'],$enterprise_row['e_taxemail'],$enterprise_row['e_number'],$mail_auth_code];
        $a_info_array = [];
        $b_info_array = [];
        switch ($mode) {
            case "us_new"://가입시
                $user_id = base64_encode($user_row['u_id']);
                $auth_code = $user_row['auth_code'];
                $str_date = strtotime($user_row['u_datetime']);
                $domain = "http://{$_SERVER['SERVER_NAME']}";
                $link = "{$domain}/mail_auth/?code={$auth_code}&stamp={$str_date}&idx={$user_id}";
                $a_info_array = ["{{인증링크}}"];
                $b_info_array = [$link];
            break;
            case "pa_finish"://결제시
                $info_query = "select * from tb_payment where p_no = '{$info_key}'";
                $result =  $CI->db->query($info_query);
                $payinfo = $result->row_array();
                $work_query = "SELECT a.w_id,a.w_title,a.c_id,a.w_date,a.w_start,a.w_end,a.p_no,a.wp_no,a.w_status,a.w_thumbnail,a.total_price,b.cp_id,b.c_title
                    FROM tb_work AS a
                    LEFT JOIN tb_category AS b ON a.c_id = b.c_id 
                    WHERE a.p_no = '{$info_key}' AND a.is_del = 'N'
                    ORDER BY a.w_id DESC";
                $work_result =  $CI->db->query($work_query);
                $wlist = [];

                foreach ($work_result->result() as $key1 => $row) {
                    $wlist[$key1] = [
                        "w_id"=>$row->w_id,
                        "w_title"=>$row->w_title,
                        "c_id"=>$row->c_id,
                        "cp_id"=>$row->cp_id,
                        "c_title"=>$row->c_title,
                        "w_date"=>$row->w_date,
                        "w_start"=>$row->w_start,
                        "w_end"=>$row->w_end,
                        "p_no"=>$row->p_no,
                        "wp_no"=>$row->wp_no,
                        "w_status"=>$row->w_status,
                        "w_thumbnail"=>$row->w_thumbnail
                    ];

                    $payment_price = $payinfo['p_totalprice'];
                    $total_price_data = $row->total_price;
                    $show_payment_price = $payment_price>0?($payment_price<$total_price_data?$payment_price:$total_price_data):0;
                    $wlist[$key1]['total_price'] = $show_payment_price;
                    $payment_price -= $show_payment_price;
                    $wlist[$key1]['price_dc'] = $show_payment_price < $total_price_data ? 'Y' : 'N';

                    $arr_c_id = $wlist[$key1]['c_id'];
                    $category_array = explode(",", $arr_c_id);
                    foreach ($category_array as $key2 => $c_id) {
                        $category_query = $CI->db->get_where('tb_category', "c_id={$c_id}");
                        $category_res = $category_query->result();
                        $wlist[$key1]['category_data'][$key2] = $category_res[0];
                    }
                    $cptitle_query = "select * from tb_category where c_id = {$wlist[$key1]['cp_id']}";
                    $cptitle_result = $CI->db->query($cptitle_query)->result();
                    $wlist[$key1]['cp_title'] = $cptitle_result[0]->c_title;
                }

                $wlist_html = "<table border='0' cellpadding='0' cellspacing='0' align='center' width='100%' style='border-collapse:collapse;'>" . PHP_EOL;
                $wlist_html .= "  <tr>" . PHP_EOL;
                $wlist_html .= "      <td style='margin:0;padding:0;'>" . PHP_EOL;
                $wlist_html .= "          <table border='0' cellpadding='0' cellspacing='0' align='center' width='100%' style='border-collapse:collapse;'>" . PHP_EOL;
                foreach ($wlist as $row) {
                    $count_cate = count($row['category_data']);
                    $cate_name =  $count_cate > 1 ? '외 '.($count_cate-1).'건' : '';
                    $wlist_html .= "              <tr>" . PHP_EOL;
                    $wlist_html .= "                  <td width='244' style='margin:0;padding:0;'><img src='".$row['w_thumbnail']."' alt='' width='220' height='132' style='display:block;'></td>" . PHP_EOL;
                    $wlist_html .= "                  <td style='margin:0;padding:0;'>" . PHP_EOL;
                    $wlist_html .= "                      <div style='margin:0;padding:0;height:48px;overflow:hidden;font-size:20px;font-weight:700;color:#333333;font-family:-apple-system,BlinkMacSystemFont,Helvetica,Roboto,Verdana,Dotum,Arial,sans-serif;letter-spacing:-0.05em;'>".$row['w_title']."</div>" . PHP_EOL;
                    $wlist_html .= "                      <div style='margin:0;padding:0;margin-top:8px;font-size:13px;font-weight:700;color:#808080;font-family:-apple-system,BlinkMacSystemFont,Helvetica,Roboto,Verdana,Dotum,Arial,sans-serif;letter-spacing:-0.05em;'>".$row['cp_title']."</div>" . PHP_EOL;
                    $wlist_html .= "                      <div style='margin:0;padding:0;margin-top:2px;font-size:13px;font-weight:700;color:#808080;font-family:-apple-system,BlinkMacSystemFont,Helvetica,Roboto,Verdana,Dotum,Arial,sans-serif;letter-spacing:-0.05em;'>".$row['category_data'][0]->c_title." ".$cate_name."</div>" . PHP_EOL;
                    $wlist_html .= "                      <div style='margin:0;padding:0;margin-top:8px;font-size:14px;font-weight:700;color:#808080;font-family:-apple-system,BlinkMacSystemFont,Helvetica,Roboto,Verdana,Dotum,Arial,sans-serif;letter-spacing:-0.05em;'>출품가액<span style='margin-left:30px;color:#333333;'>".number_format($row['total_price'])."</span>원</div>" . PHP_EOL;
                    $wlist_html .= "                </td>" . PHP_EOL;
                    $wlist_html .= "              </tr>" . PHP_EOL;
                    $wlist_html .= "              <tr>" . PHP_EOL;
                    $wlist_html .= "                  <td colspan='2' height='36' style='margin:0;padding:0;font-size:0;line-height:0;height:36px;'></td>" . PHP_EOL;
                    $wlist_html .= "              </tr>" . PHP_EOL;
                }
                $wlist_html .= "              <tr>" . PHP_EOL;
                $wlist_html .= "                  <td colspan='2' height='1' bgcolor='#e0e0e0' style='margin:0;padding:0;font-size:0;line-height:0;height:1px;'></td>" . PHP_EOL;
                $wlist_html .= "              </tr>" . PHP_EOL;
                $wlist_html .= "              <tr>" . PHP_EOL;
                $wlist_html .= "                  <td colspan='2' height='22' style='margin:0;padding:0;font-size:0;line-height:0;height:22px;'></td>" . PHP_EOL;
                $wlist_html .= "              </tr>" . PHP_EOL;
                $wlist_html .= "              <tr>" . PHP_EOL;
                $wlist_html .= "                  <td colspan='2' style='margin:0;padding:0;color:#808080;font-size:16px;font-weight:700;line-height:20px;text-align:right;font-family:-apple-system,BlinkMacSystemFont,Helvetica,Roboto,Verdana,Dotum,Arial,sans-serif;letter-spacing:-0.05em;'>총 결제금액<span style='margin-left:30px;color:#333333;'>".number_format($payinfo['p_totalprice'])."</span>원</td>" . PHP_EOL;
                $wlist_html .= "              </tr>" . PHP_EOL;
                $wlist_html .= "          </table>" . PHP_EOL;
                $wlist_html .= "      </td>" . PHP_EOL;
                $wlist_html .= "  </tr>" . PHP_EOL;
                $wlist_html .= "</table>" . PHP_EOL;

                // $wlist_encode = str_replace('"', '\'', json_encode($wlist));
                $paymethod = $paytype[$payinfo['p_method']];
                $resultinfo = $payinfo['p_method']=="deposit"?"{$bankarray[$payinfo['p_result']]} {$payinfo['p_account']}":"{$cardarray[$payinfo['p_account']]}({$payinfo['p_result']})";
                $a_info_array = ["{{결제유형}}","{{결제번호}}","{{결제일시}}","{{결제정보}}","{{총결제금액}}","{{결제리스트}}"];
                $b_info_array = [$paymethod,$info_key,$payinfo['p_findate'],$resultinfo,$payinfo['p_totalprice'],$wlist_html];
            break;
            case "pa_request"://결제시
                $info_query = "select * from tb_payment where p_no = '{$info_key}'";
                $result =  $CI->db->query($info_query);
                $payinfo = $result->row_array();
                $work_query = "SELECT a.w_id,a.w_title,a.c_id,a.w_date,a.w_start,a.w_end,a.p_no,a.wp_no,a.w_status,a.w_thumbnail,a.total_price,b.cp_id,b.c_title
                    FROM tb_work AS a
                    LEFT JOIN tb_category AS b ON a.c_id = b.c_id 
                    WHERE a.p_no = '{$info_key}' AND a.is_del = 'N'
                    ORDER BY a.w_id DESC";
                $work_result =  $CI->db->query($work_query);
                $wlist = [];

                foreach ($work_result->result() as $key1 => $row) {
                    $wlist[$key1] = [
                        "w_id"=>$row->w_id,
                        "w_title"=>$row->w_title,
                        "c_id"=>$row->c_id,
                        "cp_id"=>$row->cp_id,
                        "c_title"=>$row->c_title,
                        "w_date"=>$row->w_date,
                        "w_start"=>$row->w_start,
                        "w_end"=>$row->w_end,
                        "p_no"=>$row->p_no,
                        "wp_no"=>$row->wp_no,
                        "w_status"=>$row->w_status,
                        "w_thumbnail"=>$row->w_thumbnail
                    ];

                    $payment_price = $payinfo['p_totalprice'];
                    $total_price_data = $row->total_price;
                    $show_payment_price = $payment_price>0?($payment_price<$total_price_data?$payment_price:$total_price_data):0;
                    $wlist[$key1]['total_price'] = $show_payment_price;
                    $payment_price -= $show_payment_price;
                    $wlist[$key1]['price_dc'] = $show_payment_price < $total_price_data ? 'Y' : 'N';

                    $arr_c_id = $wlist[$key1]['c_id'];
                    $category_array = explode(",", $arr_c_id);
                    foreach ($category_array as $key2 => $c_id) {
                        $category_query = $CI->db->get_where('tb_category', "c_id={$c_id}");
                        $category_res = $category_query->result();
                        $wlist[$key1]['category_data'][$key2] = $category_res[0];
                    }
                    $cptitle_query = "select * from tb_category where c_id = {$wlist[$key1]['cp_id']}";
                    $cptitle_result = $CI->db->query($cptitle_query)->result();
                    $wlist[$key1]['cp_title'] = $cptitle_result[0]->c_title;
                }

                $wlist_html = "<table border='0' cellpadding='0' cellspacing='0' align='center' width='100%' style='border-collapse:collapse;'>" . PHP_EOL;
                $wlist_html .= "  <tr>" . PHP_EOL;
                $wlist_html .= "      <td style='margin:0;padding:0;'>" . PHP_EOL;
                $wlist_html .= "          <table border='0' cellpadding='0' cellspacing='0' align='center' width='100%' style='border-collapse:collapse;'>" . PHP_EOL;
                foreach ($wlist as $row) {
                    $count_cate = count($row['category_data']);
                    $cate_name =  $count_cate > 1 ? '외 '.($count_cate-1).'건' : '';
                    $wlist_html .= "              <tr>" . PHP_EOL;
                    $wlist_html .= "                  <td width='244' style='margin:0;padding:0;'><img src='".$row['w_thumbnail']."' alt='' width='220' height='132' style='display:block;'></td>" . PHP_EOL;
                    $wlist_html .= "                  <td style='margin:0;padding:0;'>" . PHP_EOL;
                    $wlist_html .= "                      <div style='margin:0;padding:0;height:48px;overflow:hidden;font-size:20px;font-weight:700;color:#333333;font-family:-apple-system,BlinkMacSystemFont,Helvetica,Roboto,Verdana,Dotum,Arial,sans-serif;letter-spacing:-0.05em;'>".$row['w_title']."</div>" . PHP_EOL;
                    $wlist_html .= "                      <div style='margin:0;padding:0;margin-top:8px;font-size:13px;font-weight:700;color:#808080;font-family:-apple-system,BlinkMacSystemFont,Helvetica,Roboto,Verdana,Dotum,Arial,sans-serif;letter-spacing:-0.05em;'>".$row['cp_title']."</div>" . PHP_EOL;
                    $wlist_html .= "                      <div style='margin:0;padding:0;margin-top:2px;font-size:13px;font-weight:700;color:#808080;font-family:-apple-system,BlinkMacSystemFont,Helvetica,Roboto,Verdana,Dotum,Arial,sans-serif;letter-spacing:-0.05em;'>".$row['category_data'][0]->c_title." ".$cate_name."</div>" . PHP_EOL;
                    $wlist_html .= "                      <div style='margin:0;padding:0;margin-top:8px;font-size:14px;font-weight:700;color:#808080;font-family:-apple-system,BlinkMacSystemFont,Helvetica,Roboto,Verdana,Dotum,Arial,sans-serif;letter-spacing:-0.05em;'>출품가액<span style='margin-left:30px;color:#333333;'>".number_format($row['total_price'])."</span>원</div>" . PHP_EOL;
                    $wlist_html .= "                </td>" . PHP_EOL;
                    $wlist_html .= "              </tr>" . PHP_EOL;
                    $wlist_html .= "              <tr>" . PHP_EOL;
                    $wlist_html .= "                  <td colspan='2' height='36' style='margin:0;padding:0;font-size:0;line-height:0;height:36px;'></td>" . PHP_EOL;
                    $wlist_html .= "              </tr>" . PHP_EOL;
                }
                $wlist_html .= "              <tr>" . PHP_EOL;
                $wlist_html .= "                  <td colspan='2' height='1' bgcolor='#e0e0e0' style='margin:0;padding:0;font-size:0;line-height:0;height:1px;'></td>" . PHP_EOL;
                $wlist_html .= "              </tr>" . PHP_EOL;
                $wlist_html .= "              <tr>" . PHP_EOL;
                $wlist_html .= "                  <td colspan='2' height='22' style='margin:0;padding:0;font-size:0;line-height:0;height:22px;'></td>" . PHP_EOL;
                $wlist_html .= "              </tr>" . PHP_EOL;
                $wlist_html .= "              <tr>" . PHP_EOL;
                $wlist_html .= "                  <td colspan='2' style='margin:0;padding:0;color:#808080;font-size:16px;font-weight:700;line-height:20px;text-align:right;font-family:-apple-system,BlinkMacSystemFont,Helvetica,Roboto,Verdana,Dotum,Arial,sans-serif;letter-spacing:-0.05em;'>총 결제금액<span style='margin-left:30px;color:#333333;'>".number_format($payinfo['p_totalprice'])."</span>원</td>" . PHP_EOL;
                $wlist_html .= "              </tr>" . PHP_EOL;
                $wlist_html .= "          </table>" . PHP_EOL;
                $wlist_html .= "      </td>" . PHP_EOL;
                $wlist_html .= "  </tr>" . PHP_EOL;
                $wlist_html .= "</table>" . PHP_EOL;

                // $wlist_encode = str_replace('"', '\'', json_encode($wlist));
                $paymethod = $paytype[$payinfo['p_method']];
                $bankinfo = $payinfo['p_method']=="deposit"?"{$bankarray[$payinfo['p_result']]} {$payinfo['p_account']}":"";
                $a_info_array = ["{{결제유형}}","{{결제번호}}","{{결제일시}}","{{입금계좌번호}}","{{입금자명}}","{{입금마감일}}","{{총결제금액}}","{{결제리스트}}"];
                // $b_info_array = [$paymethod,$info_key,$payinfo['p_subdate'],$bankinfo,$payinfo['p_accountname'],$award_result['reaward_end'],$payinfo['p_totalprice'],$wlist_html];
                $b_info_array = [$paymethod,$info_key,$payinfo['p_subdate'],$bankinfo,$payinfo['p_accountname'],$award_result['award_end'],$payinfo['p_totalprice'],$wlist_html];
            break;
            case "aw_finish"://출품시
            break;
            case "ex_join"://가입시
                $user_id = base64_encode($user_row['u_id']);
                $auth_code = $user_row['auth_code'];
                $str_date = strtotime($user_row['u_datetime']);
                $domain = "http://{$_SERVER['SERVER_NAME']}";
                $link = "{$domain}/mail_auth/?code={$auth_code}&stamp={$str_date}&idx={$user_id}";
                $a_info_array = ["{{인증링크}}"];
                $b_info_array = [$link];
            break;
            case "ex_password"://비밀번호 찾기시
                $user_id = base64_encode($user_row['u_id']);
                $auth_code = $user_row['auth_code'];
                $domain = "http://{$_SERVER['SERVER_NAME']}";
                $link = "{$domain}/change_password/?code={$auth_code}&idx={$user_id}";
                $a_info_array = ["{{비밀번호링크}}"];
                $b_info_array = [$link];
            break;
        }
        $a_array = array_merge($a_array, $a_info_array);
        $b_array = array_merge($b_array, $b_info_array);
        foreach ($a_array as $a_key => $a_value) {
            $b_value = $b_array[$a_key];
            $contents = $CI->notice->replace_template($a_value, $b_value, $contents);
        }
        return $contents;
    }

    public static function send_notification($method, $u_no, $group, $type, $info_key=null)
    {
        $CI =& get_instance();
        $CI->load->model('user_model');
        $CI->load->helper('common');
        $CI->load->library('notice');
        $msg = "회원정보가 없습니다.";
        $query = "select n_{$method} from tb_notification where n_group = '{$group}' and n_type = '{$type}'";
        $result =  $CI->db->query($query);
        $notice_config = $result->row_array();
        $return = "-1";
        $mode = "{$group}_{$type}";
        if ($notice_config["n_{$method}"]==0) {
            $msg = "전송이 설정되지 않았습니다.";
        } else {
            $method_config_array = [
                    "sms"=>[
                        "title"=>"문자",
                        "subject"=>"n_sms_subject",
                        "contents"=>"n_sms_contents",
                        "address_field"=>"u_phone2"
                    ],
                    "mail"=>[
                        "title"=>"메일",
                        "subject"=>"n_mail_subject",
                        "contents"=>"n_mail_contents",
                        "address_field"=>"u_email"
                    ]
            ];
            $method_config = $method_config_array["{$method}"];
            if ($u_no>0) {
                $user_result = $CI->user_model->get_user($u_no);
                $msg = "회원정보를 찾을 수 없습니다.";
                if ($user_result['status']=="00") {
                    $user_row = $user_result['data'];
                    $user_name = $user_row['u_name'];
                    $query = "select * from tb_notification_contents where n_group = '{$group}' and n_type = '{$type}'";
                    $result =  $CI->db->query($query);
                    $template = $result->row_array();
                    $msg = "템플릿 정보가 없습니다.";
                    if (is_null($template)==false) {
                        $receiv = $user_row["{$method_config['address_field']}"];
                        $subject = $CI->notice->replace_result($u_no, $template[$method_config['subject']], $mode, $info_key);
                        $contents_origin = $CI->notice->replace_result($u_no, $template[$method_config['contents']], $mode, $info_key);
                        if ($method == 'mail') {
                            $contents = str_replace(array("\r\n","\r","\n"), '', $contents_origin);
                        } elseif ($method == 'sms') {
                            $contents = str_replace(array("\r\n","\r","\n"), '\n', $contents_origin);
                        }
                        $msg = "{$method_config['title']}전송이 실패하였습니다.";
                        $send_status = $receiv==""?false:true;
                        $return = $send_status==true?$CI->notice->send_toast($method, $receiv, $subject, $contents):"-1";
                        if ($return===0) {
                            $msg = "{$user_name}({$receiv})에게 {$method_config['title']}를 성공적으로 전송하였습니다.";
                            $CI->admin_model->logging_admin("{$user_name}({$receiv})에게 {$method_config['title']}전송", $u_no);
                        }
                    }
                }
            }
        }
        return $return;
    }
        
    public function send_toast($method, $address, $subject, $contents)
    {
        if ($method=="sms") {
            return $this->toast_sms($address, $subject, $contents);
        } elseif ($method=="mail") {
            return $this->toast_mail($address, $subject, $contents);
        }
    }

    public static function send_toast_form($method, $address, $subject, $contents)
    {
        $CI =& get_instance();
        $CI->load->library('notice');
        if ($method=="sms") {
            return $CI->notice->toast_sms($address, $subject, $contents);
        } elseif ($method=="mail") {
            return $CI->notice->toast_mail($address, $subject, $contents);
        }
    }

    public function toast_sms($phone_number, $subject, $contents)
    {
        $sender_phone_number = $this->sender_number;
        $json = '{"title":"'.$subject.'","body":"'.$contents.'","sendNo":"'.$sender_phone_number.'","recipientList":[{"recipientNo":"'.$phone_number.'"}]}';
        $appKey = $this->config_sms_appkey;
        $domain = $this->config_sms_domain;
        $url = "{$domain}/sms/v2.3/appKeys/{$appKey}/sender/mms";
        $result = $this->socket_post($url, $json);
        $resultRawArr = json_decode($result);
        $result_code = $resultRawArr->header->resultCode;
        return $result_code;
    }

    public function toast_mail($mail_address, $subject, $contents)
    {
        $sender_mail_address = $this->sender_address;
        $json = '{"title":"'.$subject.'","body":"'.$contents.'","senderAddress":"'.$sender_mail_address.'","receiverList":[{"receiveMailAddr":"'.$mail_address.'","receiveType":"MRT0"}]}';
        $appKey = $this->config_mail_appkey;
        $domain = $this->config_mail_domain;
        //$url = "{$domain}/email/v1.6/appKeys/{$appKey}/sender/mail";
        //$result = $this->socket_post($url, $json);
        //$resultRawArr = json_decode($result);

        $url = "{$domain}/email/v1.6/appKeys/{$appKey}/sender/mail";
        $curl = curl_init();
        curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS =>$json,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));
        $response = curl_exec($curl);
        curl_close($curl);
        $resultRawArr = json_decode($response);
        $result_code = $resultRawArr->header->resultCode;
        return $result_code;
    }
        
    private function cryptoJsAesEncrypt($key, $value)
    {
        $key = $this->config_bill_encrkey;
        $salt = openssl_random_pseudo_bytes(8);
        //encKey.substring(0, 256 / 8)
        $key = substr($key, 0, 256 / 8);
        $iv  = substr($key, 0, 128 / 8);
        $json = json_encode($value, JSON_UNESCAPED_UNICODE);
        $encrypted_data = openssl_encrypt($json, 'aes-256-cbc', $key, true, $iv);
        $data = array("ct" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "s" => bin2hex($salt));
        return base64_encode($encrypted_data);
    }

    public function toast_bill($billing_data)
    {
        $crtKey = $this->config_bill_crtkey;
        $encrKey = $this->config_bill_encrkey;
        $appKey = $this->config_bill_appkey;

        $crypto_data = $this->cryptoJsAesEncrypt($encrKey, $billing_data);
        $json = urlencode($crypto_data);
        $domain = $this->config_bill_domain;
        $url = "{$domain}/taxbill/v1.0/crtkey/{$crtKey}/issue?p={$json}";

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_HTTPHEADER => array("cache-control: no-cache"),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        return json_decode($response);
    }
        
    public function socket_post($url, $json)
    {
        $url = parse_url($url);
        $host = $url['host'];
        $path = $url['path'];

        //소켓 오픈
        $fp = fsockopen($host, 80, $errno, $errstr, 10.0);
        if (!is_resource($fp)) {
            echo "not connect host : errno=$errno,errstr=$errstr";
            exit;
        }

        fputs($fp, "POST $path HTTP/1.1\r\n");
        fputs($fp, "Host: $host\r\n");
        fputs($fp, "Content-type: application/json;charset=UTF-8\r\n");
        fputs($fp, "Content-length: " . strlen($json) . "\r\n");
        fputs($fp, "Connection:close" . "\r\n\r\n");
        fputs($fp, $json);

        //반환값 받기
        $result = '';
        while (!feof($fp)) {
            $result .= fgets($fp, 128);
        }
        fclose($fp);

        $result = explode("\r\n\r\n", $result, 2);

        $header = isset($result[0]) ? $result[0] : '';
        $content = isset($result[1]) ? $result[1] : '';

        return $content;
    }
}
