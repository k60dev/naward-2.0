<?php
function get_global_award($a_id)
{
    $config = ["more_works_than_this"=>"5","global_work_price"=>10000];
    return $config;
}

function get_auth()
{
    $ci =& get_instance();
    $ua_auth = $ci->session->userdata('ua_auth');
    if ($ci->session->userdata('is_super')=='Y') {
        $ua_auth = '*';
    }
    return $ua_auth;
}

function get_userinfo()
{
    $ci =& get_instance();
    $u_result = $ci->session->all_userdata();
    return $u_result;
}

function is_login()
{
    $ci =& get_instance();
    $u_no = $ci->session->userdata('u_no');
    $u_result = $u_no>0 ? true : false;
    return $u_result;
}

function is_admin()
{
    $ci =& get_instance();
    $ua_id = $ci->session->userdata('ua_id');
    $u_result = $ua_id>0 ? true : false;
    return $u_result;
}

function get_access($access_auth)
{
    $ua_auth = get_auth();
    $access = strpos($ua_auth, $access_auth)!==false||$ua_auth=="*"?"00":"99";
    return $access;
}

function pack_return($access, $data, $msg=false)
{
    $ci =& get_instance();
    $returnData = $access=="00"?$data:[];
    $returnMsg = $msg!=false?$msg:"";
    switch ($access) {
        case "00":
            $returnCode = "200";
        break;
        case "99":
            $returnCode = "401";
        break;
        case "39"://리무브 실패
            $returnCode = "400";
            $returnMsg = $returnMsg?$returnMsg:"failed to removee";
        break;
        case "29"://리드 실패
            $returnCode = "400";
            $returnMsg = $returnMsg?$returnMsg:"failed to read";
        break;
        case "19"://업데이트 실패
            $returnCode = "400";
            $returnMsg = $returnMsg?$returnMsg:"failed to modify";
        break;
        case "09"://인서트 실패
            $returnCode = "400";
            $returnMsg = $returnMsg?$returnMsg:"failed to create";
        break;
    }

    $ci->output->set_status_header($returnCode, $returnMsg);
    return ["status"=>$returnCode,"data"=>$returnData];
}



function return_refactoring($status, $data, $msg, $access="00")
{
    $ci =& get_instance();
    $returnData = $access=="00"?$data:[];
    $returnMsg = $msg!=""?$msg:"";
    if ($access=="00") {
        switch ($status) {
            case "00":
                $returnCode = "200";
            break;
            case "69"://접근 실패
                $returnCode = "404";
                $returnMsg = $returnMsg?$returnMsg:"not found";
            break;
            case "79"://접근 실패
                $returnCode = "403";
                $returnMsg = $returnMsg?$returnMsg:"failed to access";
            break;
            case "99"://접근 실패
                $returnCode = "401";
                $returnMsg = $returnMsg?$returnMsg:"failed to access";
            break;
            case "89"://리소스 중복 Conflict
                $returnCode = "409";
                $returnMsg = $returnMsg?$returnMsg:"conflict";
            break;
            case "39"://리무브 실패
                $returnCode = "400";
                $returnMsg = $returnMsg?$returnMsg:"failed to removee";
            break;
            case "29"://리드 실패
                $returnCode = "400";
                $returnMsg = $returnMsg?$returnMsg:"failed to read";
            break;
            case "19"://업데이트 실패
                $returnCode = "400";
                $returnMsg = $returnMsg?$returnMsg:"failed to modify";
            break;
            case "09"://인서트 실패
                $returnCode = "400";
                $returnMsg = $returnMsg?$returnMsg:"failed to create";
            break;
        }
    } else {
        $returnCode = "401";
    }
    $returnMsg = json_encode($returnMsg);
    $ci->output->set_status_header($returnCode, $returnMsg);
    return ["status"=>$returnCode,"data"=>$returnData];
}

function class_return_refactoring($status, $data, $msg="")
{
    $returnArray = [
        "status"=>$status,
        "data"=>$data,
        "msg"=>$msg
    ];
    return $returnArray;
}

function set_award_session($a_id)
{
    $ci =& get_instance();
    $a_session = array('a_id'=>$a_id);
    $ci->session->set_userdata($a_session);
    $a_id = $ci->session->userdata('a_id');
    return $a_id;
}

function get_award_session()
{
    $ci =& get_instance();
    //$a_session = array('a_id'=>'2020');
    //$ci->session->set_userdata($a_session);
    $a_id = $ci->session->userdata('a_id');
    return $a_id;
}

function dateDifference($date_1, $date_2, $differenceFormat = '%R%a')
{
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);
    $interval = date_diff($datetime1, $datetime2);
    return $interval->format($differenceFormat);
}

function create_encrypt($max, $length)
{
    $char = ["A","B","C","D","E","F","G","H","I","J","K","0","1","2","3","4","5","6","7","8","9","10","!","@","$","^","*","-","_"];
    $rand_area = count($char);
    $max_char = $length;
    $string = "";
    for ($i=0;$i<=$max_char;$i++) {
        $rand = rand(0, $max);
        $char_rand_seed = floor($rand%$rand_area);
        $string .= $char[$char_rand_seed];
    }
    return $string;
}

function create_mail_autoricate()
{
    return create_encrypt(9999, 10);
}

function fail_result($msg=null)
{
    $result = ["status"=>99,"data"=>[],"msg"=>$msg];
    return $result;
}

function export_csv($filename, $header, $body)
{
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Type: text/csv;charset=UTF-8");
    header("Content-Disposition: attachment;filename={$filename}.csv");
    header("Content-Transfer-Encoding: binary");


    ob_start();
    $df = fopen("php://output", 'w');
    echo "\xEF\xBB\xBF";
    fputcsv($df, $header);
    foreach ($body as $row) {
        $re_row = str_replace("#","＃",$row);
        fputcsv($df, $re_row);
    }
    fclose($df);
    echo ob_get_clean();
    die();
}

function print_r2($var)
{
    ob_start();
    print_r($var);
    $str = ob_get_contents();
    ob_end_clean();
    $str = str_replace(" ", "&nbsp;", $str);
    echo nl2br("<span style='font-family:Tahoma, 굴림; font-size:9pt;'>$str</span>");
}
