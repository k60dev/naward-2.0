<?php

function get_upload_token()
{
    $auth_url = 'https://api-identity.infrastructure.cloud.toast.com/v2.0';
    $tenant_id = '1d7131c8fbf34ac882916743c7d6760d';
    $username = 'master@dea.or.kr';
    $password = 'dea7206@';

    $req_url = "{$auth_url}/tokens";
    $req_body = [
                'auth' =>[
                        'tenantId' => $tenant_id,
                        'passwordCredentials' => [
                                'username' => $username,
                                'password' => $password
                        ]
                ]
        ];
    $req_header = array(
            'Content-Type: application/json'
        );  // 요청 헤더 생성
        
    $curl_array = [
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => $req_header,
                CURLOPT_POSTFIELDS => json_encode($req_body)
        ];
    $response = file_curl($req_url, $curl_array);
        
    $contents = json_decode($response);
    $token_id = $contents->access->token->id;
    return $token_id;
}



function get_url($dir)//$container, $object
{
    //return storage_url . '/' . $container . '/' . $object;
    return "https://api-storage.cloud.toast.com/v1/AUTH_1d7131c8fbf34ac882916743c7d6760d/{$dir}";
}

function get_request_header()
{
    return array(
                'X-Auth-Token: ' . get_upload_token()
        );
}

function storage_exist($storage, $path=null)
{
    echo $path;
    $req_url = get_url($storage);
    $req_url .= $path != null? "?path={$path}":"";
    $req_header = get_request_header();
    $curl_array = [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => $req_header,
        ];
    $response = file_curl($req_url, $curl_array);

    $object_list = explode("\n", $response);
    return $object_list;
}

function storage_create($storage)
{
    $req_url = get_url($storage);
    $req_header = get_request_header();
    $curl_array = [
                CURLOPT_PUT => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => $req_header
        ];
    $response = file_curl($req_url, $curl_array);
    storage_modify($storage);
}

function storage_modify($storage)
{
    $req_url = get_url($storage);

    //$permission = $is_public ? '.r:*' : '';
    $permission = '.r:*';
    $req_header = get_request_header();
    $req_header[] = 'X-Container-Read: ' . $permission;  // 헤더에 권한 추가
    $curl_array = [
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => $req_header
        ];
    $response = file_curl($req_url, $curl_array);

    return $response;
}

function storage_upload($dir, $upload_result)
{
    //
    $req_url = get_url($dir);
    $req_header = get_request_header();
    $upload_state = $upload_result["status"]=="00"?true:false;
    $returnArray = ["path"=>"","status"=>"99","msg"=>"업로드 실패!"];
    if ($upload_state==true) {
        $upload_path = $upload_result['upload_path'];
        $file_name = $upload_result['file_name'];
        $req_url .= "/{$file_name}";
        $full_file = "{$upload_path}/{$file_name}";
    
        $fd = fopen($full_file, 'r');  // 파일을 연다.
        $curl_array = [
                        CURLOPT_PUT => true,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_INFILE => $fd,  // 파일 스트림을 매개변수로 넣는다.
                        CURLOPT_HTTPHEADER => $req_header
                ];
        $response = file_curl($req_url, $curl_array);
        fclose($fd);
        $returnArray = ["path"=>$req_url,"status"=>"00","msg"=>"업로드 성공!"];
    }
    return $returnArray;
}

function storage_upload_exist($dir, $upload, $path)
{
    //
    $req_url = get_url($dir);
    $req_header = get_request_header();
    $upload_path = $path;
    $file_name = $upload;
    $req_url .= "/{$file_name}";
    $full_file = "{$upload_path}/{$file_name}";

    $fd = fopen($full_file, 'r');  // 파일을 연다.
    $curl_array = [
                    CURLOPT_PUT => true,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_INFILE => $fd,  // 파일 스트림을 매개변수로 넣는다.
                    CURLOPT_HTTPHEADER => $req_header
            ];
    $response = file_curl($req_url, $curl_array);

    fclose($fd);
    $returnArray = ["path"=>$response,"status"=>"00","msg"=>"업로드 성공!"];
    return $returnArray;
}

function storage_file_download($dir, $file_name)
{
    $req_url = get_url($dir."/".$file_name);
    $new_file = "assets/resource/{$file_name}";
    $req_header = get_request_header();
    $fd = fopen($new_file, 'w');
    $curl_array = [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FILE => $fd,
                CURLOPT_HTTPHEADER => $req_header
        ];
    $response = file_curl($req_url, $curl_array);
    fclose($fd);
    return $new_file;
}

function storage_path_download($path, $file_name)
{
    $req_url = $path.$file_name;
    $new_file = "assets/resource/{$file_name}";
    $req_header = get_request_header();
    $fd = fopen($new_file, 'w');
    $curl_array = [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FILE => $fd,
                CURLOPT_HTTPHEADER => $req_header
        ];
    $response = file_curl($req_url, $curl_array);
    fclose($fd);
    return $new_file;
}

function storage_file_delete($dir, $file_name)
{
    $req_url = get_url($dir);
    $req_url .= $file_name;
    $req_header = get_request_header();

    $curl_array = [
                CURLOPT_CUSTOMREQUEST => "DELETE",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => $req_header
        ];
    $response = file_curl($req_url, $curl_array);
}

function storage_url_delete($url, $file_name)
{
    $req_url = $url.$file_name;
    $req_header = get_request_header();

    $curl_array = [
                CURLOPT_CUSTOMREQUEST => "DELETE",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => $req_header
        ];
    $response = file_curl($req_url, $curl_array);
}

function file_curl($req_url, $curl_array)
{
    $curl  = curl_init($req_url);
    curl_setopt_array($curl, $curl_array);
    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}


function file_upload_myserver($img_dir)
{
    $ci =& get_instance();
    $config['upload_path'] = "./uploads/{$img_dir}";
    if (is_dir($config['upload_path'])==false) {
        mkdir($config['upload_path'], "0755", true);
    }
    $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
    //$config['max_size']	= '100';
    //$config['max_width']  = '10240';
    //$config['max_height']  = '7680';
    $field_name = "image";

    $ci->load->library('upload', $config);
    $path = $config['upload_path'];
    $status = "99";
    $imgname = "";
    $file_name = "";
    $full_path = "";
    if (! $ci->upload->do_upload($field_name)) {
        $msg = $ci->upload->display_errors();
    } else {
        $result = $ci->upload->data();
        $file_name = $result['file_name'];
        $full_path = $result['full_path'];
        $status = "00";
        $msg = "success";
        createMaxsizeImg("{$full_path}", "{$full_path}");
    }
    return ["status"=>$status,"msg"=>$msg,"full_path"=>$full_path,"file_name"=>$file_name,"upload_path"=>$path];
}

function upload_file_in_controller($config)
{
    extract($config);
    $file_url = get_url($type);
    $images = $_FILES;
    $result = ["status"=>"09","data"=>[],"msg"=>$msg];
    $ori_name = $images["{$picture_name}"]['name'];
    $file_info = pathinfo($ori_name);
    $img_name = "{$new_picture_name}.{$file_info['extension']}";
    $_FILES['image']['name'] = $img_name;
    $_FILES['image']['type'] = $images["{$picture_name}"]['type'];
    $_FILES['image']['tmp_name'] = $images["{$picture_name}"]['tmp_name'];
    $_FILES['image']['error'] = $images["{$picture_name}"]['error'];
    $_FILES['image']['size'] = $images["{$picture_name}"]['size'];
    $upload_result = file_upload_myserver($type);
    $storage_result = storage_upload($type, $upload_result);
    if ($upload_result['status']=="00") {
        $img_result = "{$upload_result['upload_path']}/{$upload_result['file_name']}";
        unlink($img_result);
    }
    return $storage_result;
}
