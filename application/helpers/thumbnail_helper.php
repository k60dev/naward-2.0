<?php
const IMAGE_HANDLERS = [
    IMAGETYPE_JPEG => [
            'load' => 'imagecreatefromjpeg',
            'save' => 'imagejpeg',
            'quality' => 100
    ],
    IMAGETYPE_PNG => [
            'load' => 'imagecreatefrompng',
            'save' => 'imagepng',
            'quality' => 0
    ],
    IMAGETYPE_GIF => [
            'load' => 'imagecreatefromgif',
            'save' => 'imagegif'
    ]
];

function createThumbnail($src, $dest, $targetWidth, $targetHeight)
{
    $type = exif_imagetype($src);
    if (!$type || !IMAGE_HANDLERS[$type]) {
        return null;
    }
    $image = call_user_func(IMAGE_HANDLERS[$type]['load'], $src);
    if (!$image) {
        return null;
    }

    $width = imagesx($image);
    $height = imagesy($image);
    $margin_top = 0;
    $margin_left = 0;

    $picture_width = $targetWidth;
    $picture_height = $targetHeight;

    $ratio = $width / $height;
    if ($width > $height) {/* 가로가 더 길경우 */
        $ratioHeight = floor($targetWidth / $ratio);
        $margin_top = ($targetHeight-$ratioHeight)/2;
        $picture_height = $ratioHeight;
    } elseif ($width <= $height) { /* 세로가 더 길경우 혹은 정방형일때 */
        
        $ratioWidth = floor($targetHeight * $ratio);
        if ($width>$targetWidth) {
            $picture_width ="500";//$width*$ratio;
        }
        //$picture_width = $targetWidth>$width?$targetWidth:$width;
        $margin_left = ($targetWidth-$ratioWidth)/2;
        $picture_width = $ratioWidth;
    } else { //기타..
        $ratioHeight = $targetWidth;
        $targetWidth = floor($targetWidth * $ratio);
    }


    $thumbnail = imagecreatetruecolor($targetWidth, $targetHeight);
    if ($type == IMAGETYPE_GIF || $type == IMAGETYPE_PNG) {
        imagecolortransparent(
            $thumbnail,
            imagecolorallocate($thumbnail, 0, 0, 0)
        );

        if ($type == IMAGETYPE_PNG) {
            imagealphablending($thumbnail, false);
            imagesavealpha($thumbnail, true);
        }
    }

    imagecopyresampled(
        $thumbnail,
        $image,
        $margin_left,
        $margin_top,
        0,
        0,
        $picture_width,
        $picture_height,
        $width,
        $height
    );

    return call_user_func(
        IMAGE_HANDLERS[$type]['save'],
        $thumbnail,
        $dest,
        IMAGE_HANDLERS[$type]['quality']
    );
}


function createMaxsizeImg($src, $dest, $targetWidth="2048", $targetHeight=null)
{

    // 1. Load the image from the given $src
    // - see if the file actually exists
    // - check if it's of a valid image type
    // - load the image resource

    // get the type of the image
    // we need the type to determine the correct loader
    $type = exif_imagetype($src);

    // if no valid type or no handler found -> exit
    if (!$type || !IMAGE_HANDLERS[$type]) {
        return null;
    }

    // load the image with the correct loader
    $image = call_user_func(IMAGE_HANDLERS[$type]['load'], $src);

    // no image found at supplied location -> exit
    if (!$image) {
        return null;
    }


    // 2. Create a thumbnail and resize the loaded $image
    // - get the image dimensions
    // - define the output size appropriately
    // - create a thumbnail based on that size
    // - set alpha transparency for GIFs and PNGs
    // - draw the final thumbnail

    // get original image width and height
    $width = imagesx($image);
    $height = imagesy($image);

    // maintain aspect ratio when no height set
    if ($targetHeight == null) {

        // get width to height ratio
        $ratio = $width / $height;

        // if is portrait
        // use ratio to scale height to fit in square
        if ($width > $height) {
            $targetHeight = floor($targetWidth / $ratio);
        }
        // if is landscape
        // use ratio to scale width to fit in square
        else {
            $targetHeight = $targetWidth;
            $targetWidth = floor($targetWidth * $ratio);
        }
    }
    $targetWidth = $width>$targetWidth?$targetWidth:$width;
    $targetHeight = $width>$targetWidth?$targetHeight:$height;

    // create duplicate image based on calculated target size
    $thumbnail = imagecreatetruecolor($targetWidth, $targetHeight);

    // set transparency options for GIFs and PNGs
    if ($type == IMAGETYPE_GIF || $type == IMAGETYPE_PNG) {

        // make image transparent
        imagecolortransparent(
            $thumbnail,
            imagecolorallocate($thumbnail, 0, 0, 0)
        );

        // additional settings for PNGs
        if ($type == IMAGETYPE_PNG) {
            imagealphablending($thumbnail, false);
            imagesavealpha($thumbnail, true);
        }
    }

    // copy entire source image to duplicate image and resize
    imagecopyresampled(
        $thumbnail,
        $image,
        0,
        0,
        0,
        0,
        $targetWidth,
        $targetHeight,
        $width,
        $height
    );


    // 3. Save the $thumbnail to disk
    // - call the correct save method
    // - set the correct quality level

    // save the duplicate version of the image to disk
    return call_user_func(
        IMAGE_HANDLERS[$type]['save'],
        $thumbnail,
        $dest,
        IMAGE_HANDLERS[$type]['quality']
    );
}


function upload_local_file($img_dir)
{
    $config['upload_path'] = "./uploads/{$img_dir}";
    if (is_dir($config['upload_path'])==false) {
        mkdir($config['upload_path'], "0755", true);
    }
    $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
    //$config['max_size']	= '100';
    //$config['max_width']  = '10240';
    //$config['max_height']  = '7680';
    $field_name = "image";

    $path = $config['upload_path'];
    $status = "99";
    $imgname = "";
    $file_name = "";
    $full_path = "";
    $ci =& get_instance();
    $ci->load->library('upload', $config);
    if (! $ci->upload->do_upload($field_name)) {
        $msg = $ci->upload->display_errors();
    } else {
        $result = $ci->upload->data();
        $file_name = $result['file_name'];
        $full_path = $result['full_path'];
        $status = "00";
        $msg = "success";
        createMaxsizeImg("{$full_path}", "{$full_path}");
    }
    return ["status"=>$status,"msg"=>$msg,"full_path"=>$full_path,"file_name"=>$file_name,"upload_path"=>$path];
}
