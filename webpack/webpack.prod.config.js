'use strict'
const merge = require('webpack-merge')
const webpack = require('webpack')
const baseWebpackConfig = require('./webpack.config')
// const config = require('../config').prod

const prodWebpackConfig = merge(baseWebpackConfig, {
  mode: 'production',
  plugins: [
    new webpack.DefinePlugin({
      // env : config.env
    }),
  ],
  watch: false,
  cache: false,
  optimization: {
    minimize: true,
  }
})

module.exports = prodWebpackConfig