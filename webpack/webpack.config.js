'use strict';
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { VueLoaderPlugin } = require('vue-loader');
const path = require('path');

module.exports = {
  entry: {
    app: ['@babel/polyfill', path.resolve(__dirname, './src') + '/index.js'],
  },
  output: {
    path: path.resolve(__dirname, '../assets/vue/'),
    publicPath: '/assets/vue/',
    filename: 'js/[name].[chunkhash].js',
    chunkFilename: 'js/[id]_[chunkhash].js',
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        include: [path.resolve(__dirname, './src')],
      },
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: ['@babel/plugin-syntax-dynamic-import'],
          }
        },
        include: [path.resolve(__dirname, './src')],
        exclude: [/node_modules/],
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [
                require('autoprefixer')
              ]
            },
          },
        ],
      },
      {
        test: /\.s(c|a)ss$/,
        use: [
          // 'vue-style-loader',
          {
            loader: MiniCssExtractPlugin.loader,
          },
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [
                require('autoprefixer')
              ]
            },
          },
          {
            loader: 'sass-loader',
            options: {
              implementation: require('sass'),
              sassOptions: {
                fiber: require('fibers'),
                outputStyle: 'compressed',
              },
              // prependData: '@import "@assets/sass/common.scss";',
            },
          },
          // {
          //   loader: 'sass-resources-loader',
          //   options: {
          //     resources: [path.resolve(__dirname, '../assets/sass/common.scss')],
          //   },
          // },
        ],
      },
      {
        test: /\.(png|jpg|gif|ico|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]',
        },
      },
    ],
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src'),
      '@assets': path.resolve(__dirname, '../assets'),
    },
    extensions: ['.vue', '.js', '.scss', '.json'],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: 'css/[name].[contenthash].css',
      chunkFilename: 'css/[id]_[contenthash].css',
    }),
    new HtmlWebpackPlugin({
      filename: path.resolve(__dirname, '../application/views/vue') + '/index.html',
      template: path.resolve(__dirname, './src') + '/index.html',
      inject: true,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: false,
      },
    }),
  ],
  optimization: {
    splitChunks: {
      chunks: 'all',
      minSize: 30000,
      minChunks: 1,
      maxAsyncRequests: 5,
      maxInitialRequests: 5,
      automaticNameDelimiter: '_',
      name: true,
      cacheGroups: {
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true,
        },
        vuetify: {
          test: /[\\/]node_modules[\\/](vuetify)(.[a-zA-Z0-9.\-_]+)[\\/]/,
          priority: -10,
        },
        vendors: {
          test: /[\\/]node_modules[\\/](?!vuetify)(.[a-zA-Z0-9.\-_]+)[\\/]/,
          priority: -9,
        },
      },
    },
  },
};
