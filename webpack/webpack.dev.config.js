'use strict'
const merge = require('webpack-merge')
const webpack = require('webpack')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const baseWebpackConfig = require('./webpack.config')
// const config = require('../config').dev

const devWebpackConfig = merge(baseWebpackConfig, {
  mode: 'development',
  devtool: 'inline-source-map',
  plugins: [
    new BundleAnalyzerPlugin({
      openAnalyzer: false
    }),
    new webpack.DefinePlugin({
      // env : config.env
    }),
  ],
  watch: true,
  cache: true,
  optimization: {
    minimize: false,
  }
})

module.exports = new Promise((resolve, reject) => {
  resolve(devWebpackConfig);
})
