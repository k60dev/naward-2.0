import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import IconList from '@/components/common/IconList.vue'

Vue.use(Vuetify)

const vuetify = new Vuetify({
  theme: {
    dark: true,
    themes: {
      dark: {
        // primary: '#161d77',
        primary: '#0819ed',
        secondary: '#ffc107',
        accent: '#0819ed',
        error: '#ff1c86',
        info: '#2196f3',
        success: '#4dba35',
        warning: '#c70039',
        anchor: '#ffffff',
      },
    },
    options: {
      variations: false
    },
    disable: true
  },
  icons: {
    iconfont: 'mdi',
    values: {
      checked: 'mdi-square',
      unchecked: 'mdi-square-outline',
      customList: {
        component: IconList,
      }
    },
  },
})

export default vuetify