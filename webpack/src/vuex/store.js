import Vue from 'vue'
import Vuex from 'vuex'

import * as modules from './modules/index'

Vue.use(Vuex)

export default new Vuex.Store({
  state : {
    user: false,
    admin: false,
    auth: false,
    payment_price: 0,
    skeleton: true,
    offsetTop: 0,
    offsetBottom: 0,
    scrollInvoked: 0,
    sortBy: '',
    sortDesc: true,
    searchFilter: '',
    categoryFilter: '',
    subcategoryFilter: '',
    itemsPerPageValue: 10,
  },
  // getters: {
  //   isLogin(state) {
  //     return state.user !== null;
  //   }
  // },
  mutations : {
    setAuthUser(state, user) {
      state.user = user
    },
    setAuthAdmin(state, admin) {
      state.admin = admin
    },

    setPaymentPrice(state, payment_price) {
      state.payment_price = payment_price
    },

    setSkeleton(state, skeleton) {
      state.skeleton = skeleton
    },
    setOffsetTop(state, offsetTop) {
      state.offsetTop = offsetTop
    },
    setOffsetBottom(state, offsetBottom) {
      offsetBottom = document.body.scrollHeight - (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight);
      state.offsetBottom = offsetBottom
    },
    setScrollInvoked(state, scrollInvoked) {
      state.scrollInvoked = scrollInvoked
    },
    setSearchFilter (state, searchFilter) {
      state.searchFilter = searchFilter
    },
    setSortBy (state, sortBy) {
      state.sortBy = sortBy
    },
    setSortDesc (state, sortDesc) {
      state.sortDesc = sortDesc
    },
    setCategoryFilter (state, categoryFilter) {
      state.categoryFilter = categoryFilter
    },
    setSubcategoryFilter (state, subcategoryFilter) {
      state.subcategoryFilter = subcategoryFilter
    },
    setItemsPerPageValue (state, itemsPerPageValue) {
      state.itemsPerPageValue = itemsPerPageValue
    },
    showSnackbar(state, payload) {
      state.snackbarContent = payload.content
      state.snackbarColor = payload.color
      state.snackbarIcon = payload.icon
      state.snackbarLight = payload.light
    },
    showReady(state, modal) {
      state.modalReadyTitle = modal.title
      state.modalReadyContent = modal.content
    }
  },
  actions : {},
  modules : modules.default
})
