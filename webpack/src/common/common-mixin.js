import Vue from 'vue'
import axios from 'axios'

const TIMEOUT = '3000'

Vue.mixin({
  data: () => {
    return {
      //이곳에 선언하는 data 속성은 전역에서 this로 접근 가능합니다.
    }
  },
  created: function() {
    this.axios = axios.create({
      timeout: TIMEOUT,
      withCredentials: true,
      headers : { }
    });
    this.axios.interceptors.response.use(this.onSuccess, this.onError)
  },

  methods: {
    onSuccess : response => { },
    onError : function (error) { },

    checkIfLogged() {
      var vm = this;
      return new Promise((resolve, reject) => {
        axios.get('/api/service/islogin')
          .then(response => {
            resolve(response.data);
          })
          .catch(error => {
            reject(error.response.data);
          });
      })
    },

    checkIfAdmin() {
      var vm = this;
      return new Promise((resolve, reject) => {
        axios.get('/api/service/isadmin')
          .then(response => {
            resolve(response.data);
          })
          .catch(error => {
            reject(error.response.data);
          });
      })
    },

    checkPermission(auth) {
      var vm = this;
      return new Promise((resolve, reject) => {
        axios.get('/api/service/ispermission/' + auth)
          .then(response => {
            resolve(response.data);
          })
          .catch(error => {
            reject(error.response.data);
          });
      })
    }

    /*GET, POST 등의 통신 함수, Toast(alert) 표출함수, 에러핸들링함수 등 선언*/

  }
});