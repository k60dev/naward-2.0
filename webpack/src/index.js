import 'core-js/stable'
import 'regenerator-runtime/runtime'
import '@babel/polyfill'
import Vue from 'vue'
import VueMeta from 'vue-meta'
import vuetify from '@/plugins/vuetify'
import VueCompositionApi from '@vue/composition-api'
import VueMoment from 'vue-moment'
import moment from 'moment';
import VueClipboard from 'vue-clipboard2'
import axios from 'axios'
import router from './router'
import App from './App.vue'
import VuePlyr from 'vue-plyr'
import CKEditor from 'ckeditor4-vue';
import VueScrollmagic from 'vue-scrollmagic'

moment.locale('ko');

Vue.use(VueMeta);
Vue.use(VueCompositionApi);
Vue.use(VueMoment, { moment });
Vue.use(VueClipboard);
Vue.prototype.$axios = axios;
Vue.use( CKEditor );
Vue.use(VueScrollmagic, {refreshInterval: 0});
Vue.use(VuePlyr)

new Vue({
  render: h => h(App),
  router: router,
  vuetify,
  components: { App },
  template : '<App/>'
}).$mount('#app');
