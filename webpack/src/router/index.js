import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/vuex/store';

const Blank = () => import(/* webpackChunkName: "blank" */ '@/components/blank')

const Admin = () => import(/* webpackChunkName: "admin" */ '@/components/admin/Admin')

const AdminDashboard = () => import(/* webpackChunkName: "adminDashboard" */ '@/components/admin/Dashboard')

const AdminPreview = () => import(/* webpackChunkName: "adminPreview" */ '@/components/admin/Preview')

const AdminTerms = () => import(/* webpackChunkName: "adminTerms" */ '@/components/admin/config/Terms')
const AdminNotification = () => import(/* webpackChunkName: "adminNotification" */ '@/components/admin/config/Notification')

const AdminUserList = () => import(/* webpackChunkName: "adminUserList" */ '@/components/admin/user/UserList')
// const AdminUserRestList = () => import(/* webpackChunkName: "adminUserList" */ '@/components/admin/user/UserList')
const AdminUserDetail = () => import(/* webpackChunkName: "adminUserDetail" */ '@/components/admin/user/UserDetail')

const AdminPaymentList = () => import(/* webpackChunkName: "adminPaymentList" */ '@/components/admin/payment/PaymentList')
const AdminPaymentDetail = () => import(/* webpackChunkName: "adminPaymentDetail" */ '@/components/admin/payment/PaymentDetail')

const AdminAdministratorList = () => import(/* webpackChunkName: "adminAdministratorList" */ '@/components/admin/administrator/AdministratorList')
const AdminAdministratorDetail = () => import(/* webpackChunkName: "adminAdministratorDetail" */ '@/components/admin/administrator/AdministratorDetail')
const AdminAdministratorLogs = () => import(/* webpackChunkName: "adminAdministratorLogs" */ '@/components/admin/administrator/Logs')

const AdminCart = () => import(/* webpackChunkName: "adminCart" */ '@/components/admin/award/Cart')

const AdminAwardIndex = () => import(/* webpackChunkName: "adminAwardIndex" */ '@/components/admin/award/Index')
const AdminAwardSelect = () => import(/* webpackChunkName: "adminAwardSelect" */ '@/components/admin/award/Select')
const AdminAwardMain = () => import(/* webpackChunkName: "adminAwardMain" */ '@/components/admin/award/Main')
const AdminAwardBase = () => import(/* webpackChunkName: "adminAwardBase" */ '@/components/admin/award/Base')
const AdminAwardPopup = () => import(/* webpackChunkName: "adminAwardPopup" */ '@/components/admin/award/Popup')
const AdminAwardJudges = () => import(/* webpackChunkName: "adminAwardJudges" */ '@/components/admin/award/Judges')
const AdminAwardJudgesParty = () => import(/* webpackChunkName: "adminAwardJudgesParty" */ '@/components/admin/award/JudgesParty')
const AdminAwardDescript = () => import(/* webpackChunkName: "adminAwardDescript" */ '@/components/admin/award/Descript')
const AdminAwardCategories = () => import(/* webpackChunkName: "adminAwardCategories" */ '@/components/admin/award/Categories')
const AdminAwardPrizes = () => import(/* webpackChunkName: "adminAwardPrizes" */ '@/components/admin/award/Prizes')
const AdminAwardResultList = () => import(/* webpackChunkName: "adminAwardResultList" */ '@/components/admin/award/ResultList')
const AdminAwardResultDetail = () => import(/* webpackChunkName: "adminAwardResultDetail" */ '@/components/admin/award/ResultDetail')
const AdminAwardWinnerList = () => import(/* webpackChunkName: "adminAwardWinnerList" */ '@/components/admin/award/WinnerList')
const AdminAwardWinnerDetail = () => import(/* webpackChunkName: "adminAwardWinnerDetail" */ '@/components/admin/award/WinnerDetail')
const AdminAwardJudgement = () => import(/* webpackChunkName: "adminAwardJudgement" */ '@/components/admin/award/Judgement')
const AdminAwardAboardList = () => import(/* webpackChunkName: "adminAwardAboardList" */ '@/components/admin/award/AboardList')
const AdminAwardAboardDetail = () => import(/* webpackChunkName: "adminAwardAboardDetail" */ '@/components/admin/award/AboardDetail')


const UserMain = () => import(/* webpackChunkName: "userMain" */ '@/components/user/Main')

const UserJoinGuide = () => import(/* webpackChunkName: "userjoinGuide" */ '@/components/user/member/JoinGuide');
const UserJoin = () => import(/* webpackChunkName: "userjoin" */ '@/components/user/member/Join');
const UserJoinOld = () => import(/* webpackChunkName: "userJoinOld" */ '@/components/user/member/OlduserJoin');
const UserLogin = () => import(/* webpackChunkName: "userLogin" */ '@/components/user/member/Login');
const UserFind = () => import(/* webpackChunkName: "userFind" */ '@/components/user/member/FindUser');
const UserPassword = () => import(/* webpackChunkName: "userPasword" */ '@/components/user/member/Password');

const UserMypageBase = () => import(/* webpackChunkName: "userMypageBase" */ '@/components/user/mypage/Base');
const UserMypageMain = () => import(/* webpackChunkName: "userMypageMain" */ '@/components/user/mypage/Main');
const UserMypageRecipient = () => import(/* webpackChunkName: "userMypageRecipient" */ '@/components/user/mypage/Recipient');
const UserMypagePayment = () => import(/* webpackChunkName: "userMypagePayment" */ '@/components/user/mypage/Payment');
const UserMypagePaymentDetail = () => import(/* webpackChunkName: "userMypagePaymentDetail" */ '@/components/user/mypage/PaymentDetail');
const UserMypagePrize = () => import(/* webpackChunkName: "userMypagePrize" */ '@/components/user/mypage/Prize');
const UserMypageProfile = () => import(/* webpackChunkName: "userMypageProfile" */ '@/components/user/mypage/Profile');

const UserEnrollBase = () => import(/* webpackChunkName: "userEnrollBase" */ '@/components/user/enroll/Base');
const UserEnrollMain = () => import(/* webpackChunkName: "userEnrollMain" */ '@/components/user/enroll/Main');
const UserEnrollCategory = () => import(/* webpackChunkName: "userEnrollCategory" */ '@/components/user/enroll/Category');
const UserEnrollWork = () => import(/* webpackChunkName: "userEnrollWork" */ '@/components/user/enroll/Work');
const UserEnrollWorkList = () => import(/* webpackChunkName: "userEnrollWorkList" */ '@/components/user/enroll/WorkList');
const UserEnrollWorkPayment = () => import(/* webpackChunkName: "userEnrollWorkPayment" */ '@/components/user/enroll/Payment');
const UserEnrollPayment = () => import(/* webpackChunkName: "userEnrollPayment" */ '@/components/user/enroll/Payment');
const UserEnrollReceipt = () => import(/* webpackChunkName: "userEnrollReceipt" */ '@/components/user/enroll/Receipt');


const UserAwardMain = () => import(/* webpackChunkName: "userAwardMain" */ '@/components/user/award/Main');

const UserAwardArchive = () => import(/* webpackChunkName: "userAwardArchive" */ '@/components/user/award/Archive');
const UserAwardArchiveWinnerBase = () => import(/* webpackChunkName: "userAwardArchiveWinnerBase" */ '@/components/user/award/ArchiveWinnerBase');
const UserAwardArchiveWinnerList = () => import(/* webpackChunkName: "userAwardArchiveWinnerList" */ '@/components/user/award/ArchiveWinnerList');
const UserAwardArchiveWinnerDetail = () => import(/* webpackChunkName: "userAwardWinnerDetail" */ '@/components/user/award/ArchiveWinnerDetail');

const UserAwardWinnerBase = () => import(/* webpackChunkName: "userAwardWinnerBase" */ '@/components/user/award/WinnerBase');
const UserAwardWinnerList = () => import(/* webpackChunkName: "userAwardWinnerList" */ '@/components/user/award/WinnerList');
const UserAwardWinnerDetail = () => import(/* webpackChunkName: "userAwardWinnerDetail" */ '@/components/user/award/WinnerDetail');

const UserAboardList = () => import(/* webpackChunkName: "userAboardList" */ '@/components/user/award/Aboard');
const UserArchiveAboardList = () => import(/* webpackChunkName: "userArchiveAboardList" */ '@/components/user/award/ArchiveAboard');



const UserAboutBase = () => import(/* webpackChunkName: "userAboutBase" */ '@/components/user/about/Base');
const UserAboutIndex = () => import(/* webpackChunkName: "userAboutIndex" */ '@/components/user/about/Index');
const UserAboutJudge = () => import(/* webpackChunkName: "userAboutJudge" */ '@/components/user/about/Judge');
const UserAboutSponsor = () => import(/* webpackChunkName: "userAboutSponsor" */ '@/components/user/about/Sponsor');

const UserFaq = () => import(/* webpackChunkName: "userFaq" */ '@/components/user/Faq');

const UserTerms = () => import(/* webpackChunkName: "userTerms" */ '@/components/user/Terms');
const UserPrivacy = () => import(/* webpackChunkName: "userPrivacy" */ '@/components/user/Terms');

const UserJudgementList = () => import(/* webpackChunkName: "userJudgementList" */ '@/components/user/judgement/JudgementList');

const testimg = () => import(/* webpackChunkName: "testimg" */ '@/components/testimg')

const requireLogin = (to, from, next) => {
  if (store.state.user) {
    return next();
  }
  next({name: 'userLogin', query: { redirect: to.fullPath }});
};

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/admin',
      component: Admin,
      children: [
        {
          path: '',
          name: 'admin',
          redirect: { name: 'adminDashboard' }
        },
        {
          path: 'dashboard',
          name: 'adminDashboard',
          component: AdminDashboard,
          meta: { layout: 'LayoutAdmin' }
        },

        {
          path: 'preview/:w_id',
          name: 'adminPreview',
          component: AdminPreview,
          meta: { layout: 'LayoutDefault' }
        },

        {
          path: 'config',
          component: Blank,
          children: [
            {
              path: '',
              name: 'adminConfig',
              redirect: { name: 'adminTerms' }
            },
            {
              path: 'terms',
              name: 'adminTerms',
              component: AdminTerms,
              meta: { layout: 'LayoutAdmin' },
            },
            {
              path: 'notification',
              name: 'adminNotification',
              component: AdminNotification,
              meta: { layout: 'LayoutAdmin' },
            },
          ]
        },

        {
          path: 'user',
          component: Blank,
          children: [
            {
              path: '',
              name: 'adminUser',
              // component: AdminUserList,
              // meta: { layout: 'LayoutAdmin' }
              redirect: { name: 'adminUserListStatus', params: { status: 'active' }}
            },
            {
              path: ':status',
              name: 'adminUserListStatus',
              component: AdminUserList,
              meta: { layout: 'LayoutAdmin' }
            },
            {
              path: 'create/0',
              name: 'adminUserCreate',
              component: AdminUserDetail,
              meta: { layout: 'LayoutAdmin' }
            },
            {
              path: 'view/:u_no',
              name: 'adminUserDetail',
              component: AdminUserDetail,
              meta: { layout: 'LayoutAdmin' }
            },
          ]
        },

        {
          path: 'payment',
          component: Blank,
          children: [
            {
              path: '',
              name: 'adminPaymentList',
              component: AdminPaymentList,
              meta: { layout: 'LayoutAdmin' }
            },
            {
              path: ':status',
              name: 'adminPaymentStatus',
              component: AdminPaymentList,
              meta: { layout: 'LayoutAdmin' }
            },
            {
              path: 'view/:p_no',
              name: 'adminPaymentDetail',
              component: AdminPaymentDetail,
              meta: { layout: 'LayoutAdmin' }
            },
          ]
        },

        {
          path: 'award',
          component: Blank,
          children: [
            {
              path: '',
              name: 'adminAward',
              component: AdminAwardIndex,
              meta: { layout: 'LayoutAdmin' }
            },
            {
              path: ':a_id',
              name: 'adminAwardSelect',
              component: AdminAwardSelect,
              children: [
                {
                  path: 'main',
                  name: 'adminAwardMain',
                  component: AdminAwardMain,
                  meta: { layout: 'LayoutAdmin' }
                },
                {
                  path: 'common',
                  component: Blank,
                  children: [
                    {
                      path: '',
                      name: 'adminAwardConfigCommon',
                      redirect: { name: 'adminAwardBase' }
                    },
                    {
                      path: 'base',
                      name: 'adminAwardBase',
                      component: AdminAwardBase,
                      meta: { layout: 'LayoutAdmin' }
                    },
                    {
                      path: 'judges',
                      name: 'adminAwardJudges',
                      component: AdminAwardJudges,
                      meta: { layout: 'LayoutAdmin' }
                    },
                    {
                      path: 'judgesparty',
                      name: 'adminAwardJudgesParty',
                      component: AdminAwardJudgesParty,
                      meta: { layout: 'LayoutAdmin' }
                    },
                    {
                      path: 'popup',
                      name: 'adminAwardPopup',
                      component: AdminAwardPopup,
                      meta: { layout: 'LayoutAdmin' }
                    },
                    {
                      path: 'descript',
                      name: 'adminAwardDescript',
                      component: AdminAwardDescript,
                      meta: { layout: 'LayoutAdmin' }
                    },
                  ]
                },
                {
                  path: 'detail',
                  component: Blank,
                  children: [
                    {
                      path: '',
                      name: 'adminAwardConfigDetail',
                      redirect: { name: 'adminAwardCategories' }
                    },
                    {
                      path: 'categories',
                      name: 'adminAwardCategories',
                      component: AdminAwardCategories,
                      meta: { layout: 'LayoutAdmin' }
                    },
                    {
                      path: 'prizes',
                      name: 'adminAwardPrizes',
                      component: AdminAwardPrizes,
                      meta: { layout: 'LayoutAdmin' }
                    },
                    {
                      path: 'result',
                      component: Blank,
                      children: [
                        {
                          path: '',
                          name: 'adminAwardResultList',
                          component: AdminAwardResultList,
                          meta: { layout: 'LayoutAdmin' }
                        },
                        {
                          path: ':status',
                          name: 'adminAwardResultStatus',
                          component: AdminAwardResultList,
                          meta: { layout: 'LayoutAdmin' }
                        },
                        {
                          path: 'view/:w_id',
                          name: 'adminAwardResultDetail',
                          component: AdminAwardResultDetail,
                          meta: { layout: 'LayoutAdmin' }
                        },
                      ]
                    },
                  ]
                },
                {
                  path: 'followup',
                  component: Blank,
                  children: [
                    {
                      path: '',
                      name: 'adminAwardConfigFollowup',
                      redirect: { name: 'adminAwardJudgement' }
                    },
                    {
                      path: 'winner',
                      component: Blank,
                      children: [
                        {
                          path: '',
                          name: 'adminAwardWinnerList',
                          component: AdminAwardWinnerList,
                          meta: { layout: 'LayoutAdmin' }
                        },
                        {
                          path: ':status',
                          name: 'adminAwardWinnerStatus',
                          component: AdminAwardWinnerList,
                          meta: { layout: 'LayoutAdmin' }
                        },
                        {
                          path: 'view/:zr_id',
                          name: 'adminAwardWinnerDetail',
                          component: AdminAwardWinnerDetail,
                          meta: { layout: 'LayoutAdmin' }
                        },
                      ]
                    },
                    {
                      path: 'judgement',
                      name: 'adminAwardJudgement',
                      component: AdminAwardJudgement,
                      meta: { layout: 'LayoutAdmin' }
                    },
                    {
                      path: ':ab_type',
                      name: 'adminAwardAboardList',
                      component: AdminAwardAboardList,
                      meta: { layout: 'LayoutAdmin' }
                    },
                    {
                      path: ':ab_type/create',
                      name: 'adminAwardAboardCreate',
                      component: AdminAwardAboardDetail,
                      meta: { layout: 'LayoutAdmin' }
                    },
                    {
                      path: ':ab_type/:ab_id',
                      name: 'adminAwardAboardDetail',
                      component: AdminAwardAboardDetail,
                      meta: { layout: 'LayoutAdmin' }
                    },
                  ]
                },
              ]
            },
          ]
        },

        {
          path: 'cart',
          name: 'adminCart',
          component: AdminCart,
          meta: { layout: 'LayoutAdmin' }
        },

        {
          path: 'administrator',
          component: Blank,
          children: [
            {
              path: '',
              name: 'adminAdministratorList',
              component: AdminAdministratorList,
              meta: { layout: 'LayoutAdmin' }
            },
            {
              path: 'create',
              name: 'adminAdministratorCreate',
              component: AdminAdministratorDetail,
              meta: { layout: 'LayoutAdmin' }
            },
            {
              path: 'view/:ua_id',
              name: 'adminAdministratorDetail',
              component: AdminAdministratorDetail,
              meta: { layout: 'LayoutAdmin' }
            },
            {
              path: 'logs',
              name: 'adminAdministratorLogs',
              component: AdminAdministratorLogs,
              meta: { layout: 'LayoutAdmin' }
            },
          ]
        }
      ],
      meta : {adminAuth:true}
    },

    {
      path: '/main',
      name: 'userMain',
      component: UserMain,
      meta: { layout: 'LayoutDefault' }
    },
    {
      path: '/',
      redirect: '/main',
    },

    {
      path: '/join-guide',
      name: 'userJoinGuide',
      component: UserJoinGuide,
      meta: { layout: 'LayoutDefault' }
    },
    {
      path: '/join',
      name: 'userJoin',
      component: UserJoin,
      meta: { layout: 'LayoutDefault' }
    },
    {
      path: '/olduser',
      name: 'olduserJoin',
      component: UserJoinOld,
      meta: { layout: 'LayoutDefault' }
    },
    {
      path: '/login',
      name: 'userLogin',
      component: UserLogin,
      meta: { layout: 'LayoutFull' }
    },
    {
      path: '/find_userinfo',
      name: 'userFind',
      component: UserFind,
      meta: { layout: 'LayoutFull' }
    },
    {
      path: '/change_password',
      name: 'userPassword',
      component: UserPassword,
      meta: { layout: 'LayoutFull' }
    },
    {
      path: '/testimg',
      name: 'testimg',
      component: testimg,
      meta: { layout: 'LayoutFull' }
    },

    {
      path: '/mypage',
      component: UserMypageBase,
      children: [
        {
          path: '',
          name: 'userMypageMain',
          redirect: { name: 'userMypageRecipient' }
        },
        // {
        //   path: '',
        //   name: 'userMypageMain',
        //   component: UserMypageMain,
        //   meta: { layout: 'LayoutDefault' },
        // },
        {
          path: 'recipient',
          name: 'userMypageRecipient',
          component: UserMypageRecipient,
          meta: { layout: 'LayoutDefault' }
        },
        {
          path: 'payment',
          name: 'userMypagePayment',
          component: UserMypagePayment,
          meta: { layout: 'LayoutDefault' }
        },
        {
          path: 'payment_detail/:p_no',
          name: 'userMypagePaymentDetail',
          component: UserMypagePaymentDetail,
          meta: { layout: 'LayoutDefault' }
        },
        {
          path: 'prize',
          name: 'userMypagePrize',
          component: UserMypagePrize,
          meta: { layout: 'LayoutDefault' }
        },
        {
          path: '/mypage/profile',
          name: 'userMypageProfile',
          component: UserMypageProfile,
          meta: { layout: 'LayoutDefault' }
        },
      ]
    },

    {
      path: '/about',
      component: UserAboutBase,
      children: [
        {
          path: '',
          name: 'userAboutIndex',
          component: UserAboutIndex,
          meta: { layout: 'LayoutDefault' }
        },
        {
          path: 'judge',
          name: 'userAboutJudge',
          component: UserAboutJudge,
          meta: { layout: 'LayoutDefault' }
        },
        {
          path: 'sponsor',
          name: 'userAboutSponsor',
          component: UserAboutSponsor,
          meta: { layout: 'LayoutDefault' }
        },
      ]
    },

    {
      path: '/enroll',
      component: UserEnrollBase,
      children: [
        {
          path: '',
          name: 'userEnrollMain',
          component: UserEnrollMain,
          meta: { layout: 'LayoutDefault' }
        },
        {
          path: 'category',
          name: 'userEnrollCategory',
          component: UserEnrollCategory,
          meta: { layout: 'LayoutDefault' }
        },
        {
          path: 'work',
          name: 'userEnrollWork',
          component: UserEnrollWork,
          meta: { layout: 'LayoutDefault' }
        },
        {
          path: 'cart',
          name: 'userEnrollWorkList',
          component: UserEnrollWorkList,
          meta: { layout: 'LayoutDefault' }
        },
        {
          path: 'payment',
          name: 'userEnrollWorkPayment',
          component: UserEnrollWorkPayment,
          meta: { layout: 'LayoutDefault' }
        },
        {
          path: 'payment/:before_total_price',
          name: 'userEnrollWorkPaymentProcess',
          component: UserEnrollWorkPayment,
          meta: { layout: 'LayoutDefault' }
        },
        {
          path: 'work/:w_id',
          name: 'userEnrollWorkDetail',
          component: UserEnrollWork,
          meta: { layout: 'LayoutDefault' }
        },
        {
          path: 'receipt',
          name: 'userEnrollReceipt',
          component: UserEnrollReceipt,
          meta: { layout: 'LayoutDefault' }
        },
        {
          path: 'receipt/:p_no',
          name: 'userEnrollReceiptDetail',
          component: UserEnrollReceipt,
          meta: { layout: 'LayoutDefault' }
        },
      ]
    },

    {
      path: '/faq',
      name: 'userFaq',
      component: UserFaq,
      meta: { layout: 'LayoutDefault' }
    },
    {
      path: '/terms_of_service',
      name: 'userTerms',
      component: UserTerms,
      meta: { layout: 'LayoutDefault' }
    },
    {
      path: '/privacy_policy',
      name: 'userPrivacy',
      component: UserPrivacy,
      meta: { layout: 'LayoutDefault' }
    },

    // {
    //   path: '/andaward/:award',
    //   name: 'userAwardMain',
    //   component: UserAwardMain,
    //   meta: { layout: 'LayoutDefault' }
    // },
    {
      path: '/winners',
      component: UserAwardWinnerBase,
      children: [
        {
          path: 'main/:award',
          name: 'userAwardMain',
          component: UserAwardMain,
          meta: { layout: 'LayoutDefault' }
        },
        {
          path: ':award',
          name: 'userAwardWinnerList',
          component: UserAwardWinnerList,
          meta: { layout: 'LayoutDefault' }
        },
         // {
         //   path: ':award/:w_id',
         //   name: 'userAwardWinnerDetail',
         //   component: UserAwardWinnerDetail,
         //   meta: { layout: 'LayoutDefault' }
         // },
      ]
    },

    {
      path: '/aboard',
      component: UserAwardWinnerBase,
      children: [
        {
          path: ':award',
          name: 'userAboardList',
          component: UserAboardList,
          meta: { layout: 'LayoutDefault' }
        },
      ]
    },

    {
      path: '/archive',
      component: UserAwardArchiveWinnerBase,
      children: [
        {
          path: ':award',
          name: 'userAwardArchive',
          component: UserAwardArchive,
          meta: { layout: 'LayoutDefault' }
        }
      ]
    },
    {
    path: '/archive/winners',
    component: UserAwardArchiveWinnerBase,
    children: [
      {
        path: ':award',
        name: 'userAwardArchiveWinnerList',
        component: UserAwardArchiveWinnerList,
        meta: { layout: 'LayoutDefault' }
      },
      {
        path: ':award/:w_id',
        name: 'userAwardArchiveWinnerDetail',
        component: UserAwardArchiveWinnerDetail,
        meta: { layout: 'LayoutDefault' }
      },
    ]
    },

    {
    path: '/archive/aboard',
    component: UserAwardArchiveWinnerBase,
    children: [
      {
        path: ':award',
        name: 'userArchiveAboardList',
        component: UserArchiveAboardList,
        meta: { layout: 'LayoutDefault' }
      },
    ]
    },

    {
      path: '/:awardRouter/:award',
      name: 'awardRouter',
      redirect: { name: 'userAwardWinnerList'}
    },

    // {
    //   path: '/:award',
    //   component: UserAwardBase,
    //   children: [
    //     {
    //       path: '',
    //       name: 'userAward',
    //       redirect: { name: 'userAwardMain' }
    //     },
    //     {
    //       path: 'main',
    //       name: 'userAwardMain',
    //       component: UserAwardMain,
    //       meta: { layout: 'LayoutDefault' }
    //     },
    //     {
    //       path: 'archive',
    //       name: 'userAwardAcrhive',
    //       component: UserAwardAcrhive,
    //       meta: { layout: 'LayoutDefault' }
    //     },
    //     {
    //       path: 'winners',
    //       name: 'userAwardWinners',
    //       component: UserAwardWinners,
    //       meta: { layout: 'LayoutDefault' }
    //     },
    //     {
    //       path: 'winner/:w_id',
    //       name: 'userAwardWinnerDetail',
    //       component: UserAwardWinner,
    //       meta: { layout: 'LayoutDefault' }
    //     },
    //   ]
    // },

    {
    //   path: '/judgement/:award/:j_access',
      path: '/judgement/:award/:jp_access',
      name: 'userJudgementList',
      component: UserJudgementList,
      meta: { layout: 'LayoutSimple' }
    },
  ]
})

router.beforeEach((to, from, next) => {
  document.body.classList.remove('loading', 'page-' + from.name);
  document.body.classList.add('loaded', 'page-' + to.name);
  store.commit('setSkeleton', true);

  setTimeout(function() {
    store.commit('setSkeleton', false);
    setTimeout(function() {
      store.commit('setOffsetBottom');
    }, 100);
  }, 500);
  window.scrollTo(0, 0);
  next();
})

router.afterEach((to, from) => {
})

export default router
